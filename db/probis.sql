-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 05, 2019 at 06:02 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `probis`
--

-- --------------------------------------------------------

--
-- Table structure for table `approval_probis`
--

CREATE TABLE `approval_probis` (
  `id` int(11) NOT NULL,
  `pegawai` int(11) NOT NULL,
  `pengajuan_probis` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `approval_probis`
--

INSERT INTO `approval_probis` (`id`, `pegawai`, `pengajuan_probis`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(3, 1, 3, '2019-04-26', 2, NULL, NULL, 0),
(4, 2, 3, '2019-04-26', 2, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `dokumen_input`
--

CREATE TABLE `dokumen_input` (
  `id` int(11) NOT NULL,
  `probis` int(11) NOT NULL,
  `upt` int(11) NOT NULL,
  `nama_dokumen` varchar(255) DEFAULT NULL,
  `file` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dokumen_input`
--

INSERT INTO `dokumen_input` (`id`, `probis`, `upt`, `nama_dokumen`, `file`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 466, 6, 'Dokumen A', '3505090512940001_kartuAkun.pdf', '2019-12-05', 1, '2019-12-05 08:27:06', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `dokumen_output`
--

CREATE TABLE `dokumen_output` (
  `id` int(11) NOT NULL,
  `nama_dokumen` varchar(255) DEFAULT NULL,
  `probis` int(11) NOT NULL,
  `upt` int(11) NOT NULL,
  `file` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dokumen_output`
--

INSERT INTO `dokumen_output` (`id`, `nama_dokumen`, `probis`, `upt`, `file`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Dokumen B', 466, 6, '3505090512940001_kartuDaftar.pdf', '2019-12-05', 1, '2019-12-05 08:29:51', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `feedback_probis`
--

CREATE TABLE `feedback_probis` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `probis` int(11) NOT NULL,
  `pesan` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedback_probis`
--

INSERT INTO `feedback_probis` (`id`, `user`, `probis`, `pesan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 3, 3, 'Coba Feedback', '2019-04-29', 3, '2019-04-29 06:11:04', 3, 1),
(2, 3, 3, 'Kurang Sesuai', '2019-04-29', 3, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `forgot_password`
--

CREATE TABLE `forgot_password` (
  `id` int(11) NOT NULL,
  `email` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forgot_password`
--

INSERT INTO `forgot_password` (`id`, `email`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(2, 'dodikitn@gmail.com', '2019-05-11', NULL, '2019-05-11 03:28:31', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `info_probis`
--

CREATE TABLE `info_probis` (
  `id` int(11) NOT NULL,
  `keterangan` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `info_probis`
--

INSERT INTO `info_probis` (`id`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, '<p>Proses bisnis adalah kumpulan aktivitas yang berkaitan secara logis, yang diperlukan didalam perusahaan yang dimulai dari aktivitas menerima masukan sumber daya untuk diolah sampai dengan aktivitas menghantarkan keluaran yang bernilai tambah dalam rangka memenuhi kebutuhan stakeholder (costumer, karyawan, pemegang saham, masyarakat).<p>\r\n<p>\r\nDekomposisi proses adalah keterkaitan antar proses bisnis dengan alur proses bisnis, alur proses kegiatan, alur proses tugas dan langkah kerja yang dikelompokkan kedalam aliran proses yang bersifat end to end proses dalam menjalankan kegiatan operasi perusahaan. \r\n</p>\r\n<p>\r\nDalam proses bisnis terdapat input – proses – output. \r\nYang dimaksudkan input – proses – output adalah sebagai berikut:\r\n</p>\r\n<p>\r\nInput : Dokumen yag dibutuhkan dalam menjalankan proses bisnis perusahaan, misalnya RJP PLN Pusat, RUPTL dan lain - lain\r\n</p>\r\n<p>\r\nProses : Sesuatu dari beberapa kegiatan yang menjadi satu untuk menghasilkan sesuatu yang ingin dihasilkan\r\n</p>\r\n<p>\r\nOutput : Hasil dari pengolahan kegiatan yang menghasilkan suatu produk</p>\r\n<p>\r\nAdapun level proses bisnis yang ada sebagai berikut:\r\n</p>\r\n<br/>\r\n\r\n<div class=\"table-responsive\">\r\n   <table class=\"table color-bordered-table primary-bordered-table\">\r\n    <thead>\r\n     <tr>\r\n      <th class=\'text-center\' width=\"30\">Level</th>\r\n      <th class=\'text-center\' width=\"30\">Jenis Dokumen</th>\r\n      <th class=\'text-center\' width=\"40\">Devinisi</th>\r\n     </tr>\r\n    </thead>\r\n    <tbody>\r\n     <tr>\r\n      <td>Level 1</td>\r\n      <td>Proses Bisnis</td>\r\n      <td>Serangkaian aktivitas saling terkait / berinteraksi yang mengubah masukan menjadi keluaran</td>\r\n     </tr>\r\n     <tr>\r\n      <td>Level 2</td>\r\n      <td>Alur Proses</td>\r\n      <td>Suatu petunjuk yang menjelaskan bagaimana suatu proses bisnis\r\n       dilaksanakan yang masing-masing memiliki atribut sendiri</td>\r\n     </tr>\r\n     <tr>\r\n      <td>Level 3</td>\r\n      <td>Alur Proses Kegiatan</td>\r\n      <td>Suatu petunjuk detail/rinci yang menjelaskan bagaimana suatu aktivitas tertentu untuk melaksanakan suatu alur proses bisnis</td>\r\n     </tr>\r\n     <tr>\r\n      <td>Level 4</td>\r\n      <td>Alur Proses Tugas</td>\r\n      <td>Cara tertentu untuk melaksanakan suatu kegiatan, berisi tahapan tugas selangkah demi selangkah, contoh SOP</td>\r\n     </tr>\r\n     <tr>\r\n      <td>Level 5</td>\r\n      <td>Alur Langkah Kerja</td>\r\n      <td>Berisi penjelasan secara detail setiap langkah kerja beserta peralatan, dokumen penunjang yang diperlukan, contoh IK</td>\r\n     </tr>\r\n    </tbody>\r\n   </table>\r\n\r\n  </div>', NULL, NULL, '2019-05-22 04:48:30', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `keyword_probis`
--

CREATE TABLE `keyword_probis` (
  `id` int(11) NOT NULL,
  `keyword` varchar(150) DEFAULT NULL,
  `probis` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `keyword_probis`
--

INSERT INTO `keyword_probis` (`id`, `keyword`, `probis`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1260, 'Perencanaan', 466, '2019-10-05', 1, '2019-12-04 08:31:21', 1, 0),
(1261, 'Visi', 466, '2019-10-05', 1, '2019-12-04 08:31:21', 1, 0),
(1262, 'Misi', 466, '2019-10-05', 1, '2019-12-04 08:31:21', 1, 0),
(1263, 'Strategis', 466, '2019-10-05', 1, '2019-12-04 08:31:21', 1, 0),
(1264, 'RKAU', 466, '2019-10-05', 1, '2019-12-04 08:31:21', 1, 0),
(1265, 'Perencanaan', 467, '2019-10-05', 1, '2019-12-04 09:49:45', 1, 0),
(1266, 'Visi', 467, '2019-10-05', 1, '2019-12-04 09:49:45', 1, 0),
(1267, 'Misi', 467, '2019-10-05', 1, '2019-12-04 09:49:45', 1, 0),
(1268, 'Strategis', 467, '2019-10-05', 1, '2019-12-04 09:49:45', 1, 0),
(1269, 'RKAU', 467, '2019-10-05', 1, '2019-12-04 09:49:45', 1, 0),
(1270, 'Visi', 468, '2019-10-05', 1, NULL, NULL, 0),
(1271, 'Misi', 468, '2019-10-05', 1, NULL, NULL, 0),
(1272, 'Perusahaan', 468, '2019-10-05', 1, NULL, NULL, 0),
(1273, 'Rapat [&&} Pimpinan [&&} Sosialisasi', 468, '2019-10-05', 1, NULL, NULL, 0),
(1274, 'Internalisasi', 468, '2019-10-05', 1, NULL, NULL, 0),
(1275, 'SWOT', 468, '2019-10-05', 1, NULL, NULL, 0),
(1276, 'Brain Storming', 468, '2019-10-05', 1, NULL, NULL, 0),
(1277, 'Kebijakan', 469, '2019-10-05', 1, NULL, NULL, 0),
(1278, 'manajemen', 469, '2019-10-05', 1, NULL, NULL, 0),
(1279, 'aset', 469, '2019-10-05', 1, NULL, NULL, 0),
(1280, 'RJP', 469, '2019-10-05', 1, NULL, NULL, 0),
(1281, 'sosialisasi', 469, '2019-10-05', 1, NULL, NULL, 0),
(1282, 'RJP', 470, '2019-10-05', 1, NULL, NULL, 0),
(1283, 'RUPTL', 470, '2019-10-05', 1, NULL, NULL, 0),
(1284, 'RJPP', 470, '2019-10-05', 1, NULL, NULL, 0),
(1285, 'visi', 470, '2019-10-05', 1, NULL, NULL, 0),
(1286, 'misi', 470, '2019-10-05', 1, NULL, NULL, 0),
(1287, 'draft', 470, '2019-10-05', 1, NULL, NULL, 0),
(1288, 'kriteria', 470, '2019-10-05', 1, NULL, NULL, 0),
(1289, 'major', 470, '2019-10-05', 1, NULL, NULL, 0),
(1290, 'catatatn perubahan', 470, '2019-10-05', 1, NULL, NULL, 0),
(1291, 'endorsement', 470, '2019-10-05', 1, NULL, NULL, 0),
(1292, 'KPI', 471, '2019-10-05', 1, NULL, NULL, 0),
(1293, 'Roadmap', 471, '2019-10-05', 1, NULL, NULL, 0),
(1294, 'pencapaian', 471, '2019-10-05', 1, NULL, NULL, 0),
(1295, 'Target', 471, '2019-10-05', 1, NULL, NULL, 0),
(1296, 'Kontrak manajemen', 471, '2019-10-05', 1, NULL, NULL, 0),
(1297, 'endorsement', 471, '2019-10-05', 1, NULL, NULL, 0),
(1298, 'pengukuran', 471, '2019-10-05', 1, NULL, NULL, 0),
(1299, 'kinerja', 471, '2019-10-05', 1, NULL, NULL, 0),
(1300, 'RKAU', 472, '2019-10-05', 1, NULL, NULL, 0),
(1301, 'PLN Pusat', 472, '2019-10-05', 1, NULL, NULL, 0),
(1302, 'Usulan', 472, '2019-10-05', 1, NULL, NULL, 0),
(1303, 'planning ss]ession', 472, '2019-10-05', 1, NULL, NULL, 0),
(1304, 'sasaran', 472, '2019-10-05', 1, NULL, NULL, 0),
(1305, 'inisiatif', 472, '2019-10-05', 1, NULL, NULL, 0),
(1306, 'strategi', 472, '2019-10-05', 1, NULL, NULL, 0),
(1307, 'anggaran', 472, '2019-10-05', 1, NULL, NULL, 0),
(1308, 'Risk Assessment', 472, '2019-10-05', 1, NULL, NULL, 0),
(1309, 'RAB', 472, '2019-10-05', 1, NULL, NULL, 0),
(1310, 'DIVPR [&&} DIVOR', 472, '2019-10-05', 1, NULL, NULL, 0),
(1311, 'DIVANG', 472, '2019-10-05', 1, NULL, NULL, 0),
(1312, 'ebudget', 472, '2019-10-05', 1, NULL, NULL, 0),
(1313, 'pagu', 472, '2019-10-05', 1, NULL, NULL, 0),
(1314, 'indikatif', 472, '2019-10-05', 1, NULL, NULL, 0),
(1315, 'SKKI', 472, '2019-10-05', 1, NULL, NULL, 0),
(1316, 'penajaman', 472, '2019-10-05', 1, NULL, NULL, 0),
(1317, 'endorsement', 472, '2019-10-05', 1, NULL, NULL, 0),
(1318, '', 473, '2019-10-05', 1, NULL, NULL, 0),
(1319, 'Review', 474, '2019-10-05', 1, NULL, NULL, 0),
(1320, 'Revisi', 474, '2019-10-05', 1, NULL, NULL, 0),
(1321, 'Probis', 474, '2019-10-05', 1, NULL, NULL, 0),
(1322, 'Usulan', 474, '2019-10-05', 1, NULL, NULL, 0),
(1323, 'draft', 474, '2019-10-05', 1, NULL, NULL, 0),
(1324, 'Manajemen', 475, '2019-10-05', 1, NULL, NULL, 0),
(1325, 'Proyek', 475, '2019-10-05', 1, NULL, NULL, 0),
(1326, '', 475, '2019-10-05', 1, NULL, NULL, 0),
(1327, 'Manajemen', 476, '2019-10-05', 1, NULL, NULL, 0),
(1328, 'Proyek', 476, '2019-10-05', 1, NULL, NULL, 0),
(1329, '', 476, '2019-10-05', 1, NULL, NULL, 0),
(1330, 'DRP', 477, '2019-10-05', 1, NULL, NULL, 0),
(1331, 'SKKI', 477, '2019-10-05', 1, NULL, NULL, 0),
(1332, 'Pengadaan {&&] Rencana', 477, '2019-10-05', 1, NULL, NULL, 0),
(1333, 'SKKI [&&} Anggaran [&&} Investasi', 478, '2019-10-05', 1, NULL, NULL, 0),
(1334, 'DRP', 478, '2019-10-05', 1, NULL, NULL, 0),
(1335, 'workplan', 478, '2019-10-05', 1, NULL, NULL, 0),
(1336, 'SKKI murni', 478, '2019-10-05', 1, NULL, NULL, 0),
(1337, 'SKKO', 479, '2019-10-05', 1, NULL, NULL, 0),
(1338, 'Anggaran', 479, '2019-10-05', 1, NULL, NULL, 0),
(1339, 'AO', 479, '2019-10-05', 1, NULL, NULL, 0),
(1340, 'RKAU', 479, '2019-10-05', 1, NULL, NULL, 0),
(1341, 'Pra Enjiniring', 479, '2019-10-05', 1, NULL, NULL, 0),
(1342, 'SKAO [&&} penetapan [&&} Rekonsiliasi', 479, '2019-10-05', 1, NULL, NULL, 0),
(1343, 'kontrak', 479, '2019-10-05', 1, NULL, NULL, 0),
(1344, 'SKKI', 480, '2019-10-05', 1, NULL, NULL, 0),
(1345, 'RAB [&&} BOQ', 480, '2019-10-05', 1, NULL, NULL, 0),
(1346, 'Revisi', 480, '2019-10-05', 1, NULL, NULL, 0),
(1347, 'Relokasi', 480, '2019-10-05', 1, NULL, NULL, 0),
(1348, 'Feasibility', 480, '2019-10-05', 1, NULL, NULL, 0),
(1349, 'Spesifikasi teknis', 480, '2019-10-05', 1, NULL, NULL, 0),
(1350, 'pelelangan', 480, '2019-10-05', 1, NULL, NULL, 0),
(1351, 'PR', 480, '2019-10-05', 1, NULL, NULL, 0),
(1352, 'SKKO', 481, '2019-10-05', 1, NULL, NULL, 0),
(1353, 'Anggaran', 481, '2019-10-05', 1, NULL, NULL, 0),
(1354, 'AO', 481, '2019-10-05', 1, NULL, NULL, 0),
(1355, 'RKAU', 481, '2019-10-05', 1, NULL, NULL, 0),
(1356, 'Pra Enjiniring', 481, '2019-10-05', 1, NULL, NULL, 0),
(1357, 'SKAO [&&} penetapan [&&} Rekonsiliasi', 481, '2019-10-05', 1, NULL, NULL, 0),
(1358, 'kontrak', 481, '2019-10-05', 1, NULL, NULL, 0),
(1359, 'HPE', 482, '2019-10-05', 1, NULL, NULL, 0),
(1360, 'penawaran', 482, '2019-10-05', 1, NULL, NULL, 0),
(1361, 'harga', 482, '2019-10-05', 1, NULL, NULL, 0),
(1362, 'dokumen', 482, '2019-10-05', 1, NULL, NULL, 0),
(1363, 'pelelangan', 482, '2019-10-05', 1, NULL, NULL, 0),
(1364, 'kriteria', 482, '2019-10-05', 1, NULL, NULL, 0),
(1365, 'pengadaan', 482, '2019-10-05', 1, NULL, NULL, 0),
(1366, 'RAB', 482, '2019-10-05', 1, NULL, NULL, 0),
(1367, 'Manajemen', 483, '2019-10-05', 1, NULL, NULL, 0),
(1368, 'Proyek', 483, '2019-10-05', 1, NULL, NULL, 0),
(1369, '', 483, '2019-10-05', 1, NULL, NULL, 0),
(1370, 'Pelelangan', 484, '2019-10-05', 1, NULL, NULL, 0),
(1371, 'Terbatas', 484, '2019-10-05', 1, NULL, NULL, 0),
(1372, 'DPT', 484, '2019-10-05', 1, NULL, NULL, 0),
(1373, 'RKS', 484, '2019-10-05', 1, NULL, NULL, 0),
(1374, 'TOR [&] DPT harga', 484, '2019-10-05', 1, NULL, NULL, 0),
(1375, 'tarif ]&&] HPS', 484, '2019-10-05', 1, NULL, NULL, 0),
(1376, 'Penunjukkan', 485, '2019-10-05', 1, NULL, NULL, 0),
(1377, 'HPS', 485, '2019-10-05', 1, NULL, NULL, 0),
(1378, 'DPT', 485, '2019-10-05', 1, NULL, NULL, 0),
(1379, 'dokumen', 485, '2019-10-05', 1, NULL, NULL, 0),
(1380, 'kualifikasi', 485, '2019-10-05', 1, NULL, NULL, 0),
(1381, 'vendor', 485, '2019-10-05', 1, NULL, NULL, 0),
(1382, 'sampul', 485, '2019-10-05', 1, NULL, NULL, 0),
(1383, 'pelalangan', 485, '2019-10-05', 1, NULL, NULL, 0),
(1384, 'garansi', 485, '2019-10-05', 1, NULL, NULL, 0),
(1385, 'CDA', 485, '2019-10-05', 1, NULL, NULL, 0),
(1386, 'kontrak', 485, '2019-10-05', 1, NULL, NULL, 0),
(1387, 'Pengadaan', 486, '2019-10-05', 1, NULL, NULL, 0),
(1388, 'dokumen', 486, '2019-10-05', 1, NULL, NULL, 0),
(1389, 'kualifikasi', 486, '2019-10-05', 1, NULL, NULL, 0),
(1390, 'vendor', 486, '2019-10-05', 1, NULL, NULL, 0),
(1391, 'sampul', 486, '2019-10-05', 1, NULL, NULL, 0),
(1392, 'SPK', 486, '2019-10-05', 1, NULL, NULL, 0),
(1393, 'kontrak', 486, '2019-10-05', 1, NULL, NULL, 0),
(1394, '', 487, '2019-10-05', 1, NULL, NULL, 0),
(1395, 'Approval', 488, '2019-10-05', 1, NULL, NULL, 0),
(1396, 'dokumen', 488, '2019-10-05', 1, NULL, NULL, 0),
(1397, 'konsinyering', 488, '2019-10-05', 1, NULL, NULL, 0),
(1398, 'RKS', 488, '2019-10-05', 1, NULL, NULL, 0),
(1399, 'BoQ', 488, '2019-10-05', 1, NULL, NULL, 0),
(1400, 'Supply Erect', 488, '2019-10-05', 1, NULL, NULL, 0),
(1401, 'Supply Only', 488, '2019-10-05', 1, NULL, NULL, 0),
(1402, 'Approval', 489, '2019-10-05', 1, NULL, NULL, 0),
(1403, 'dokumen', 489, '2019-10-05', 1, NULL, NULL, 0),
(1404, 'konsinyering', 489, '2019-10-05', 1, NULL, NULL, 0),
(1405, 'RKS', 489, '2019-10-05', 1, NULL, NULL, 0),
(1406, 'BoQ', 489, '2019-10-05', 1, NULL, NULL, 0),
(1407, 'Supply Erect', 489, '2019-10-05', 1, NULL, NULL, 0),
(1408, 'Supply Only', 489, '2019-10-05', 1, NULL, NULL, 0),
(1409, 'Pembelian', 490, '2019-10-05', 1, NULL, NULL, 0),
(1410, 'Supply', 490, '2019-10-05', 1, NULL, NULL, 0),
(1411, 'erect', 490, '2019-10-05', 1, NULL, NULL, 0),
(1412, 'FAT', 490, '2019-10-05', 1, NULL, NULL, 0),
(1413, 'SLO', 490, '2019-10-05', 1, NULL, NULL, 0),
(1414, 'Pending', 490, '2019-10-05', 1, NULL, NULL, 0),
(1415, 'Pekerjaan', 490, '2019-10-05', 1, NULL, NULL, 0),
(1416, 'lolos', 490, '2019-10-05', 1, NULL, NULL, 0),
(1417, 'pemenang', 490, '2019-10-05', 1, NULL, NULL, 0),
(1418, 'konstruksi', 490, '2019-10-05', 1, NULL, NULL, 0),
(1419, 'Approval', 491, '2019-10-05', 1, NULL, NULL, 0),
(1420, 'Pabrikasi', 491, '2019-10-05', 1, NULL, NULL, 0),
(1421, 'FAT', 491, '2019-10-05', 1, NULL, NULL, 0),
(1422, 'Tim mutu', 491, '2019-10-05', 1, NULL, NULL, 0),
(1423, 'Approval', 492, '2019-10-05', 1, NULL, NULL, 0),
(1424, 'Pabrikasi', 492, '2019-10-05', 1, NULL, NULL, 0),
(1425, 'FAT', 492, '2019-10-05', 1, NULL, NULL, 0),
(1426, 'Tim mutu', 492, '2019-10-05', 1, NULL, NULL, 0),
(1427, 'L/C {&&] Letter of Credit', 492, '2019-10-05', 1, NULL, NULL, 0),
(1428, 'Lolos', 492, '2019-10-05', 1, NULL, NULL, 0),
(1429, 'CIF', 492, '2019-10-05', 1, NULL, NULL, 0),
(1430, 'FOB', 492, '2019-10-05', 1, NULL, NULL, 0),
(1431, 'Delivery', 492, '2019-10-05', 1, NULL, NULL, 0),
(1432, 'Transport', 492, '2019-10-05', 1, NULL, NULL, 0),
(1433, 'Approval', 493, '2019-10-05', 1, NULL, NULL, 0),
(1434, 'Pabrikasi', 493, '2019-10-05', 1, NULL, NULL, 0),
(1435, 'FAT', 493, '2019-10-05', 1, NULL, NULL, 0),
(1436, 'Tim mutu', 493, '2019-10-05', 1, NULL, NULL, 0),
(1437, 'L/C {&&] Letter of Credit', 493, '2019-10-05', 1, NULL, NULL, 0),
(1438, 'Lolos', 493, '2019-10-05', 1, NULL, NULL, 0),
(1439, 'CIF', 493, '2019-10-05', 1, NULL, NULL, 0),
(1440, 'FOB', 493, '2019-10-05', 1, NULL, NULL, 0),
(1441, 'Delivery', 493, '2019-10-05', 1, NULL, NULL, 0),
(1442, 'Transport', 493, '2019-10-05', 1, NULL, NULL, 0),
(1443, 'Erect', 494, '2019-10-05', 1, NULL, NULL, 0),
(1444, 'Material', 494, '2019-10-05', 1, NULL, NULL, 0),
(1445, 'SOP', 494, '2019-10-05', 1, NULL, NULL, 0),
(1446, 'SLO', 494, '2019-10-05', 1, NULL, NULL, 0),
(1447, 'jasa sertifikasi', 494, '2019-10-05', 1, NULL, NULL, 0),
(1448, 'pending', 494, '2019-10-05', 1, NULL, NULL, 0),
(1449, 'RLB', 494, '2019-10-05', 1, NULL, NULL, 0),
(1450, 'PIB', 495, '2019-10-05', 1, NULL, NULL, 0),
(1451, 'Pemberitahuan Impor Barang', 495, '2019-10-05', 1, NULL, NULL, 0),
(1452, 'DO', 495, '2019-10-05', 1, NULL, NULL, 0),
(1453, 'Delivery Order', 495, '2019-10-05', 1, NULL, NULL, 0),
(1454, 'Credit', 495, '2019-10-05', 1, NULL, NULL, 0),
(1455, 'Invoice', 495, '2019-10-05', 1, NULL, NULL, 0),
(1456, 'surat', 495, '2019-10-05', 1, NULL, NULL, 0),
(1457, 'kuasa [&&} e-billing lon site', 495, '2019-10-05', 1, NULL, NULL, 0),
(1458, 'bank', 496, '2019-10-05', 1, NULL, NULL, 0),
(1459, 'garansi', 496, '2019-10-05', 1, NULL, NULL, 0),
(1460, 'pemutusan', 496, '2019-10-05', 1, NULL, NULL, 0),
(1461, 'kontrak', 496, '2019-10-05', 1, NULL, NULL, 0),
(1462, 'jawaban', 496, '2019-10-05', 1, NULL, NULL, 0),
(1463, 'konfirmasi', 496, '2019-10-05', 1, NULL, NULL, 0),
(1464, 'jaminan', 496, '2019-10-05', 1, NULL, NULL, 0),
(1465, 'emergency', 497, '2019-10-05', 1, NULL, NULL, 0),
(1466, 'stock', 497, '2019-10-05', 1, NULL, NULL, 0),
(1467, 'opname', 497, '2019-10-05', 1, NULL, NULL, 0),
(1468, 'anggaran', 497, '2019-10-05', 1, NULL, NULL, 0),
(1469, 'HPS', 497, '2019-10-05', 1, NULL, NULL, 0),
(1470, 'HPE', 497, '2019-10-05', 1, NULL, NULL, 0),
(1471, 'survey', 497, '2019-10-05', 1, NULL, NULL, 0),
(1472, 'tanah', 497, '2019-10-05', 1, NULL, NULL, 0),
(1473, 'kontrak', 497, '2019-10-05', 1, NULL, NULL, 0),
(1474, '', 498, '2019-10-05', 1, NULL, NULL, 0),
(1475, '', 499, '2019-10-05', 1, NULL, NULL, 0),
(1476, '', 500, '2019-10-05', 1, NULL, NULL, 0),
(1477, '', 501, '2019-10-05', 1, NULL, NULL, 0),
(1478, 'Manajemen', 502, '2019-10-05', 1, NULL, NULL, 0),
(1479, 'Proyek', 502, '2019-10-05', 1, NULL, NULL, 0),
(1480, 'Pengendalian', 502, '2019-10-05', 1, NULL, NULL, 0),
(1481, '', 503, '2019-10-05', 1, NULL, NULL, 0),
(1482, 'PR', 504, '2019-10-05', 1, NULL, NULL, 0),
(1483, 'Purchase', 504, '2019-10-05', 1, NULL, NULL, 0),
(1484, 'Request', 504, '2019-10-05', 1, NULL, NULL, 0),
(1485, 'LKP', 504, '2019-10-05', 1, NULL, NULL, 0),
(1486, 'Pekerjaan', 504, '2019-10-05', 1, NULL, NULL, 0),
(1487, 'Pengendalian', 504, '2019-10-05', 1, NULL, NULL, 0),
(1488, 'PO', 504, '2019-10-05', 1, NULL, NULL, 0),
(1489, 'Approval', 504, '2019-10-05', 1, NULL, NULL, 0),
(1490, 'BAST', 504, '2019-10-05', 1, NULL, NULL, 0),
(1491, 'BASP', 504, '2019-10-05', 1, NULL, NULL, 0),
(1492, 'BAKP', 504, '2019-10-05', 1, NULL, NULL, 0),
(1493, '', 505, '2019-10-05', 1, NULL, NULL, 0),
(1494, '', 506, '2019-10-05', 1, NULL, NULL, 0),
(1495, '', 507, '2019-10-05', 1, NULL, NULL, 0),
(1496, 'monitoring', 508, '2019-10-05', 1, NULL, NULL, 0),
(1497, 'SKKI', 508, '2019-10-05', 1, NULL, NULL, 0),
(1498, 'Pengadaan', 508, '2019-10-05', 1, NULL, NULL, 0),
(1499, 'progres', 508, '2019-10-05', 1, NULL, NULL, 0),
(1500, 'Evaluasi', 508, '2019-10-05', 1, NULL, NULL, 0),
(1501, 'Kinerja', 508, '2019-10-05', 1, NULL, NULL, 0),
(1502, 'PRK', 508, '2019-10-05', 1, NULL, NULL, 0),
(1503, 'PMO', 509, '2019-10-05', 1, NULL, NULL, 0),
(1504, 'Project', 509, '2019-10-05', 1, NULL, NULL, 0),
(1505, 'RUPTL', 509, '2019-10-05', 1, NULL, NULL, 0),
(1506, 'procurement', 509, '2019-10-05', 1, NULL, NULL, 0),
(1507, 'COD', 509, '2019-10-05', 1, NULL, NULL, 0),
(1508, 'RKS', 509, '2019-10-05', 1, NULL, NULL, 0),
(1509, 'HPE', 509, '2019-10-05', 1, NULL, NULL, 0),
(1510, 'kontrak', 509, '2019-10-05', 1, NULL, NULL, 0),
(1511, 'Kock of meeting', 509, '2019-10-05', 1, NULL, NULL, 0),
(1512, 'CoP', 509, '2019-10-05', 1, NULL, NULL, 0),
(1513, 'Warning', 509, '2019-10-05', 1, NULL, NULL, 0),
(1514, '', 510, '2019-10-05', 1, NULL, NULL, 0),
(1515, '', 511, '2019-10-05', 1, NULL, NULL, 0),
(1516, '', 512, '2019-10-05', 1, NULL, NULL, 0),
(1517, '', 513, '2019-10-05', 1, NULL, NULL, 0),
(1518, '', 514, '2019-10-05', 1, NULL, NULL, 0),
(1519, '', 515, '2019-10-05', 1, NULL, NULL, 0),
(1520, '', 516, '2019-10-05', 1, NULL, NULL, 0),
(1521, 'faktur', 517, '2019-10-05', 1, NULL, NULL, 0),
(1522, 'pajak', 517, '2019-10-05', 1, NULL, NULL, 0),
(1523, 'SE', 517, '2019-10-05', 1, NULL, NULL, 0),
(1524, 'checklist', 517, '2019-10-05', 1, NULL, NULL, 0),
(1525, 'Bank', 517, '2019-10-05', 1, NULL, NULL, 0),
(1526, '', 518, '2019-10-05', 1, NULL, NULL, 0),
(1527, '', 519, '2019-10-05', 1, NULL, NULL, 0),
(1528, '', 520, '2019-10-05', 1, NULL, NULL, 0),
(1529, '', 521, '2019-10-05', 1, NULL, NULL, 0),
(1530, '', 522, '2019-10-05', 1, NULL, NULL, 0),
(1531, '', 523, '2019-10-05', 1, NULL, NULL, 0),
(1532, '', 524, '2019-10-05', 1, NULL, NULL, 0),
(1533, '', 525, '2019-10-05', 1, NULL, NULL, 0),
(1534, '', 526, '2019-10-05', 1, NULL, NULL, 0),
(1535, '', 527, '2019-10-05', 1, NULL, NULL, 0),
(1536, '', 528, '2019-10-05', 1, NULL, NULL, 0),
(1537, '', 529, '2019-10-05', 1, NULL, NULL, 0),
(1538, '', 530, '2019-10-05', 1, NULL, NULL, 0),
(1539, '', 531, '2019-10-05', 1, NULL, NULL, 0),
(1540, 'SLO', 532, '2019-10-05', 1, NULL, NULL, 0),
(1541, 'RKAU', 532, '2019-10-05', 1, NULL, NULL, 0),
(1542, 'Prioritas', 532, '2019-10-05', 1, NULL, NULL, 0),
(1543, 'penawaran', 532, '2019-10-05', 1, NULL, NULL, 0),
(1544, 'Draft', 532, '2019-10-05', 1, NULL, NULL, 0),
(1545, 'pusertif', 532, '2019-10-05', 1, NULL, NULL, 0),
(1546, 'Inspeksi', 532, '2019-10-05', 1, NULL, NULL, 0),
(1547, 'pending', 532, '2019-10-05', 1, NULL, NULL, 0),
(1548, 'mayor', 532, '2019-10-05', 1, NULL, NULL, 0),
(1549, 'eviden', 532, '2019-10-05', 1, NULL, NULL, 0),
(1550, 'LHPP [sertifikat', 532, '2019-10-05', 1, NULL, NULL, 0),
(1551, '', 533, '2019-10-05', 1, NULL, NULL, 0),
(1552, 'Invetarisasi', 534, '2019-10-05', 1, NULL, NULL, 0),
(1553, 'Fisik', 534, '2019-10-05', 1, NULL, NULL, 0),
(1554, 'aset', 534, '2019-10-05', 1, NULL, NULL, 0),
(1555, 'stiker', 534, '2019-10-05', 1, NULL, NULL, 0),
(1556, 'Aktiva', 534, '2019-10-05', 1, NULL, NULL, 0),
(1557, 'SAP', 534, '2019-10-05', 1, NULL, NULL, 0),
(1558, 'FM', 534, '2019-10-05', 1, NULL, NULL, 0),
(1559, 'ATTB', 534, '2019-10-05', 1, NULL, NULL, 0),
(1560, 'PDP', 534, '2019-10-05', 1, NULL, NULL, 0),
(1561, 'DL1', 534, '2019-10-05', 1, NULL, NULL, 0),
(1562, 'DL3', 534, '2019-10-05', 1, NULL, NULL, 0),
(1563, 'MM', 534, '2019-10-05', 1, NULL, NULL, 0),
(1564, 'PM', 534, '2019-10-05', 1, NULL, NULL, 0),
(1565, 'Akuntansi', 534, '2019-10-05', 1, NULL, NULL, 0),
(1566, '', 535, '2019-10-05', 1, NULL, NULL, 0),
(1567, '', 536, '2019-10-05', 1, NULL, NULL, 0),
(1568, '', 537, '2019-10-05', 1, NULL, NULL, 0),
(1569, '', 538, '2019-10-05', 1, NULL, NULL, 0),
(1570, '', 539, '2019-10-05', 1, NULL, NULL, 0),
(1571, '', 540, '2019-10-05', 1, NULL, NULL, 0),
(1572, '', 541, '2019-10-05', 1, NULL, NULL, 0),
(1573, '', 542, '2019-10-05', 1, NULL, NULL, 0),
(1574, '', 543, '2019-10-05', 1, NULL, NULL, 0),
(1575, '', 544, '2019-10-05', 1, NULL, NULL, 0),
(1576, '', 545, '2019-10-05', 1, NULL, NULL, 0),
(1577, '', 546, '2019-10-05', 1, NULL, NULL, 0),
(1578, '', 547, '2019-10-05', 1, NULL, NULL, 0),
(1579, '', 548, '2019-10-05', 1, NULL, NULL, 0),
(1580, '', 549, '2019-10-05', 1, NULL, NULL, 0),
(1581, '', 550, '2019-10-05', 1, NULL, NULL, 0),
(1582, 'CBM', 551, '2019-10-05', 1, NULL, NULL, 0),
(1583, 'anomaly', 551, '2019-10-05', 1, NULL, NULL, 0),
(1584, 'mayor [emergency', 551, '2019-10-05', 1, NULL, NULL, 0),
(1585, 'gardu [&&[ induk', 551, '2019-10-05', 1, NULL, NULL, 0),
(1586, 'Transmisi', 551, '2019-10-05', 1, NULL, NULL, 0),
(1587, 'IL1', 551, '2019-10-05', 1, NULL, NULL, 0),
(1588, 'IL2', 551, '2019-10-05', 1, NULL, NULL, 0),
(1589, 'IL3', 551, '2019-10-05', 1, NULL, NULL, 0),
(1590, 'DL1', 551, '2019-10-05', 1, NULL, NULL, 0),
(1591, 'DL2', 551, '2019-10-05', 1, NULL, NULL, 0),
(1592, 'DL3', 551, '2019-10-05', 1, NULL, NULL, 0),
(1593, 'APPL', 552, '2019-10-05', 1, NULL, NULL, 0),
(1594, 'gangguan', 552, '2019-10-05', 1, NULL, NULL, 0),
(1595, 'padam', 552, '2019-10-05', 1, NULL, NULL, 0),
(1596, 'konsumen', 552, '2019-10-05', 1, NULL, NULL, 0),
(1597, 'ENS', 552, '2019-10-05', 1, NULL, NULL, 0),
(1598, 'teguran', 552, '2019-10-05', 1, NULL, NULL, 0),
(1599, 'vendor', 552, '2019-10-05', 1, NULL, NULL, 0),
(1600, 'TVOSP', 552, '2019-10-05', 1, NULL, NULL, 0),
(1601, 'padam', 552, '2019-10-05', 1, NULL, NULL, 0),
(1602, 'Hukum', 552, '2019-10-05', 1, NULL, NULL, 0),
(1603, '', 553, '2019-10-05', 1, NULL, NULL, 0),
(1604, '', 554, '2019-10-05', 1, NULL, NULL, 0),
(1605, '', 555, '2019-10-05', 1, NULL, NULL, 0),
(1606, '', 556, '2019-10-05', 1, NULL, NULL, 0),
(1607, '', 557, '2019-10-05', 1, NULL, NULL, 0),
(1608, 'Mutasi', 558, '2019-10-05', 1, NULL, NULL, 0),
(1609, 'keluar', 558, '2019-10-05', 1, NULL, NULL, 0),
(1610, 'aset', 558, '2019-10-05', 1, NULL, NULL, 0),
(1611, 'survey', 558, '2019-10-05', 1, NULL, NULL, 0),
(1612, 'kepemilikan', 558, '2019-10-05', 1, NULL, NULL, 0),
(1613, 'penolakan', 558, '2019-10-05', 1, NULL, NULL, 0),
(1614, 'BAST', 558, '2019-10-05', 1, NULL, NULL, 0),
(1615, 'ijin', 558, '2019-10-05', 1, NULL, NULL, 0),
(1616, 'Prinsip', 558, '2019-10-05', 1, NULL, NULL, 0),
(1617, 'relokasi', 558, '2019-10-05', 1, NULL, NULL, 0),
(1618, '', 559, '2019-10-05', 1, NULL, NULL, 0),
(1619, '', 560, '2019-10-05', 1, NULL, NULL, 0),
(1620, '', 561, '2019-10-05', 1, NULL, NULL, 0),
(1621, '', 562, '2019-10-05', 1, NULL, NULL, 0),
(1622, '', 563, '2019-10-05', 1, NULL, NULL, 0),
(1623, '', 564, '2019-10-05', 1, NULL, NULL, 0),
(1624, '', 565, '2019-10-05', 1, NULL, NULL, 0),
(1625, '', 566, '2019-10-05', 1, NULL, NULL, 0),
(1626, '', 567, '2019-10-05', 1, NULL, NULL, 0),
(1627, '', 568, '2019-10-05', 1, NULL, NULL, 0),
(1628, '', 569, '2019-10-05', 1, NULL, NULL, 0),
(1629, '', 570, '2019-10-05', 1, NULL, NULL, 0),
(1630, '', 571, '2019-10-05', 1, NULL, NULL, 0),
(1631, '', 572, '2019-10-05', 1, NULL, NULL, 0),
(1632, '', 573, '2019-10-05', 1, NULL, NULL, 0),
(1633, '', 574, '2019-10-05', 1, NULL, NULL, 0),
(1634, '', 575, '2019-10-05', 1, NULL, NULL, 0),
(1635, '', 576, '2019-10-05', 1, NULL, NULL, 0),
(1636, '', 577, '2019-10-05', 1, NULL, NULL, 0),
(1637, '', 578, '2019-10-05', 1, NULL, NULL, 0),
(1638, '', 579, '2019-10-05', 1, NULL, NULL, 0),
(1639, '', 580, '2019-10-05', 1, NULL, NULL, 0),
(1640, '', 581, '2019-10-05', 1, NULL, NULL, 0),
(1641, '', 582, '2019-10-05', 1, NULL, NULL, 0),
(1642, '', 583, '2019-10-05', 1, NULL, NULL, 0),
(1643, '', 584, '2019-10-05', 1, NULL, NULL, 0),
(1644, '', 585, '2019-10-05', 1, NULL, NULL, 0),
(1645, '', 586, '2019-10-05', 1, NULL, NULL, 0),
(1646, '', 587, '2019-10-05', 1, NULL, NULL, 0),
(1647, '', 588, '2019-10-05', 1, NULL, NULL, 0),
(1648, '', 589, '2019-10-05', 1, NULL, NULL, 0),
(1649, '', 590, '2019-10-05', 1, NULL, NULL, 0),
(1650, '', 591, '2019-10-05', 1, NULL, NULL, 0),
(1651, '', 592, '2019-10-05', 1, NULL, NULL, 0),
(1652, '', 593, '2019-10-05', 1, NULL, NULL, 0),
(1653, '', 594, '2019-10-05', 1, NULL, NULL, 0),
(1654, '', 595, '2019-10-05', 1, NULL, NULL, 0),
(1655, '', 596, '2019-10-05', 1, NULL, NULL, 0),
(1656, '', 597, '2019-10-05', 1, NULL, NULL, 0),
(1657, '', 598, '2019-10-05', 1, NULL, NULL, 0),
(1658, '', 599, '2019-10-05', 1, NULL, NULL, 0),
(1659, 'IPPKH', 600, '2019-10-05', 1, NULL, NULL, 0),
(1660, 'RKAU', 600, '2019-10-05', 1, NULL, NULL, 0),
(1661, 'DISHUT', 600, '2019-10-05', 1, NULL, NULL, 0),
(1662, 'survey', 600, '2019-10-05', 1, NULL, NULL, 0),
(1663, 'tower', 600, '2019-10-05', 1, NULL, NULL, 0),
(1664, 'Sutet', 600, '2019-10-05', 1, NULL, NULL, 0),
(1665, 'ijin', 600, '2019-10-05', 1, NULL, NULL, 0),
(1666, 'Perpanjangan', 600, '2019-10-05', 1, NULL, NULL, 0),
(1667, 'RUPTL', 601, '2019-10-05', 1, NULL, NULL, 0),
(1668, 'emergency', 601, '2019-10-05', 1, NULL, NULL, 0),
(1669, 'titik', 601, '2019-10-05', 1, NULL, NULL, 0),
(1670, 'koodinat', 601, '2019-10-05', 1, NULL, NULL, 0),
(1671, 'RTRW', 601, '2019-10-05', 1, NULL, NULL, 0),
(1672, 'tata', 601, '2019-10-05', 1, NULL, NULL, 0),
(1673, 'Ruang', 601, '2019-10-05', 1, NULL, NULL, 0),
(1674, 'Wilayah', 601, '2019-10-05', 1, NULL, NULL, 0),
(1675, 'tanah', 602, '2019-10-05', 1, NULL, NULL, 0),
(1676, 'aset', 602, '2019-10-05', 1, NULL, NULL, 0),
(1677, 'jasa', 602, '2019-10-05', 1, NULL, NULL, 0),
(1678, 'notaris', 602, '2019-10-05', 1, NULL, NULL, 0),
(1679, 'BPN', 602, '2019-10-05', 1, NULL, NULL, 0),
(1680, 'BPHTB', 602, '2019-10-05', 1, NULL, NULL, 0),
(1681, 'RKAU', 602, '2019-10-05', 1, NULL, NULL, 0),
(1682, 'Sertifikat', 602, '2019-10-05', 1, NULL, NULL, 0),
(1683, 'sertifikasi', 602, '2019-10-05', 1, NULL, NULL, 0),
(1684, 'tanah', 603, '2019-10-05', 1, NULL, NULL, 0),
(1685, 'aset', 603, '2019-10-05', 1, NULL, NULL, 0),
(1686, 'jasa', 603, '2019-10-05', 1, NULL, NULL, 0),
(1687, 'notaris', 603, '2019-10-05', 1, NULL, NULL, 0),
(1688, 'BPN', 603, '2019-10-05', 1, NULL, NULL, 0),
(1689, 'BPHTB', 603, '2019-10-05', 1, NULL, NULL, 0),
(1690, 'RKAU', 603, '2019-10-05', 1, NULL, NULL, 0),
(1691, 'Sertifikat', 603, '2019-10-05', 1, NULL, NULL, 0),
(1692, 'sertifikasi', 603, '2019-10-05', 1, NULL, NULL, 0),
(1693, '', 604, '2019-10-05', 1, NULL, NULL, 0),
(1694, '', 605, '2019-10-05', 1, NULL, NULL, 0),
(1695, '', 606, '2019-10-05', 1, NULL, NULL, 0),
(1696, '', 607, '2019-10-05', 1, NULL, NULL, 0),
(1697, '', 608, '2019-10-05', 1, NULL, NULL, 0),
(1698, '', 609, '2019-10-05', 1, NULL, NULL, 0),
(1699, '', 610, '2019-10-05', 1, NULL, NULL, 0),
(1700, 'Suara', 611, '2019-10-05', 1, NULL, NULL, 0),
(1701, 'Pelanggan [&&} keluhan', 611, '2019-10-05', 1, NULL, NULL, 0),
(1702, 'SKP', 611, '2019-10-05', 1, NULL, NULL, 0),
(1703, 'SKPP', 611, '2019-10-05', 1, NULL, NULL, 0),
(1704, 'Survey', 612, '2019-10-05', 1, NULL, NULL, 0),
(1705, 'Responden [&&} kepuasan', 612, '2019-10-05', 1, NULL, NULL, 0),
(1706, 'SKP', 612, '2019-10-05', 1, NULL, NULL, 0),
(1707, 'SKPP', 612, '2019-10-05', 1, NULL, NULL, 0),
(1708, '', 613, '2019-10-05', 1, NULL, NULL, 0),
(1709, '', 614, '2019-10-05', 1, NULL, NULL, 0),
(1710, '', 615, '2019-10-05', 1, NULL, NULL, 0),
(1711, 'QPR', 616, '2019-10-05', 1, NULL, NULL, 0),
(1712, 'Monitroring', 616, '2019-10-05', 1, NULL, NULL, 0),
(1713, 'SILM', 616, '2019-10-05', 1, NULL, NULL, 0),
(1714, 'control', 616, '2019-10-05', 1, NULL, NULL, 0),
(1715, '', 617, '2019-10-05', 1, NULL, NULL, 0),
(1716, 'RTM', 618, '2019-10-05', 1, NULL, NULL, 0),
(1717, 'Management', 618, '2019-10-05', 1, NULL, NULL, 0),
(1718, 'Change', 618, '2019-10-05', 1, NULL, NULL, 0),
(1719, 'Monitoring', 618, '2019-10-05', 1, NULL, NULL, 0),
(1720, 'Tindak [lanjut]', 618, '2019-10-05', 1, NULL, NULL, 0),
(1721, '', 619, '2019-10-05', 1, NULL, NULL, 0),
(1722, '', 620, '2019-10-05', 1, NULL, NULL, 0),
(1723, '', 621, '2019-10-05', 1, NULL, NULL, 0),
(1724, 'coba', 622, '2019-10-11', 1, NULL, NULL, 0),
(1725, '', 466, '2019-12-04', 1, NULL, NULL, 0),
(1726, '', 467, '2019-12-04', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `log_probis`
--

CREATE TABLE `log_probis` (
  `id` int(11) NOT NULL,
  `probis` int(11) NOT NULL,
  `tanggal_akses` datetime DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_probis`
--

INSERT INTO `log_probis` (`id`, `probis`, `tanggal_akses`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 2, '2019-05-13 03:14:20', '2019-05-13', 2, NULL, NULL, 0),
(2, 2, '2019-05-14 00:00:00', NULL, NULL, NULL, NULL, 0),
(3, 1, '2019-05-14 00:00:00', NULL, NULL, NULL, NULL, 0),
(4, 2, '2019-05-20 23:57:11', '2019-05-20', 2, NULL, NULL, 0),
(5, 2, '2019-05-20 23:58:04', '2019-05-20', 2, NULL, NULL, 0),
(6, 2, '2019-05-20 23:58:17', '2019-05-20', 2, NULL, NULL, 0),
(7, 3, '2019-05-21 00:19:40', '2019-05-21', 2, NULL, NULL, 0),
(8, 2, '2019-07-15 08:56:04', '2019-07-15', 2, NULL, NULL, 0),
(9, 14, '2019-07-16 14:04:19', '2019-07-16', 2, NULL, NULL, 0),
(10, 14, '2019-07-16 14:04:56', '2019-07-16', 2, NULL, NULL, 0),
(11, 3, '2019-07-16 14:05:11', '2019-07-16', 2, NULL, NULL, 0),
(12, 15, '2019-07-23 08:34:47', '2019-07-23', 2, NULL, NULL, 0),
(13, 195, '2019-07-24 06:22:25', '2019-07-24', 2, NULL, NULL, 0),
(14, 195, '2019-07-24 06:22:39', '2019-07-24', 2, NULL, NULL, 0),
(15, 195, '2019-07-24 06:23:11', '2019-07-24', 2, NULL, NULL, 0),
(16, 466, '2019-10-07 05:52:38', '2019-10-07', 1, NULL, NULL, 0),
(17, 466, '2019-10-07 05:52:44', '2019-10-07', 1, NULL, NULL, 0),
(18, 621, '2019-10-07 05:55:47', '2019-10-07', 1, NULL, NULL, 0),
(19, 466, '2019-10-07 07:36:42', '2019-10-07', 1, NULL, NULL, 0),
(20, 467, '2019-10-07 07:37:05', '2019-10-07', 1, NULL, NULL, 0),
(21, 468, '2019-10-07 07:53:04', '2019-10-07', 1, NULL, NULL, 0),
(22, 470, '2019-10-07 07:53:29', '2019-10-07', 1, NULL, NULL, 0),
(23, 473, '2019-10-07 07:53:38', '2019-10-07', 1, NULL, NULL, 0),
(24, 466, '2019-10-07 07:57:45', '2019-10-07', 2, NULL, NULL, 0),
(25, 499, '2019-10-10 06:48:57', '2019-10-10', 6, NULL, NULL, 0),
(26, 500, '2019-10-10 06:49:04', '2019-10-10', 6, NULL, NULL, 0),
(27, 466, '2019-10-10 06:50:40', '2019-10-10', 6, NULL, NULL, 0),
(28, 622, '2019-10-11 08:44:49', '2019-10-11', 1, NULL, NULL, 0),
(29, 466, '2019-11-05 03:42:41', '2019-11-05', 1, NULL, NULL, 0),
(30, 466, '2019-11-06 19:16:24', '2019-11-06', 1, NULL, NULL, 0),
(31, 466, '2019-11-06 19:16:26', '2019-11-06', 1, NULL, NULL, 0),
(32, 466, '2019-11-06 19:16:39', '2019-11-06', 1, NULL, NULL, 0),
(33, 469, '2019-11-19 03:23:59', '2019-11-19', 6, NULL, NULL, 0),
(34, 467, '2019-11-19 03:24:38', '2019-11-19', 6, NULL, NULL, 0),
(35, 466, '2019-11-19 03:25:36', '2019-11-19', 6, NULL, NULL, 0),
(36, 466, '2019-12-03 08:07:02', '2019-12-03', 6, NULL, NULL, 0),
(37, 466, '2019-12-04 08:30:57', '2019-12-04', 1, NULL, NULL, 0),
(38, 466, '2019-12-04 08:30:59', '2019-12-04', 1, NULL, NULL, 0),
(39, 475, '2019-12-04 08:31:08', '2019-12-04', 1, NULL, NULL, 0),
(40, 466, '2019-12-04 08:31:21', '2019-12-04', 1, NULL, NULL, 0),
(41, 466, '2019-12-04 08:31:24', '2019-12-04', 1, NULL, NULL, 0),
(42, 466, '2019-12-04 08:31:58', '2019-12-04', 1, NULL, NULL, 0),
(43, 466, '2019-12-04 08:32:10', '2019-12-04', 1, NULL, NULL, 0),
(44, 466, '2019-12-04 08:33:15', '2019-12-04', 1, NULL, NULL, 0),
(45, 467, '2019-12-04 09:49:45', '2019-12-04', 1, NULL, NULL, 0),
(46, 476, '2019-12-04 19:40:20', '2019-12-04', 1, NULL, NULL, 0),
(47, 466, '2019-12-04 19:40:27', '2019-12-04', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `path_probis`
--

CREATE TABLE `path_probis` (
  `no` varchar(50) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `child_first` int(11) DEFAULT NULL,
  `child_second` int(11) DEFAULT NULL,
  `child_third` varchar(50) DEFAULT NULL,
  `probis` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `path_probis`
--

INSERT INTO `path_probis` (`no`, `id`, `parent`, `child_first`, `child_second`, `child_third`, `probis`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(NULL, 465, 1, 0, 0, '0', 466, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 466, 1, 1, 0, '0', 467, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 467, 1, 1, 1, '0', 468, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 468, 1, 1, 2, '0', 469, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 469, 1, 1, 3, '0', 470, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 470, 1, 1, 4, '0', 471, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 471, 1, 1, 5, '0', 472, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 472, 1, 1, 6, '0', 473, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 473, 1, 1, 7, '0', 474, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 474, 2, 0, 0, '0', 475, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 475, 2, 1, 0, '0', 476, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 476, 2, 1, 1, '0', 477, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 477, 2, 1, 2, '0', 478, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 478, 2, 1, 3, '0', 479, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 479, 2, 1, 4, '0', 480, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 480, 2, 1, 5, '0', 481, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 481, 2, 1, 6, '0', 482, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 482, 2, 2, 0, '0', 483, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 483, 2, 2, 1, '0', 484, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 484, 2, 2, 2, '0', 485, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 485, 2, 2, 3, '0', 486, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 486, 2, 2, 4, '0', 487, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 487, 2, 2, 5, '0', 488, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 488, 2, 2, 6, '0', 489, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 489, 2, 2, 7, '0', 490, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 490, 2, 2, 8, '0', 491, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 491, 2, 2, 9, '0', 492, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 492, 2, 2, 10, '0', 493, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 493, 2, 2, 11, '0', 494, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 494, 2, 2, 12, '0', 495, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 495, 2, 2, 13, '0', 496, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 496, 2, 2, 14, '0', 497, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 497, 2, 2, 15, '0', 498, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 498, 2, 2, 16, '0', 499, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 499, 2, 2, 17, '0', 500, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 500, 2, 2, 18, '0', 501, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 501, 2, 3, 0, '0', 502, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 502, 2, 3, 1, '0', 503, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 503, 2, 3, 1, '0', 504, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 504, 2, 3, 2, '0', 505, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 505, 2, 3, 3, '0', 506, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 506, 2, 3, 4, '0', 507, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 507, 2, 3, 5, '0', 508, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 508, 2, 3, 6, '0', 509, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 509, 2, 4, 0, '0', 510, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 510, 2, 4, 1, '0', 511, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 511, 2, 4, 2, '0', 512, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 512, 2, 4, 3, '0', 513, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 513, 2, 4, 4, '0', 514, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 514, 2, 4, 5, '0', 515, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 515, 2, 5, 0, '0', 516, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 516, 2, 5, 1, '0', 517, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 517, 2, 5, 2, '0', 518, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 518, 2, 5, 3, '0', 519, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 519, 2, 5, 4, '0', 520, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 520, 2, 5, 5, '0', 521, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 521, 2, 5, 6, '0', 522, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 522, 3, 0, 0, '0', 523, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 523, 3, 1, 0, '0', 524, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 524, 3, 1, 1, '0', 525, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 525, 3, 1, 2, '0', 526, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 526, 3, 1, 3, '0', 527, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 527, 3, 1, 4, '0', 528, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 528, 3, 1, 5, '0', 529, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 529, 3, 1, 6, '0', 530, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 530, 3, 1, 7, '0', 531, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 531, 3, 1, 8, '0', 532, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 532, 3, 1, 9, '0', 533, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 533, 3, 1, 10, '0', 534, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 534, 3, 1, 11, '0', 535, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 535, 3, 2, 0, '0', 536, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 536, 3, 2, 1, '0', 537, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 537, 3, 2, 2, '0', 538, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 538, 3, 2, 3, '0', 539, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 539, 3, 2, 4, '0', 540, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 540, 3, 2, 5, '0', 541, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 541, 3, 2, 6, '0', 542, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 542, 3, 3, 0, '0', 543, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 543, 3, 3, 1, '0', 544, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 544, 3, 3, 2, '0', 545, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 545, 3, 3, 3, '0', 546, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 546, 3, 3, 4, '0', 547, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 547, 3, 3, 5, '0', 548, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 548, 3, 3, 6, '0', 549, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 549, 3, 3, 7, '0', 550, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 550, 3, 3, 8, '0', 551, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 551, 3, 3, 9, '0', 552, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 552, 3, 3, 10, '0', 553, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 553, 3, 3, 11, '0', 554, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 554, 3, 4, 0, '0', 555, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 555, 3, 4, 1, '0', 556, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 556, 3, 4, 2, '0', 557, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 557, 3, 4, 3, '0', 558, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 558, 3, 4, 4, '0', 559, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 559, 4, 0, 0, '0', 560, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 560, 4, 1, 0, '0', 561, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 561, 4, 2, 0, '0', 562, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 562, 4, 2, 1, '0', 563, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 563, 4, 2, 2, '0', 564, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 564, 4, 2, 3, '0', 565, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 565, 4, 2, 4, '0', 566, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 566, 4, 2, 5, '0', 567, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 567, 4, 2, 6, '0', 568, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 568, 4, 2, 7, '0', 569, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 569, 4, 3, 0, '0', 570, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 570, 4, 3, 1, '0', 571, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 571, 4, 3, 2, '0', 572, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 572, 4, 3, 3, '0', 573, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 573, 4, 3, 4, '0', 574, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 574, 4, 3, 5, '0', 575, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 575, 4, 3, 6, '0', 576, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 576, 4, 3, 7, '0', 577, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 577, 4, 3, 8, '0', 578, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 578, 4, 3, 9, '0', 579, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 579, 4, 3, 10, '0', 580, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 580, 4, 3, 11, '0', 581, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 581, 4, 3, 12, '0', 582, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 582, 4, 3, 13, '0', 583, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 583, 4, 4, 0, '0', 584, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 584, 4, 4, 1, '0', 585, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 585, 4, 4, 2, '0', 586, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 586, 4, 4, 3, '0', 587, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 587, 4, 4, 4, '0', 588, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 588, 4, 4, 5, '0', 589, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 589, 4, 4, 6, '0', 590, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 590, 4, 4, 7, '0', 591, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 591, 4, 4, 8, '0', 592, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 592, 4, 4, 9, '0', 593, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 593, 4, 4, 10, '0', 594, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 594, 4, 4, 11, '0', 595, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 595, 4, 4, 12, '0', 596, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 596, 4, 4, 13, '0', 597, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 597, 4, 5, 0, '0', 598, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 598, 4, 6, 0, '0', 599, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 599, 4, 6, 1, '0', 600, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 600, 4, 6, 2, '0', 601, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 601, 4, 6, 3, '0', 602, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 602, 4, 6, 4, '0', 603, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 603, 4, 6, 5, '0', 604, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 604, 4, 6, 6, '0', 605, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 605, 4, 7, 0, '0', 606, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 606, 4, 7, 1, '0', 607, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 607, 4, 7, 2, '0', 608, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 608, 4, 8, 0, '0', 609, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 609, 4, 8, 1, '0', 610, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 610, 4, 8, 2, '0', 611, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 611, 4, 8, 3, '0', 612, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 612, 4, 9, 0, '0', 613, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 613, 5, 0, 0, '0', 614, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 614, 5, 1, 0, '0', 615, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 615, 5, 1, 1, '0', 616, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 616, 5, 2, 0, '0', 617, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 617, 5, 3, 0, '0', 618, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 618, 5, 4, 0, '0', 619, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 619, 5, 5, 0, '0', 620, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 620, 5, 6, 0, '0', 621, '2019-10-05', 1, NULL, NULL, 0),
(NULL, 621, 2, 2, 19, NULL, 622, '2019-10-11', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `nama_pegawai` varchar(150) DEFAULT NULL,
  `email` text,
  `nip` varchar(150) DEFAULT NULL,
  `upt` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id`, `user`, `nama_pegawai`, `email`, `nip`, `upt`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 2, 'Dodik Rismawan Affrudin', 'dodikitn@gmail.com', '1318033', 6, '2019-04-22', 1, '2019-05-11 05:31:15', 1, 0),
(2, 3, 'Bejo', NULL, '1318031', 6, '2019-04-22', 1, NULL, NULL, 0),
(3, 4, 'Tedi', 'tedi@gmail.com', '1318032', 2, '2019-07-24', 1, NULL, NULL, 0),
(4, 5, 'Adit', 'adit@gmail.com', '1318034', 2, '2019-07-24', 1, NULL, NULL, 0),
(5, 6, 'general', 'general@gmail.com', '12', 6, '2019-10-10', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pengajuan_probis`
--

CREATE TABLE `pengajuan_probis` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `upt` int(11) NOT NULL,
  `no_probis` varchar(150) DEFAULT NULL,
  `nama_probis` varchar(150) DEFAULT NULL,
  `file_pdf` varchar(255) DEFAULT NULL,
  `file_excel` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengajuan_probis`
--

INSERT INTO `pengajuan_probis` (`id`, `user`, `upt`, `no_probis`, `nama_probis`, `file_pdf`, `file_excel`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(3, 3, 3, 'USULAN19APR001', 'Usulan Probis A', 'invoice.pdf', 'invoice.xlsx', '2019-04-24', 3, NULL, NULL, 0),
(4, 3, 3, 'USULAN19APR002', 'Usulan Probis B', '1554947702103_Proses Bisnis.pdf', 'SIKEDIP.xls', '2019-04-27', 3, '2019-04-27 04:18:45', 3, 0),
(5, 3, 3, 'USULAN19JUL001', 'Probis Coba', NULL, NULL, '2019-07-17', 3, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pengajuan_probis_has_status`
--

CREATE TABLE `pengajuan_probis_has_status` (
  `id` int(11) NOT NULL,
  `pengajuan_probis` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT 'APPROVED\nREJECTED\nREVIEWED\nDRAFT',
  `keterangan` text NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengajuan_probis_has_status`
--

INSERT INTO `pengajuan_probis_has_status` (`id`, `pengajuan_probis`, `user`, `status`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`) VALUES
(3, 3, 3, 'DRAFT', '', '2019-04-24', 3, NULL, NULL),
(5, 3, 2, 'REVIEWED', '', '2019-04-24', 2, NULL, NULL),
(7, 3, 2, 'APPROVED', '', '2019-04-26', 2, NULL, NULL),
(9, 4, 3, 'DRAFT', '', '2019-04-27', 3, NULL, NULL),
(10, 4, 2, 'REVIEWED', '', '2019-04-27', 2, NULL, NULL),
(11, 4, 2, 'REVISION', 'Tidak Sesuai Mohon Perbaiki', '2019-04-27', 2, NULL, NULL),
(12, 4, 3, 'HAS REVISION', '', '2019-04-27', 3, NULL, NULL),
(13, 4, 2, 'REJECTED', 'Maaf Tidak Bisa', '2019-04-27', 2, NULL, NULL),
(14, 5, 3, 'DRAFT', '', '2019-07-17', 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `priveledge`
--

CREATE TABLE `priveledge` (
  `id` int(11) NOT NULL,
  `hak_akses` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `priveledge`
--

INSERT INTO `priveledge` (`id`, `hak_akses`) VALUES
(1, 'Superadmin'),
(2, 'Admin'),
(3, 'Requestor'),
(4, 'General');

-- --------------------------------------------------------

--
-- Table structure for table `probis`
--

CREATE TABLE `probis` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `upt` int(11) NOT NULL,
  `nama_probis` varchar(255) DEFAULT NULL,
  `no_probis` varchar(150) DEFAULT NULL,
  `file_excel` varchar(255) DEFAULT NULL,
  `file_pdf` varchar(255) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL,
  `edisi` int(11) NOT NULL DEFAULT '0',
  `revisi` int(11) NOT NULL DEFAULT '0',
  `pengajuan_probis` int(11) NOT NULL DEFAULT '0',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `probis`
--

INSERT INTO `probis` (`id`, `user`, `upt`, `nama_probis`, `no_probis`, `file_excel`, `file_pdf`, `tgl_update`, `tahun`, `edisi`, `revisi`, `pengajuan_probis`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(2, 2, 3, 'Probis A', 'PROBIS19APR001', 'SIKEDIP.xls', 'FITUR APLIKASI PROBIS.pdf', NULL, NULL, 0, 0, 0, '2019-04-22', 2, '2019-05-20 23:57:10', 2, 1),
(3, 2, 3, 'Usulan Probis A', 'PROBIS19APR002', 'invoice.xlsx', 'invoice.pdf', '2019-07-16', NULL, 0, 0, 3, '2019-04-26', 2, '2019-07-16 14:05:11', 2, 1),
(13, 1, 2, 'Probis Contoh Satu', 'PROBIS/2019', NULL, NULL, NULL, NULL, 0, 0, 0, '2019-07-15', 1, NULL, NULL, 1),
(14, 2, 3, 'Probis Coba', 'PROBIS19JUL001', NULL, NULL, '2019-07-16', NULL, 0, 0, 0, '2019-07-16', 2, NULL, NULL, 1),
(466, 1, 6, 'Proses Bisnis Perencanaan', 'PROBIS19OCT001', NULL, '3505090512940001_kartuDaftar.pdf', '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, '2019-12-04 08:31:21', 1, 0),
(467, 1, 6, 'Proses Bisnis Penyusunan Visi, Misi, Perencanaan strategis, dan RKAU', 'PROBIS19OCT002', NULL, '3505090512940001_kartuAkun.pdf', '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, '2019-12-04 09:49:45', 1, 0),
(468, 1, 6, 'Proses Bisnis Penyusunan Visi', 'PROBIS19OCT003', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(469, 1, 6, 'Proses Bisnis Penetapan dan/atau Pembaharuan Kebijakan Manajemen Aset', 'PROBIS19OCT004', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(470, 1, 6, 'Proses Bisnis Penyusunan dan/atau Review RJP', 'PROBIS19OCT005', NULL, NULL, '2019-01-31', 2019, 1, 1, 0, '2019-10-05', 1, NULL, NULL, 0),
(471, 1, 6, 'Proses Bisnis Penetapan KPI', 'PROBIS19OCT006', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(472, 1, 6, 'Proses Bisnis Penyusunan RKAU', 'PROBIS19OCT007', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(473, 1, 6, 'Proses Bisnis Penyusunan dan/atau Review Risk Profile', 'PROBIS19OCT008', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(474, 1, 6, 'Proses Bisnis Penyusunan dan/atau Review Proses Bisnis', 'PROBIS19OCT009', NULL, NULL, '2019-02-01', 2019, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(475, 1, 6, 'Proses Bisnis Manajemen Proyek', 'PROBIS19OCT010', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(476, 1, 6, 'Proses Bisnis Perencanaan Manajemen Proyek', 'PROBIS19OCT011', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(477, 1, 6, 'Proses Bisnis Pra Enjiniring', 'PROBIS19OCT012', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(478, 1, 6, 'Proses Bisnis Penyusunan SKKI', 'PROBIS19OCT013', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(479, 1, 6, 'Proses Bisnis Penyusunan SKKO', 'PROBIS19OCT014', NULL, NULL, '2019-01-07', 0, 1, 1, 0, '2019-10-05', 1, NULL, NULL, 0),
(480, 1, 6, 'Proses Bisnis Kegiatan Enjiniring untuk SKKI', 'PROBIS19OCT015', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(481, 1, 6, 'Proses Bisnis Kegiatan Enjiniring untuk SKKO - KI', 'PROBIS19OCT016', NULL, NULL, '2019-01-07', 0, 1, 1, 0, '2019-10-05', 1, NULL, NULL, 0),
(482, 1, 6, 'Proses Bisnis Perencanaan Pengadaan', 'PROBIS19OCT017', NULL, NULL, '2019-04-10', 0, 1, 1, 0, '2019-10-05', 1, NULL, NULL, 0),
(483, 1, 6, 'Proses Bisnis Pelaksanaan Manajemen Proyek', 'PROBIS19OCT018', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(484, 1, 6, 'Proses Bisnis Pelaksanaan Pegadaan Lelang Terbatas', 'PROBIS19OCT019', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(485, 1, 6, 'Proses Bisnis Pelaksanaan Pegadaan Penunjukan Langsung', 'PROBIS19OCT020', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(486, 1, 6, 'Proses Bisnis Pelaksanaan Pegadaan Pengadaan Langsung', 'PROBIS19OCT021', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(487, 1, 6, 'Proses Bisnis Pelaksanaan Pegadaan Pelelangan Terbuka', 'PROBIS19OCT022', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(488, 1, 6, 'Proses Bisnis Pelaksanaan Pekerjaan Approval dokumen dengan penyedia barang/jasa atau langsung dengan pabrikan', 'PROBIS19OCT023', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(489, 1, 6, 'Proses Bisnis Pelaksanaan Pekerjaan Approval Dokumen dengan Konsultan', 'PROBIS19OCT024', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(490, 1, 6, 'Proses Bisnis Pelaksanaan Pekerjaan Supply Erect', 'PROBIS19OCT025', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(491, 1, 6, 'Proses Bisnis Pelaksanaan Pekerjaan Supply Only dalam Negeri', 'PROBIS19OCT026', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(492, 1, 6, 'Proses Bisnis Pelaksanaan Pekerjaan Supply Only luar Negeri', 'PROBIS19OCT027', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(493, 1, 6, 'Proses Bisnis Pelaksanaan Pekerjaan Supply Only Luar Negeri - LC', 'PROBIS19OCT028', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(494, 1, 6, 'Proses Bisnis Pelaksanaan Pekerjaan Erect Only', 'PROBIS19OCT029', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(495, 1, 6, 'Proses Bisnis Costum Clearance', 'PROBIS19OCT030', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(496, 1, 6, 'Proses Bisnis Verifikasi Bank Garansi (Jaminan Penawaran, Jaminan Pelaksanaan dan jaminan pemeliharaan)', 'PROBIS19OCT031', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(497, 1, 6, 'Proses Bisnis Penunjukkan Langsung Emergency', 'PROBIS19OCT032', NULL, NULL, '2018-10-02', 2018, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(498, 1, 6, 'Proses Bisnis Pelaksanaan Kontrak Kesepakatan Harga Satuan (KHS) UIT - sebagai User', 'PROBIS19OCT033', NULL, NULL, '2018-10-02', 2018, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(499, 1, 8, 'Proses Bisnis Pelaksanaan Kontrak Kesepakatan Harga Satuan (KHS) UIT - Supply Erect', 'PROBIS19OCT034', NULL, NULL, '2019-07-30', 2019, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(500, 1, 8, 'Proses Bisnis Pelaksanaan Kontrak Kesepakatan Harga Satuan (KHS) UIT - Material Supply Only', 'PROBIS19OCT035', NULL, NULL, '2019-07-30', 2019, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(501, 1, 8, 'Proses Bisnis Pengadaan & Pengendalian Kontrak Yang Langsung Berhubungan Dengan User (Jasa Konsultasi )', 'PROBIS19OCT036', NULL, NULL, '2019-07-30', 2019, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(502, 1, 6, 'Proses Bisnis Pengendalian Manajemen Proyek', 'PROBIS19OCT037', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(503, 1, 6, 'Proses Bisnis Pengendalian dan administrasi Pekerjaan', 'PROBIS19OCT038', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(504, 1, 6, 'Proses Bisnis Pengendalian dan administrasi Pekerjaan - Anggaran Operasi Kantor Induk', 'PROBIS19OCT039', NULL, NULL, '2019-01-07', 2019, 1, 1, 0, '2019-10-05', 1, NULL, NULL, 0),
(505, 1, 6, 'Proses Bisnis Pemantauan pada masa pemeliharaan', 'PROBIS19OCT040', NULL, NULL, '2019-01-07', 2019, 1, 1, 0, '2019-10-05', 1, NULL, NULL, 0),
(506, 1, 6, 'Proses Bisnis Pemantauan progress  pekerjaan dan pengelolaan risiko', 'PROBIS19OCT041', NULL, NULL, '2019-01-07', 2019, 1, 1, 0, '2019-10-05', 1, NULL, NULL, 0),
(507, 1, 6, 'Proses Bisnis Pelaporan pada stakeholder', 'PROBIS19OCT042', NULL, NULL, '2019-01-07', 2019, 1, 1, 0, '2019-10-05', 1, NULL, NULL, 0),
(508, 1, 6, 'Proses Bisnis Monitoring Progress Pekerjaan dan Realisasi Pembayaran Anggaran Investasi (AI)', 'PROBIS19OCT043', NULL, NULL, '2019-04-25', 2019, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(509, 1, 6, 'Proses Bisnis Kegiatan Implementasi Aplikasi Project Management Office (PMO)', 'PROBIS19OCT044', NULL, NULL, '2019-02-26', 2019, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(510, 1, 6, 'Proses Bisnis Pengelolaan RKAU', 'PROBIS19OCT045', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(511, 1, 6, 'Proses Bisnis Revisi SKKI', 'PROBIS19OCT046', NULL, NULL, '2018-03-27', 2018, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(512, 1, 6, 'Proses Bisnis Penambahan / likuiditas program kerja operasional', 'PROBIS19OCT047', NULL, NULL, '2018-03-09', 2018, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(513, 1, 6, 'Proses Bisnis Pengalihan program kerja Operasional (pengalihan disburse)', 'PROBIS19OCT048', NULL, NULL, '2018-03-09', 2018, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(514, 1, 6, 'Proses Bisnis Pengalihan / penggantian program kerja Operasional', 'PROBIS19OCT049', NULL, NULL, '2018-03-09', 2018, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(515, 1, 8, 'Proses Bisnis Perubahan Alokasi Anggaran Bidang Lain', 'PROBIS19OCT050', NULL, NULL, '2019-08-20', 2019, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(516, 1, 6, 'Proses Bisnis Penyelesaian Manajemen Proyek', 'PROBIS19OCT051', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(517, 1, 6, 'Proses Bisnis Kegiatan Pembayaran Pekerjaan Pengadaan Barang dan Jasa (termasuk pembayaran per termin) - KI', 'PROBIS19OCT052', NULL, NULL, '2019-01-07', 2019, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(518, 1, 6, 'Proses Bisnis Pembayaran L/C', 'PROBIS19OCT053', NULL, NULL, '2017-08-16', 2018, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(519, 1, 6, 'Proses Bisnis Pembayaran CIF', 'PROBIS19OCT054', NULL, NULL, '2017-08-16', 2018, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(520, 1, 6, 'Proses Bisnis Pengelolaan DPT (Daftar Penyedia Terseleksi)', 'PROBIS19OCT055', NULL, NULL, '2019-08-21', 2019, 1, 1, 0, '2019-10-05', 1, NULL, NULL, 0),
(521, 1, 6, 'Proses Bisnis Penyusunan lesson learned', 'PROBIS19OCT056', NULL, NULL, '2019-08-21', 2019, 1, 1, 0, '2019-10-05', 1, NULL, NULL, 0),
(522, 1, 8, 'Proses Bisnis Tagihan Alihdaya', 'PROBIS19OCT057', NULL, NULL, '2019-08-21', 2019, 1, 1, 0, '2019-10-05', 1, NULL, NULL, 0),
(523, 1, 6, 'Proses Bisnis Pengelolaan Sistem Transmisi', 'PROBIS19OCT058', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(524, 1, 6, 'Proses Bisnis Aset Register', 'PROBIS19OCT059', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(525, 1, 6, 'Proses Bisnis Pembuatan buku SOP Pengoperasian GI dan Pedoman Komunikasi', 'PROBIS19OCT060', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(526, 1, 6, 'Proses Bisnis Penetapan dan evaluasi setting proteksi bay baru', 'PROBIS19OCT061', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(527, 1, 6, 'Proses Bisnis Penambahan data GI/Bay baru melalui aplikasi CBM - SAP', 'PROBIS19OCT062', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(528, 1, 6, 'Proses Bisnis Penetapan dan evaluasi setting proteksi bay uprating', 'PROBIS19OCT063', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(529, 1, 6, 'Proses Bisnis Penambahan/ penggantian data peralatan melalui aplikasi CBM - SAP', 'PROBIS19OCT064', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(530, 1, 6, 'Proses Bisnis Penyusunan/Update Buku Petunjuk Pengoperasian Gardu Induk', 'PROBIS19OCT065', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(531, 1, 6, 'Proses Bisnis Energize', 'PROBIS19OCT066', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(532, 1, 6, 'Proses Bisnis Sertifikat Laik Operasi', 'PROBIS19OCT067', NULL, NULL, '2019-02-28', 0, 1, 1, 0, '2019-10-05', 1, NULL, NULL, 0),
(533, 1, 6, 'Proses Bisnis Kapitalisasi aset', 'PROBIS19OCT068', NULL, NULL, '2017-08-16', 2018, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(534, 1, 6, 'Proses Bisnis Inventarisasi Fisik Aktiva Tetap, PDP, Persediaan Material dan ATTB', 'PROBIS19OCT069', NULL, NULL, '2019-04-01', 2019, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(535, 1, 6, 'Proses Bisnis First Years Inspection Bay Trafo dan Bay Penghantar', 'PROBIS19OCT070', NULL, NULL, '2019-08-26', 2019, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(536, 1, 6, 'Proses Bisnis Operasi dan Kontingensi', 'PROBIS19OCT071', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(537, 1, 6, 'Proses Bisnis Investigasi gangguan Gardu Induk & Transmisi', 'PROBIS19OCT072', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(538, 1, 6, 'Proses Bisnis Investigasi gangguan Transmisi berbasis DFR', 'PROBIS19OCT073', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(539, 1, 6, 'Proses Bisnis Investigasi gangguan Trafo berbasis DFR', 'PROBIS19OCT074', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(540, 1, 6, 'Proses Bisnis Manajemen Data gangguan melalui aplikasi FOIS', 'PROBIS19OCT075', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(541, 1, 6, 'Proses Bisnis Manajemen Data anomali emergensi  melalui aplikasi CBM', 'PROBIS19OCT076', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(542, 1, 6, 'Proses Bisnis Penyusunan Transmission Service Agreement (TSA)', 'PROBIS19OCT077', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(543, 1, 6, 'Proses Bisnis Perencanaan, Pelaksanaan, dan Evaluasi  hasil Pemeliharaan', 'PROBIS19OCT078', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(544, 1, 6, 'Proses Bisnis Perencanaan Pemeliharaan', 'PROBIS19OCT079', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(545, 1, 6, 'Proses Bisnis Pelaksanaan dan Evaluasi hasil Pemeliharaan Gardu Induk dan Transmisi', 'PROBIS19OCT080', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(546, 1, 6, 'Proses Bisnis Permintaan material', 'PROBIS19OCT081', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(547, 1, 6, 'Proses Bisnis Penanganan gangguan dan anomali emergensi', 'PROBIS19OCT082', NULL, NULL, '2017-09-18', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(548, 1, 6, 'Proses Bisnis Evaluasi setting proteksi bay eksisting', 'PROBIS19OCT083', NULL, NULL, '2017-09-18', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(549, 1, 6, 'Proses Bisnis Tindak Lanjut: Penggantian peralatan karena kondisi', 'PROBIS19OCT084', NULL, NULL, '2017-09-18', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(550, 1, 6, 'Proses Bisnis Tindak Lanjut: Penggantian peralatan karena rusak', 'PROBIS19OCT085', NULL, NULL, '2017-09-18', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(551, 1, 6, 'Proses Bisnis Pelaksanaan dan Monitoring Assesmen Trafo', 'PROBIS19OCT086', NULL, NULL, '2018-12-21', 2018, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(552, 1, 6, 'Proses Bisnis Klaim Gangguan APPL', 'PROBIS19OCT087', NULL, NULL, '2019-01-23', 2019, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(553, 1, 6, 'Proses Bisnis Kegiatan Monitoring Pemasangan Trafo dan MTU Kritis', 'PROBIS19OCT088', NULL, NULL, '2019-01-23', 2019, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(554, 1, 8, 'Proses Bisnis Monitoring Dan Evaluasi Pengelolaan Alih Daya dan SLA', 'PROBIS19OCT089', NULL, NULL, '2019-09-10', 2019, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(555, 1, 6, 'Proses Bisnis Disposal', 'PROBIS19OCT090', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(556, 1, 6, 'Proses Bisnis Penon-aktifan data bay atau GI melalui aplikasi CBM', 'PROBIS19OCT091', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(557, 1, 6, 'Proses Bisnis Penon-aktifan data peralatan melalui aplikasi CBM', 'PROBIS19OCT092', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(558, 1, 6, 'Proses Bisnis Mutasi keluar/relokasi aset atas permintaan pihak eksternal', 'PROBIS19OCT093', NULL, NULL, '2017-11-02', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(559, 1, 6, 'Proses Bisnis Mutasi keluar aset', 'PROBIS19OCT094', NULL, NULL, '2017-11-02', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(560, 1, 6, 'Proses Bisnis Penunjang', 'PROBIS19OCT095', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(561, 1, 6, 'Proses Bisnis Pengelolaan LK2 dan Keamanan', 'PROBIS19OCT096', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(562, 1, 6, 'Proses Bisnis Pengelolaan SDM', 'PROBIS19OCT097', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(563, 1, 6, 'Proses Bisnis Perencanaan Tenaga Kerja', 'PROBIS19OCT098', NULL, NULL, '1970-01-01', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(564, 1, 6, 'Proses Bisnis Pengelolaan Kompetensi Pegawai melalui Individual Training Need', 'PROBIS19OCT099', NULL, NULL, '1970-01-01', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(565, 1, 6, 'Proses Bisnis Pengelolaan Kompetensi Pegawai berbasis Kinerja', 'PROBIS19OCT100', NULL, NULL, '1970-01-01', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(566, 1, 6, 'Proses Bisnis Learning Need Analysis', 'PROBIS19OCT101', NULL, NULL, '1970-01-01', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(567, 1, 6, 'Proses Bisnis Knowledge Management', 'PROBIS19OCT102', NULL, NULL, '1970-01-01', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(568, 1, 6, 'Proses Bisnis Pengelolaan Sistem Tenaga Kerja', 'PROBIS19OCT103', NULL, NULL, '1970-01-01', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(569, 1, 6, 'Proses Bisnis Sistem Manajemen Kinerja Pegawai', 'PROBIS19OCT104', NULL, NULL, '1970-01-01', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(570, 1, 6, 'Proses Bisnis Pengelolaan Logistik', 'PROBIS19OCT105', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(571, 1, 6, 'Proses Bisnis Kegiatan Tugas Tata kelola pergudangan', 'PROBIS19OCT106', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(572, 1, 6, 'Proses Bisnis Penerimaan barang dari fungsi pembelian', 'PROBIS19OCT107', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(573, 1, 6, 'Proses Bisnis Penggunaan material dalam 1 APP', 'PROBIS19OCT108', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(574, 1, 6, 'Proses Bisnis Langkah Kerja Perpindahan material antar APP dalam TJBTB', 'PROBIS19OCT109', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(575, 1, 6, 'Proses Bisnis Langkah kerja Proses permintaan material dari luar TJBTB', 'PROBIS19OCT110', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(576, 1, 6, 'Proses Bisnis Relokasi Eks Bongkaran /ATTB', 'PROBIS19OCT111', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(577, 1, 6, 'Proses Bisnis Penerimaan Barang Sisa Pemakaian', 'PROBIS19OCT112', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(578, 1, 6, 'Proses Bisnis Penerimaan Barang Aset Bongkaran Pekerjaan Uprating/Rekondukturing/Penggantian Peralatan Terkait Pemeliharaan', 'PROBIS19OCT113', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(579, 1, 6, 'Proses Bisnis Pengeluaran Barang untuk Perbaikan', 'PROBIS19OCT114', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(580, 1, 6, 'Proses Bisnis Pengeluaran Barang untuk Penghapusan', 'PROBIS19OCT115', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(581, 1, 6, 'Proses Bisnis Pengeluaran Barang untuk Pengembalian ke Penyedia Barang (Masa Garansi)', 'PROBIS19OCT116', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(582, 1, 6, 'Proses Bisnis Tugas Pemeriksaan fisik barang SAP MM (stock opname)', 'PROBIS19OCT117', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(583, 1, 6, 'Proses Bisnis Tugas Permintaan material', 'PROBIS19OCT118', NULL, NULL, '2017-08-16', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(584, 1, 6, 'Proses Bisnis Pengelolaan Fasilitas Pemeliharaan', 'PROBIS19OCT119', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(585, 1, 6, 'Proses Bisnis Kegiatan  Manajemen pelayanan pelanggan pengujian HV 500 kV internal TJBTB', 'PROBIS19OCT120', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(586, 1, 6, 'Proses Bisnis Kegiatan  Manajemen pelayanan pelanggan pengujian HV 500 kV eksternal TJBTB', 'PROBIS19OCT121', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(587, 1, 6, 'Proses Bisnis Pengelolaan Laboratorium Minyak Pelayanan Pelanggan', 'PROBIS19OCT122', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(588, 1, 6, 'Proses Bisnis Pengelolaan Laboratorium Minyak Pengelolaan operasional Lab', 'PROBIS19OCT123', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(589, 1, 6, 'Proses Bisnis Kegiatan  Manajemen pelayanan pelanggan pekerjaan alat berat internal TJBTB', 'PROBIS19OCT124', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(590, 1, 6, 'Proses Bisnis Kegiatan  Manajemen pelayanan pelanggan pekerjaan alat berat eksternal TJBTB', 'PROBIS19OCT125', NULL, NULL, '2017-09-20', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(591, 1, 6, 'Proses Bisnis Kegiatan  Manajemen pengelolaan trafo mobile internal TJBTB', 'PROBIS19OCT126', NULL, NULL, '2017-09-20', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(592, 1, 6, 'Proses Bisnis Kegiatan  Manajemen pengelolaan trafo mobile eksternal TJBTB', 'PROBIS19OCT127', NULL, NULL, '2017-09-20', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(593, 1, 6, 'Proses Bisnis Kegiatan  Manajemen pelayanan pelanggan pekerjaan PDKB internal TJBTB', 'PROBIS19OCT128', NULL, NULL, '2017-09-20', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(594, 1, 6, 'Proses Bisnis Kegiatan  Manajemen pelayanan pelanggan pekerjaan PDKB eksternal TJBTB', 'PROBIS19OCT129', NULL, NULL, '2017-09-20', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(595, 1, 6, 'Proses Bisnis Kegiatan Sertifikasi Kalibrasi Pengujian Peralatan', 'PROBIS19OCT130', NULL, NULL, '2017-09-20', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(596, 1, 6, 'Proses Bisnis Kegiatan Manajemen pelayanan pelanggan peminjaman alat uji', 'PROBIS19OCT131', NULL, NULL, '2017-09-20', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(597, 1, 6, 'Proses Bisnis Kegiatan Pengelolaan alat uji', 'PROBIS19OCT132', NULL, NULL, '2017-09-20', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(598, 1, 6, 'Proses Bisnis Pengelolaan ICT', 'PROBIS19OCT133', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(599, 1, 6, 'Proses Bisnis Pengelolaan Administrasi Umum dan Fasilitas', 'PROBIS19OCT134', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(600, 1, 6, 'Proses bisnis Perpanjangan IPPKH', 'PROBIS19OCT135', NULL, NULL, '2018-11-22', 2018, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(601, 1, 6, 'Proses Bisnis Pengadaan tanah untuk kepentingan umum kurang dari 5 Ha tanpa perubahan Jalur', 'PROBIS19OCT136', NULL, NULL, '2018-11-22', 2018, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(602, 1, 6, 'Proses Bisnis Sertifikasi tanah KI', 'PROBIS19OCT137', NULL, NULL, '2019-07-04', 2019, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(603, 1, 6, 'Proses Bisnis Sertifikasi tanah UPT', 'PROBIS19OCT138', NULL, NULL, '2019-07-04', 2019, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(604, 1, 8, 'Proses Bisnis Pelaksanaan Kegiatan CSR', 'PROBIS19OCT139', NULL, NULL, '2019-08-05', 2019, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(605, 1, 8, 'Proses Bisnis Pengelolaan Saluran Komunikasi', 'PROBIS19OCT140', NULL, NULL, '2019-08-20', 2019, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(606, 1, 6, 'Proses Bisnis Pengelolaan Anggaran dan Keuangan', 'PROBIS19OCT141', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(607, 1, 8, 'Proses Bisnis Pengajuan Persekot Dinas', 'PROBIS19OCT142', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(608, 1, 8, 'Proses Bisnis Laporan Petanggungjawaban  Persekot Dinas', 'PROBIS19OCT143', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(609, 1, 6, 'Proses Bisnis Pengelolaan Dokumen dan Informasi', 'PROBIS19OCT144', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(610, 1, 6, 'Proses Bisnis Pengelolaan Keluhan Pelanggan APP - KI', 'PROBIS19OCT145', NULL, NULL, '2018-06-04', 2018, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(611, 1, 6, 'Proses Bisnis Pengelolaan Suara Pelanggan APP  - KI', 'PROBIS19OCT146', NULL, NULL, '2019-09-17', 2018, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(612, 1, 6, 'Proses Bisnis Survey Kepuasan Pelanggan (SKP) dan Survey Ketidakpuasan Pelanggan (SKPP)', 'PROBIS19OCT147', NULL, NULL, '2019-09-17', 2018, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(613, 1, 6, 'Proses Bisnis Pengelolaan Hubungan Internal dan Eksternal Manajemen', 'PROBIS19OCT148', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(614, 1, 6, 'Proses Bisnis Review dan Improvement', 'PROBIS19OCT149', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(615, 1, 6, 'Proses Bisnis Evaluasi dan usulan penyempurnaan dari semua bidang', 'PROBIS19OCT150', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(616, 1, 8, 'Proses Bisnis Kegiatan Monitoring dan Evaluasi Kinerja', 'PROBIS19OCT151', NULL, NULL, '2019-11-20', 2017, 1, 1, 0, '2019-10-05', 1, NULL, NULL, 0),
(617, 1, 6, 'Proses Bisnis Management of Change : Kajian Risiko', 'PROBIS19OCT152', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(618, 1, 6, 'Proses Bisnis RTM : Decision Making Process', 'PROBIS19OCT153', NULL, NULL, '2019-07-12', 2019, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(619, 1, 6, 'Proses Bisnis Management of Change: Implementasi', 'PROBIS19OCT154', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(620, 1, 6, 'Proses Bisnis Implementasi Improvement Action', 'PROBIS19OCT155', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(621, 1, 6, 'Proses Bisnis Evaluasi Efektivitas Improvement Action', 'PROBIS19OCT156', NULL, NULL, '2017-09-12', 2017, 1, 0, 0, '2019-10-05', 1, NULL, NULL, 0),
(622, 1, 1, 'Coba', 'PROBIS19OCT157', NULL, NULL, '2019-10-11', 2019, 1, 0, 0, '2019-10-11', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `probis_status`
--

CREATE TABLE `probis_status` (
  `id` int(11) NOT NULL,
  `probis` int(11) NOT NULL,
  `status` varchar(150) DEFAULT NULL COMMENT 'ONCOMING\nDONE\nPROGRESS\n',
  `keterangan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `probis_status`
--

INSERT INTO `probis_status` (`id`, `probis`, `status`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(457, 466, 'DONE', NULL, '2019-10-05', 1, NULL, NULL, 0),
(458, 467, 'DONE', NULL, '2019-10-05', 1, NULL, NULL, 0),
(459, 468, 'DONE', NULL, '2019-10-05', 1, NULL, NULL, 0),
(460, 469, 'DONE', NULL, '2019-10-05', 1, NULL, NULL, 0),
(461, 470, 'DONE', NULL, '2019-10-05', 1, NULL, NULL, 0),
(462, 471, 'DONE', NULL, '2019-10-05', 1, NULL, NULL, 0),
(463, 472, 'REVISION ON PROGRESS', 'proses revisi tgl 4 juli 2019', '2019-10-05', 1, NULL, NULL, 0),
(464, 473, 'REVISION ON PROGRESS', 'belum approval', '2019-10-05', 1, NULL, NULL, 0),
(465, 474, 'DONE', 'belum approval', '2019-10-05', 1, NULL, NULL, 0),
(466, 475, 'DONE', 'belum approval', '2019-10-05', 1, NULL, NULL, 0),
(467, 476, 'DONE', 'belum approval', '2019-10-05', 1, NULL, NULL, 0),
(468, 477, 'REVISION ON PROGRESS', 'ada revisi renda', '2019-10-05', 1, NULL, NULL, 0),
(469, 478, 'DONE', 'ada revisi renda', '2019-10-05', 1, NULL, NULL, 0),
(470, 479, 'DONE', 'ada revisi renda', '2019-10-05', 1, NULL, NULL, 0),
(471, 480, 'DONE', 'mau direvisi terkait batas kewenangan', '2019-10-05', 1, NULL, NULL, 0),
(472, 481, 'DONE', 'mau direvisi terkait batas kewenangan', '2019-10-05', 1, NULL, NULL, 0),
(473, 482, 'DONE', 'mau direvisi terkait batas kewenangan', '2019-10-05', 1, NULL, NULL, 0),
(474, 483, 'DONE', 'mau direvisi terkait batas kewenangan', '2019-10-05', 1, NULL, NULL, 0),
(475, 484, 'DONE', 'mau direvisi terkait batas kewenangan', '2019-10-05', 1, NULL, NULL, 0),
(476, 485, 'DONE', 'mau direvisi terkait batas kewenangan', '2019-10-05', 1, NULL, NULL, 0),
(477, 486, 'DONE', 'mau direvisi terkait batas kewenangan', '2019-10-05', 1, NULL, NULL, 0),
(478, 487, 'REVISION ON PROGRESS', 'belum approval', '2019-10-05', 1, NULL, NULL, 0),
(479, 488, 'DONE', 'belum approval', '2019-10-05', 1, NULL, NULL, 0),
(480, 489, 'DONE', 'belum approval', '2019-10-05', 1, NULL, NULL, 0),
(481, 490, 'DONE', 'belum approval', '2019-10-05', 1, NULL, NULL, 0),
(482, 491, 'DONE', 'belum approval', '2019-10-05', 1, NULL, NULL, 0),
(483, 492, 'DONE', 'belum approval', '2019-10-05', 1, NULL, NULL, 0),
(484, 493, 'DONE', 'belum approval', '2019-10-05', 1, NULL, NULL, 0),
(485, 494, 'DONE', 'belum approval', '2019-10-05', 1, NULL, NULL, 0),
(486, 495, 'DONE', 'belum approval', '2019-10-05', 1, NULL, NULL, 0),
(487, 496, 'DONE', 'belum approval', '2019-10-05', 1, NULL, NULL, 0),
(488, 497, 'DONE', 'belum approval', '2019-10-05', 1, NULL, NULL, 0),
(489, 498, 'PROGRESS', 'belum approval', '2019-10-05', 1, NULL, NULL, 0),
(490, 499, 'DONE', 'belum approval', '2019-10-05', 1, NULL, NULL, 0),
(491, 500, 'DONE', 'belum approval', '2019-10-05', 1, NULL, NULL, 0),
(492, 501, 'ONCOMING', 'belum approval', '2019-10-05', 1, NULL, NULL, 0),
(493, 502, 'DONE', 'belum approval', '2019-10-05', 1, NULL, NULL, 0),
(494, 503, 'ONCOMING', 'belum approval', '2019-10-05', 1, NULL, NULL, 0),
(495, 504, 'DONE', 'belum approval', '2019-10-05', 1, NULL, NULL, 0),
(496, 505, 'ONCOMING', 'belum approval', '2019-10-05', 1, NULL, NULL, 0),
(497, 506, 'ONCOMING', 'belum approval', '2019-10-05', 1, NULL, NULL, 0),
(498, 507, 'ONCOMING', 'belum approval', '2019-10-05', 1, NULL, NULL, 0),
(499, 508, 'DONE', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(500, 509, 'DONE', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(501, 510, 'DONE', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(502, 511, 'DONE', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(503, 512, 'DONE', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(504, 513, 'DONE', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(505, 514, 'DONE', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(506, 515, 'DONE', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(507, 516, 'DONE', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(508, 517, 'DONE', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(509, 518, 'DONE', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(510, 519, 'DONE', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(511, 520, 'DONE', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(512, 521, 'ONCOMING', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(513, 522, 'ONCOMING', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(514, 523, 'DONE', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(515, 524, 'DONE', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(516, 525, 'DONE', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(517, 526, 'DONE', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(518, 527, 'DONE', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(519, 528, 'DONE', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(520, 529, 'DONE', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(521, 530, 'DONE', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(522, 531, 'DONE', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(523, 532, 'DONE', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(524, 533, 'DONE', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(525, 534, 'DONE', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(526, 535, 'DONE', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(527, 536, 'DONE', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(528, 537, 'ONCOMING', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(529, 538, 'DONE', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(530, 539, 'DONE', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(531, 540, 'DONE', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(532, 541, 'ONCOMING', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(533, 542, 'DONE', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(534, 543, 'ONCOMING', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(535, 544, 'PROGRESS', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(536, 545, 'PROGRESS', 'renc ada revisi lg', '2019-10-05', 1, NULL, NULL, 0),
(537, 546, 'DONE', 'kurang approval', '2019-10-05', 1, NULL, NULL, 0),
(538, 547, 'DONE', 'kurang approval', '2019-10-05', 1, NULL, NULL, 0),
(539, 548, 'ONCOMING', 'kurang approval', '2019-10-05', 1, NULL, NULL, 0),
(540, 549, 'DONE', 'kurang approval', '2019-10-05', 1, NULL, NULL, 0),
(541, 550, 'DONE', 'kurang approval', '2019-10-05', 1, NULL, NULL, 0),
(542, 551, 'DONE', 'kurang approval', '2019-10-05', 1, NULL, NULL, 0),
(543, 552, 'DONE', 'kurang approval', '2019-10-05', 1, NULL, NULL, 0),
(544, 553, 'PROGRESS', 'kurang approval', '2019-10-05', 1, NULL, NULL, 0),
(545, 554, 'PROGRESS', 'proses approval', '2019-10-05', 1, NULL, NULL, 0),
(546, 555, 'DONE', 'proses approval', '2019-10-05', 1, NULL, NULL, 0),
(547, 556, 'PROGRESS', 'proses approval', '2019-10-05', 1, NULL, NULL, 0),
(548, 557, 'PROGRESS', 'proses approval', '2019-10-05', 1, NULL, NULL, 0),
(549, 558, 'DONE', 'proses approval', '2019-10-05', 1, NULL, NULL, 0),
(550, 559, 'PROGRESS', 'proses approval', '2019-10-05', 1, NULL, NULL, 0),
(551, 560, 'DONE', 'proses approval', '2019-10-05', 1, NULL, NULL, 0),
(552, 561, 'DONE', 'proses approval', '2019-10-05', 1, NULL, NULL, 0),
(553, 562, 'DONE', 'proses approval', '2019-10-05', 1, NULL, NULL, 0),
(554, 563, 'DONE', 'proses approval', '2019-10-05', 1, NULL, NULL, 0),
(555, 564, 'DONE', 'proses approval', '2019-10-05', 1, NULL, NULL, 0),
(556, 565, 'DONE', 'proses approval', '2019-10-05', 1, NULL, NULL, 0),
(557, 566, 'DONE', 'proses approval', '2019-10-05', 1, NULL, NULL, 0),
(558, 567, 'DONE', 'proses approval', '2019-10-05', 1, NULL, NULL, 0),
(559, 568, 'ONCOMING', 'proses approval', '2019-10-05', 1, NULL, NULL, 0),
(560, 569, 'ONCOMING', 'proses approval', '2019-10-05', 1, NULL, NULL, 0),
(561, 570, 'DONE', 'proses approval', '2019-10-05', 1, NULL, NULL, 0),
(562, 571, 'DONE', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(563, 572, 'DONE', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(564, 573, 'DONE', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(565, 574, 'DONE', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(566, 575, 'DONE', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(567, 576, 'DONE', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(568, 577, 'DONE', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(569, 578, 'DONE', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(570, 579, 'DONE', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(571, 580, 'DONE', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(572, 581, 'DONE', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(573, 582, 'DONE', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(574, 583, 'DONE', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(575, 584, 'DONE', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(576, 585, 'PROGRESS', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(577, 586, 'PROGRESS', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(578, 587, 'PROGRESS', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(579, 588, 'PROGRESS', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(580, 589, 'PROGRESS', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(581, 590, 'DONE', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(582, 591, 'PROGRESS', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(583, 592, 'PROGRESS', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(584, 593, 'PROGRESS', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(585, 594, 'PROGRESS', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(586, 595, 'PROGRESS', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(587, 596, 'ONCOMING', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(588, 597, 'ONCOMING', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(589, 598, 'ONCOMING', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(590, 599, 'ONCOMING', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(591, 600, 'DONE', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(592, 601, 'DONE', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(593, 602, 'DONE', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(594, 603, 'DONE', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(595, 604, 'DONE', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(596, 605, 'DONE', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(597, 606, 'ONCOMING', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(598, 607, 'ONCOMING', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(599, 608, 'ONCOMING', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(600, 609, 'ONCOMING', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(601, 610, 'DONE', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(602, 611, 'DONE', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(603, 612, 'DONE', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(604, 613, 'ONCOMING', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(605, 614, 'ONCOMING', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(606, 615, 'ONCOMING', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(607, 616, 'DONE', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(608, 617, 'ONCOMING', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(609, 618, 'DONE', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(610, 619, 'ONCOMING', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(611, 620, 'ONCOMING', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0),
(612, 621, 'ONCOMING', 'approval kurang', '2019-10-05', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `referensi_probis`
--

CREATE TABLE `referensi_probis` (
  `id` int(11) NOT NULL,
  `nama_referensi` varchar(150) DEFAULT NULL,
  `probis` int(11) NOT NULL,
  `upt` int(11) NOT NULL,
  `file` varchar(150) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `referensi_probis`
--

INSERT INTO `referensi_probis` (`id`, `nama_referensi`, `probis`, `upt`, `file`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(2, 'Referensi A', 2, 3, 'FITUR APLIKASI PROBIS.pdf', '2019-04-22', 2, NULL, NULL, 0),
(3, 'Referensi 1', 472, 6, 'Invoice-10987.pdf', '2019-11-04', 1, NULL, NULL, 0),
(4, 'Referens 1', 466, 6, 'Kontrak Pembuatan Aplikasi .pdf', '2019-11-04', 1, NULL, NULL, 0),
(5, 'Dodik Rismawan', 466, 6, 'Company Profile Dapps.pdf', '2019-11-08', 1, NULL, NULL, 0),
(6, 'tes', 466, 6, 'CATATAN WIPOL 13 DES 16.pdf.pdf', '2019-11-11', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `rule_probis`
--

CREATE TABLE `rule_probis` (
  `id` int(11) NOT NULL,
  `file` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `view` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rule_probis`
--

INSERT INTO `rule_probis` (`id`, `file`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`, `view`) VALUES
(1, 'FITUR APLIKASI PROBIS.pdf', NULL, NULL, '2019-05-24 03:35:20', 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `upt`
--

CREATE TABLE `upt` (
  `id` int(11) NOT NULL,
  `nama_upt` varchar(150) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `upt`
--

INSERT INTO `upt` (`id`, `nama_upt`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'UPT Malang', NULL, NULL, NULL, NULL, 0),
(2, 'UPT Surabaya', NULL, NULL, NULL, NULL, 0),
(3, 'UPT Induk', NULL, NULL, NULL, NULL, 0),
(4, 'UPT Gresik', NULL, NULL, NULL, NULL, 0),
(5, 'UPT Percobaan', '2019-04-22', 1, NULL, NULL, 0),
(6, 'Kantor Induk', '2019-07-23', 1, NULL, NULL, 0),
(8, '', '2019-07-23', 2, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `priveledge` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `priveledge`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'superadmin', 'superadmin', 1, NULL, NULL, NULL, NULL, 0),
(2, '1318033', '1234', 2, '2019-04-22', 1, '2019-07-15 08:57:12', 2, 0),
(3, '1318031', '1234', 3, '2019-04-22', 1, NULL, NULL, 0),
(4, '1318032', '1234', 2, '2019-07-24', 1, NULL, NULL, 0),
(5, '1318034', '1234', 2, '2019-07-24', 1, NULL, NULL, 0),
(6, 'general', '1234', 4, '2019-10-10', 1, NULL, NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `approval_probis`
--
ALTER TABLE `approval_probis`
  ADD PRIMARY KEY (`id`,`pegawai`,`pengajuan_probis`),
  ADD KEY `fk_approval_probis_pegawai1_idx` (`pegawai`),
  ADD KEY `fk_approval_probis_pengajuan_probis1_idx` (`pengajuan_probis`);

--
-- Indexes for table `dokumen_input`
--
ALTER TABLE `dokumen_input`
  ADD PRIMARY KEY (`id`,`probis`,`upt`),
  ADD KEY `fk_dokumen_input_probis1_idx` (`probis`),
  ADD KEY `fk_dokumen_input_upt1_idx` (`upt`);

--
-- Indexes for table `dokumen_output`
--
ALTER TABLE `dokumen_output`
  ADD PRIMARY KEY (`id`,`probis`,`upt`),
  ADD KEY `fk_dokumen_output_probis1_idx` (`probis`),
  ADD KEY `fk_dokumen_output_upt1_idx` (`upt`);

--
-- Indexes for table `feedback_probis`
--
ALTER TABLE `feedback_probis`
  ADD PRIMARY KEY (`id`,`user`,`probis`),
  ADD KEY `fk_feedback_probis_probis1_idx` (`probis`),
  ADD KEY `fk_feedback_probis_user1_idx` (`user`);

--
-- Indexes for table `forgot_password`
--
ALTER TABLE `forgot_password`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `info_probis`
--
ALTER TABLE `info_probis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `keyword_probis`
--
ALTER TABLE `keyword_probis`
  ADD PRIMARY KEY (`id`,`probis`),
  ADD KEY `fk_keyword_probis_probis1_idx` (`probis`);

--
-- Indexes for table `log_probis`
--
ALTER TABLE `log_probis`
  ADD PRIMARY KEY (`id`,`probis`),
  ADD KEY `fk_log_probis_probis1_idx` (`probis`);

--
-- Indexes for table `path_probis`
--
ALTER TABLE `path_probis`
  ADD PRIMARY KEY (`id`,`probis`),
  ADD KEY `fk_path_probis_probis1_idx` (`probis`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id`,`user`,`upt`),
  ADD KEY `fk_pegawai_upt1_idx` (`upt`),
  ADD KEY `fk_pegawai_user1_idx` (`user`);

--
-- Indexes for table `pengajuan_probis`
--
ALTER TABLE `pengajuan_probis`
  ADD PRIMARY KEY (`id`,`user`,`upt`),
  ADD KEY `fk_pengajuan_probis_user1_idx` (`user`),
  ADD KEY `fk_pengajuan_probis_upt1_idx` (`upt`);

--
-- Indexes for table `pengajuan_probis_has_status`
--
ALTER TABLE `pengajuan_probis_has_status`
  ADD PRIMARY KEY (`id`,`pengajuan_probis`,`user`),
  ADD KEY `fk_pengajuan_probis_has_status_pengajuan_probis1_idx` (`pengajuan_probis`),
  ADD KEY `fk_pengajuan_probis_has_status_user1_idx` (`user`);

--
-- Indexes for table `priveledge`
--
ALTER TABLE `priveledge`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `probis`
--
ALTER TABLE `probis`
  ADD PRIMARY KEY (`id`,`user`,`upt`),
  ADD KEY `fk_probis_user1_idx` (`user`),
  ADD KEY `fk_probis_upt1_idx` (`upt`);

--
-- Indexes for table `probis_status`
--
ALTER TABLE `probis_status`
  ADD PRIMARY KEY (`id`,`probis`),
  ADD KEY `fk_probis_status_probis_idx` (`probis`);

--
-- Indexes for table `referensi_probis`
--
ALTER TABLE `referensi_probis`
  ADD PRIMARY KEY (`id`,`probis`,`upt`),
  ADD KEY `fk_referensi_probis_probis1_idx` (`probis`),
  ADD KEY `fk_referensi_probis_upt1_idx` (`upt`);

--
-- Indexes for table `rule_probis`
--
ALTER TABLE `rule_probis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `upt`
--
ALTER TABLE `upt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`,`priveledge`),
  ADD KEY `fk_user_priveledge_idx` (`priveledge`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `approval_probis`
--
ALTER TABLE `approval_probis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `dokumen_input`
--
ALTER TABLE `dokumen_input`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `dokumen_output`
--
ALTER TABLE `dokumen_output`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `feedback_probis`
--
ALTER TABLE `feedback_probis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `forgot_password`
--
ALTER TABLE `forgot_password`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `info_probis`
--
ALTER TABLE `info_probis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `keyword_probis`
--
ALTER TABLE `keyword_probis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1727;

--
-- AUTO_INCREMENT for table `log_probis`
--
ALTER TABLE `log_probis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `path_probis`
--
ALTER TABLE `path_probis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=622;

--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pengajuan_probis`
--
ALTER TABLE `pengajuan_probis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pengajuan_probis_has_status`
--
ALTER TABLE `pengajuan_probis_has_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `priveledge`
--
ALTER TABLE `priveledge`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `probis`
--
ALTER TABLE `probis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=623;

--
-- AUTO_INCREMENT for table `probis_status`
--
ALTER TABLE `probis_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=613;

--
-- AUTO_INCREMENT for table `referensi_probis`
--
ALTER TABLE `referensi_probis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `rule_probis`
--
ALTER TABLE `rule_probis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `upt`
--
ALTER TABLE `upt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

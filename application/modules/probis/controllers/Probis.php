<?php

class Probis extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $upt;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
  $this->upt = $this->session->userdata('upt_id');
 }

 public function getModuleName() {
  return 'probis';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/probis_v3.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'probis';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Probis";
  $data['title_content'] = 'Data Probis';
  $content = $this->getDataProbis();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataProbis($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.no_probis', $keyword),
       array('t.nama_probis', $keyword),
       array('u.username', $keyword),
       array('ps.status', $keyword),
   );
  }

  $where = 't.deleted = 0 and pp.child_first = 0';
  if ($this->upt != '') {
   $where = "t.deleted = 0 and t.upt = '" . $this->upt . "' and pp.child_first = 0";
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' t',
              'field' => array(
                  't.*', 'u.username', 'pp.parent', 'pp.child_first',
                  'pp.child_second', 'pp.child_third'
              ),
              'join' => array(
                  array('user u', 't.user = u.id'),
                  array('(select max(id) as id, probis from probis_status group by probis) pss', 'pss.probis = t.id', 'left'),
                  array('probis_status ps', 'ps.id = pss.id', 'left'),
                  array('path_probis pp', 'pp.probis = t.id', 'left')
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => $where,
              'orderby' => 'pp.parent, pp.child_first, pp.child_second'
  ));

  return $total;
 }

 public function getDataProbis($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.no_probis', $keyword),
       array('t.nama_probis', $keyword),
       array('u.username', $keyword),
       array('ps.status', $keyword),
   );
  }


  $where = 't.deleted = 0 and pp.child_first = 0';
  if ($this->upt != '') {
   $where = "t.deleted = 0 and t.upt = '" . $this->upt . "' and pp.child_first = 0";
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array(
                  't.*', 'u.username', 'ps.status', 'ps.keterangan',
                  'pp.parent', 'pp.child_first',
                  'pp.child_second', 'pp.child_third'
              ),
              'join' => array(
                  array('user u', 't.user = u.id'),
                  array('(select max(id) as id, probis from probis_status group by probis) pss', 'pss.probis = t.id', 'left'),
                  array('probis_status ps', 'ps.id = pss.id', 'left'),
                  array('path_probis pp', 'pp.probis = t.id', 'left')
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => $where,
              'orderby' => 'pp.parent, pp.child_first, pp.child_second'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['status'] = $value['status'] == '' ? '-' : $value['status'];
    $value['tgl_update'] = $value['tgl_update'] == '' ? '' : date('d F Y', strtotime($value['tgl_update']));
    $edisi = '';
    if (trim($value['edisi']) < 9) {
     $edisi .= '0' . $value['edisi'];
    }
    $value['edisi'] = $edisi;
    $revisi = '';
    if (trim($value['revisi']) < 9) {
     $revisi .= '0' . $value['revisi'];
    }
    $value['revisi'] = $revisi;
    if ($value['parent'] != '') {
     $value['no_probis'] = $value['parent'] . '.' . $value['child_first'] .
             '.' . $value['child_second'] . '.' . $value['child_third'];
    }
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataProbis($keyword)
  );
 }

 public function getDetailDataProbis($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array(
                  't.*',
                  'u.username', 'u.password',
                  'p.hak_akses', 'ut.nama_upt',
                  'u.priveledge', 'pp.no_probis as no_usulan', 'pp.id as id_usulan'
              ),
              'join' => array(
                  array('user u', 't.user = u.id'),
                  array('priveledge p', 'u.priveledge = p.id'),
                  array('upt ut', 't.upt = ut.id', 'left'),
                  array('pengajuan_probis pp', 't.pengajuan_probis = pp.id', 'left'),
              ),
              'where' => "t.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function getListUpt() {
  $data = Modules::run('database/get', array(
              'table' => 'upt',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getListHakAkses() {
  $data = Modules::run('database/get', array(
              'table' => 'priveledge',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    if ($value['id'] != 1) {
     array_push($result, $value);
    }
   }
  }

  return $result;
 }

 public function getListHubunganWali() {
  $data = Modules::run('database/get', array(
              'table' => 'hubungan_wali',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getListSemester() {
  $data = Modules::run('database/get', array(
              'table' => 'semester',
              'where' => 'deleted = 0'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Probis";
  $data['title_content'] = 'Tambah Probis';
  $data['list_upt'] = $this->getListUpt();
  $data['list_hak_akses'] = $this->getListHakAkses();
  echo Modules::run('template', $data);
 }

 public function getListKeywordProbis($probis) {
  $data = Modules::run('database/get', array(
              'table' => 'keyword_probis kb',
              'field' => array('kb.*'),
              'where' => "kb.deleted = 0 and kb.probis = '" . $probis . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function ubah($id) {
  $data = $this->getDetailDataProbis($id);
  //echo '<pre>';
  //print_r($data);die;
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Probis";
  $data['title_content'] = 'Ubah Probis';
  $data['list_keyword'] = $this->getListKeywordProbis($id);
  $data['list_upt'] = $this->getListUpt();
  echo Modules::run('template', $data);
 }

 public function getDetailDataOrganisasi($id) {
  $data = Modules::run('database/get', array(
              'table' => 'masjid_has_organisasi mho',
              'field' => array('mho.*', 'o.nama_organisasi'),
              'join' => array(
                  array('organisasi o', 'mho.organisasi = o.id')
              ),
              'where' => "mho.deleted = 0 and mho.masjid = '" . $id . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getDetailPengurusProbis($masjid) {
  $data = Modules::run('database/get', array(
              'table' => 'masjid_has_pengurus mhp',
              'field' => array('mhp.*', 'j.jabatan as nama_jabatan'),
              'join' => array(
                  array('jabatan j', 'mhp.jabatan = j.id')
              ),
              'where' => array('mhp.deleted' => 0, 'mhp.masjid' => $masjid)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataWaliMurid($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_wali_murid thw',
              'field' => array('thw.*', 'hw.hubungan'),
              'join' => array(
                  array('hubungan_wali hw', 'hw.id = thw.hubungan_wali')
              ),
              'where' => "thw.deleted = 0 and thw.taruna = '" . $taruna . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataAgama($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_agama ta',
              'field' => array('ta.*'),
              'where' => "ta.deleted = 0 and ta.taruna = '" . $taruna . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['status'] = $value['status'] == 1 ? 'Aktif' : 'Tidak Aktif';
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataKesehatan($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_kesehatan ta',
              'field' => array('ta.*'),
              'where' => "ta.deleted = 0 and ta.taruna = '" . $taruna . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['status'] = $value['status'] == 1 ? 'Aktif' : 'Tidak Aktif';
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataAkademik($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_akademik ta',
              'field' => array(
                  'ta.*', 's.semester as semester_taruna',
                  'p.prodi as program_studi'
              ),
              'join' => array(
                  array('prodi p', 'ta.prodi = p.id'),
                  array('semester s', 'ta.semester = s.id'),
              ),
              'where' => "ta.deleted = 0 and ta.taruna = '" . $taruna . "'"
  ));


  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['status'] = $value['status'] == 1 ? 'Aktif' : 'Tidak Aktif';
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function detail($id, $referensi = "") {
  $data = $this->getDetailDataProbis($id);
  $data['file_excel'] = $data['file_excel'];
  $data['file_excel'] = str_replace(' ', '_', $data['file_excel']);
  $data['file_excel'] = str_replace('.', '_', $data['file_excel']);
  $data['file_excel'] = str_replace('_xlsx', '.xlsx', $data['file_excel']);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Probis";
  $data['title_content'] = "Detail Probis";
  $data['referensi'] = $referensi;
  $data['list_keyword'] = $this->getListKeywordProbis($id);
  $this->updateLogProbis($id);
  echo Modules::run('template', $data);
 }

 public function updateLogProbis($id) {
  $post['probis'] = $id;
  $post['tanggal_akses'] = date('Y-m-d H:i:s');
  Modules::run('database/_insert', 'log_probis', $post);
 }

 public function getDetailFeedback($id) {
  $data = Modules::run('database/get', array(
              'table' => 'feedback_probis fb',
              'field' => array('fb.*', 'p.nama_pegawai'),
              'join' => array(
                  array('user u', 'fb.user = u.id'),
                  array('pegawai p', 'u.id = p.user'),
              ),
              'where' => "fb.deleted = 0 and fb.probis = '" . $id . "'",
              'orderby' => 'fb.id desc'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function feedback($id) {
  $data = $this->getDetailDataProbis($id);
  $data['view_file'] = 'feedback_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Feedback Probis";
  $data['title_content'] = "Feedback Probis";
  $data['feedback'] = $this->getDetailFeedback($id);
  echo Modules::run('template', $data);
 }

 public function getPostDataProbis($value) {
  $data['no_probis'] = Modules::run('no_generator/generateNoProbis');
  $data['user'] = $this->session->userdata('user_id');

  if (isset($value->upt)) {
   $data['upt'] = $value->upt;
  } else {
   $data['upt'] = $this->upt;
  }
  //  echo '<pre>';
  //  print_r($data);die;
  $data['nama_probis'] = $value->nama;
  $data['tgl_update'] = $value->tgl_update;
  $data['tahun'] = $value->tahun;
  $data['edisi'] = $value->edisi;
  $data['revisi'] = $value->revisi;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  //  echo '<pre>';
  //  print_r($data);die;
  //  echo '<pre>';
  //  print_r($_FILES);
  //  die;
  $id = $this->input->post('id');
  $is_valid = false;
  $message = "";
  $this->db->trans_begin();
  try {
   $post = $this->getPostDataProbis($data);


   $is_uploaded = true;
   if ($id == '') {
    if (!empty($_FILES)) {
     //xls
     if (isset($_FILES['file_xls'])) {
      $data_file = $this->uploadData("file_xls");
      $is_uploaded = $data_file['is_valid'];
      if ($is_uploaded) {
       $post['file_excel'] = $_FILES['file_xls']['name'];
      } else {
       $is_valid = false;
       $message = $data_file['response'];
      }
     }

     //pdf
     if (isset($_FILES['file_pdf'])) {
      $data_file = $this->uploadData("file_pdf");
      $is_uploaded = $data_file['is_valid'];
      if ($is_uploaded) {
       $post['file_pdf'] = $_FILES['file_pdf']['name'];
      } else {
       $is_valid = false;
       $message = $data_file['response'];
      }
     }
    }

    if ($is_uploaded) {
     $id = Modules::run('database/_insert', $this->getTableName(), $post);

     if (!empty($data->keyword)) {
      foreach ($data->keyword as $value) {
       $post_keyword['keyword'] = $value->keyword;
       $post_keyword['probis'] = $id;
       Modules::run('database/_insert', 'keyword_probis', $post_keyword);
      }
     }
    }
   } else {
    //update
    if (!empty($_FILES)) {
     //xls
     if (isset($_FILES['file_xls'])) {
      $data_file = $this->uploadData("file_xls");
      $is_uploaded = $data_file['is_valid'];
      if ($is_uploaded) {
       $post['file_excel'] = $_FILES['file_xls']['name'];
      } else {
       $is_valid = false;
       $message = $data_file['response'];
      }
     }

     //pdf
     if (isset($_FILES['file_pdf'])) {
      $data_file = $this->uploadData("file_pdf");
      $is_uploaded = $data_file['is_valid'];
      if ($is_uploaded) {
       $post['file_pdf'] = $_FILES['file_pdf']['name'];
      } else {
       $is_valid = false;
       $message = $data_file['response'];
      }
     }
    }

    if ($is_uploaded) {
     unset($post['no_probis']);
     Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));

     if (!empty($data->keyword)) {
      foreach ($data->keyword as $value) {
       $post_keyword['keyword'] = $value->keyword;
       $post_keyword['probis'] = $id;
       Modules::run('database/_insert', 'keyword_probis', $post_keyword);
      }
     }

     if (!empty($data->keyword_edit)) {
      foreach ($data->keyword_edit as $value) {
       $post_keyword['keyword'] = $value->keyword;
       if ($value->is_edit == 0) {
        $post_keyword['deleted'] = 1;
       }
       Modules::run('database/_update', 'keyword_probis', $post_keyword, array('id' => $value->id));
      }
     }
    }
   }
   $this->db->trans_commit();
   if ($is_uploaded) {
    $is_valid = true;
   }
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id, 'message' => $message));
 }

 public function sendFeedback($id) {
  $is_valid = false;
  $this->db->trans_begin();
  //  echo '<pre>';
  //  print_r($_POST);die;
  try {
   $post['probis'] = $id;
   $post['pesan'] = $this->input->post('pesan');
   $post['user'] = $this->session->userdata('user_id');
   Modules::run('database/_insert', 'feedback_probis', $post);
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }
  echo json_encode(array('is_valid' => $is_valid));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Probis";
  $data['title_content'] = 'Data Probis';
  $content = $this->getDataProbis($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function deleteFeedback($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', 'feedback_probis', array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/probis/';
  $config['allowed_types'] = 'pdf|xls|xlsx';
  $config['max_size'] = '1000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);
  // $this->upload->do_upload($name_of_field);


  $is_valid = false;
  if (!$this->upload->do_upload($name_of_field)) {
   $response = $this->upload->display_errors();
  } else {
   $response = $this->upload->data();
   $is_valid = true;
  }

  return array(
      'is_valid' => $is_valid,
      'response' => $response
  );
 }

 public function showLogo($id = "") {
  //   $foto = str_replace(' ', '_', $this->input->post('file'));
  //   $foto = str_replace('.', '_', $foto);
  //   $foto = str_replace('_pdf', '.pdf', $foto);
  //   //echo $foto;die;
  $data = $this->getDetailDataProbis($id);
  // echo '<pre>';
  // print_r($data);die;
  $foto = str_replace(' ', '_', $data['file_pdf']);
  $data_foto = explode('.', $foto);
  $tipe_file = $data_foto[count($data_foto) - 1];

  if (!empty($data_foto)) {
   $foto = implode('_', $data_foto);
   $foto = str_replace("_" . $tipe_file, '.' . $tipe_file, $foto);
  }

  $foto = str_replace('__', '_', $foto);

  $data['file_fix'] = $foto;

  $data['view_file'] = 'file';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Probis";
  $data['title_content'] = "Detail Probis";
  echo $this->load->view('file', $data, true);
 }

 public function download($id) {
  $this->load->helper("download");
  $probis = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' js',
              'where' => "js.id = " . $id
          ))->row_array();

  $file = $probis['file_excel'];
  $data = file_get_contents('files/berkas/probis/' . $file);
  $name = $file;
  force_download($name, $data);
 }

 public function detailUsulan($id) {
  $data = Modules::run('review_usulan_probis/getDetailDataPengajuan', $id);
  $data['view_file'] = 'detail_usulan_probis';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Pengajuan";
  $data['title_content'] = "Detail Pengajuan";
  $data['list_status'] = Modules::run('review_usulan_probis/getListStatus', $id);
  $data['tembusan'] = Modules::run('review_usulan_probis/getDataTembusan', $id);
  echo Modules::run('template', $data);
 }

 public function getStatus() {
  $id = $this->input->post('id');
  $data['id'] = $id;
  echo $this->load->view('status_probis', $data, true);
 }

 public function updateStatus() {
  $id = $this->input->post('id');
  $status = $this->input->post('status');
  $keterangan = $this->input->post('keterangan');
  $is_valid = false;
  $this->db->trans_begin();
  try {
   $insert['probis'] = $id;
   $insert['status'] = $status;
   if ($keterangan != '') {
    $insert['keterangan'] = $keterangan;
   }

   Modules::run('database/_insert', 'probis_status', $insert);
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function setStatus() {
  $id = $this->input->post('id');
  $jenis = $this->input->post('jenis');
  $checked = $this->input->post('checked');
  $keterangan = $this->input->post('keterangan');

  // echo '<pre>';
  // print_r($_POST);die;
  $is_valid = false;
  $this->db->trans_begin();
  try {

   $post = array();
   switch ($jenis) {
    case 'regulasi':
     $post['regulasi'] = $checked;
     if ($keterangan != '') {
      $post['ket_regulasi'] = $keterangan;
     }
     break;
    case 'resiko':
     $post['resiko'] = $checked;
     if ($keterangan != '') {
      $post['ket_resiko'] = $keterangan;
     }
     break;
    case 'klausul':
     $post['klausul'] = $checked;
     if ($keterangan != '') {
      $post['ket_klausul'] = $keterangan;
     }
     break;

    default:
     # code...
     break;
   }

   Modules::run('database/_update', 'probis', $post, array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function getDataProbisChild() {

  $parent = $_POST['parent'];
  $level = $_POST['level'];

  if ($level == '1') {
   $where = "t.deleted = 0 and pp.parent = '" . $parent . "' and pp.child_first != 0 and pp.child_second = 0";
  }
  if ($level == '2') {
   $child_first = $_POST['child_first'];
   $where = "t.deleted = 0 and pp.parent = '" . $parent . "' and pp.child_first = '" . $child_first . "' and pp.child_second != 0";
  }

  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array(
                  't.*', 'u.username', 'ps.status', 'ps.keterangan',
                  'pp.parent', 'pp.child_first',
                  'pp.child_second', 'pp.child_third'
              ),
              'join' => array(
                  array('user u', 't.user = u.id'),
                  array('(select max(id) as id, probis from probis_status group by probis) pss', 'pss.probis = t.id', 'left'),
                  array('probis_status ps', 'ps.id = pss.id', 'left'),
                  array('path_probis pp', 'pp.probis = t.id', 'left')
              ),
              'where' => $where,
              'orderby' => 'pp.parent, pp.child_first, pp.child_second, pp.child_third'
  ));

  // echo '<pre>';
  // echo $this->db->last_query();die;
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['status'] = $value['status'] == '' ? '-' : $value['status'];
    $value['tgl_update'] = $value['tgl_update'] == '' ? '' : date('d F Y', strtotime($value['tgl_update']));
    $edisi = '';
    if (trim($value['edisi']) < 9) {
     $edisi .= '0' . $value['edisi'];
    }
    $value['edisi'] = $edisi;
    $revisi = '';
    if (trim($value['revisi']) < 9) {
     $revisi .= '0' . $value['revisi'];
    }
    $value['revisi'] = $revisi;
    if ($value['parent'] != '') {
     $value['no_probis'] = $value['parent'] . '.' . $value['child_first'] .
             '.' . $value['child_second'] . '.' . $value['child_third'];
    }
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getDetailChildProbis() {
  $data = $this->getDataProbisChild();
  $content['content'] = $data;
  //  echo '<pre>';
  //  print_r($data);die;
  $content['module'] = $this->getModuleName();
  if ($_POST['level'] == '1') {
   echo $this->load->view('table_child_fist', $content, true);
  }

  if ($_POST['level'] == '2') {
   echo $this->load->view('table_child_second', $content, true);
  }
 }

}

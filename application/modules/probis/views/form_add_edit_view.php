<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <!--.row-->
 <div class="row">
  <div class="col-md-12">
   <div class="panel panel-primary">
    <div class="panel-heading"> <?php echo $title_content ?></div>
    <div class="panel-wrapper collapse in" aria-expanded="true">
     <div class="panel-body">
      <form action="#" class="form-horizontal">
       <div class="form-body">
        <h3 class="box-title">Probis <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">
        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Nama Probis</label>
           <div class="col-md-9">
            <input type="text" id='nama' class="form-control required" placeholder="Nama Probis" error='Nama Probis' value="<?php echo isset($nama_probis) ? $nama_probis : '' ?>">
           </div>
          </div>
         </div>         
         <!--/span-->
        </div>       

        <?php if ($this->session->userdata('hak_akses') == 'Superadmin') { ?>
         <div class="row">
          <div class="col-md-6">
           <div class="form-group">
            <label class="control-label col-md-3">UPT</label>
            <div class="col-md-9">
             <select id='upt' class="form-control required" error='UPT'>
              <?php if (!empty($list_upt)) { ?>
               <?php foreach ($list_upt as $value) { ?>
                <?php $selected = '' ?>
                <?php if (isset($upt)) { ?>
                 <?php $selected = $upt == $value['id'] ? 'selected' : '' ?>
                <?php } ?>
                <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama_upt'] ?></option>
               <?php } ?>
              <?php } ?>
             </select>
            </div>
           </div>
          </div> 
         </div>
        <?php } ?>

        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Tahun</label>
           <div class="col-md-9">
            <input type="text" id='tahun' class="form-control required" placeholder="Tahun" error='Tahun' value="<?php echo isset($tahun) ? $tahun : '' ?>">
           </div>
          </div>
         </div>         
         <!--/span-->
        </div>       

        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Tanggal</label>
           <div class="col-md-9">
            <input type="text" id='tgl_update' class="form-control required" placeholder="Tanggal" error='Tanggal' value="<?php echo isset($tgl_update) ? $tgl_update : '' ?>">
           </div>
          </div>
         </div>         
         <!--/span-->
        </div>       

        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Edisi</label>
           <div class="col-md-9">
            <input type="text" id='edisi' class="form-control required" placeholder="Edisi" error='Edisi' value="<?php echo isset($edisi) ? $edisi : '' ?>">
           </div>
          </div>
         </div>         
         <!--/span-->
        </div> 
       
        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Revisi</label>
           <div class="col-md-9">
            <input type="text" id='revisi' class="form-control required" placeholder="Revisi" error='Revisi' value="<?php echo isset($revisi) ? $revisi : '' ?>">
           </div>
          </div>
         </div>         
         <!--/span-->
        </div> 
        
        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">File Probis (.xls)</label>
           <div class="col-sm-9">
            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
             <div class="form-control" data-trigger="fileinput"> 
              <i class="glyphicon glyphicon-file fileinput-exists"></i> 
              <span class="fileinput-filename"><?php echo isset($file_excel) ? $file_excel : '' ?></span>
             </div> 
             <span class="input-group-addon btn btn-default btn-file"> 
              <span class="fileinput-new">Select file</span> 
              <span class="fileinput-exists">Change</span>
              <input id='probis_xls' type="file" name="..." onchange="Probis.getFilename(this, 'xls')"> 
             </span> 
             <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> 
            </div>
           </div>
          </div>
         </div> 
        </div>       

        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">File Probis (.pdf)</label>
           <div class="col-sm-9">
            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
             <div class="form-control" data-trigger="fileinput"> 
              <i class="glyphicon glyphicon-file fileinput-exists"></i> 
              <span class="fileinput-filename"><?php echo isset($file_pdf) ? $file_pdf : '' ?></span>
             </div> 
             <span class="input-group-addon btn btn-default btn-file"> 
              <span class="fileinput-new">Select file</span> 
              <span class="fileinput-exists">Change</span>
              <input id='probis_pdf' type="file" name="..." onchange="Probis.getFilename(this, 'pdf')"> 
             </span> 
             <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> 
            </div>
           </div>
          </div>
         </div> 
        </div>       
       </div>

       <h3 class="box-title">Keyword Pencarian Probis <i class="fa fa-arrow-down"></i></h3>
       <hr class="m-t-0 m-b-40">

       <div class="row">
        <div class="col-md-12">
         <div class="table-responsive">
          <table class="table color-bordered-table primary-bordered-table" id="table_keyword">
           <thead>
            <tr class="">
             <th class="font-12">Keyword</th>
             <th class="text-center font-12">Action</th>
            </tr>
           </thead>
           <tbody>
            <?php if (isset($list_keyword)) { ?>
             <?php foreach ($list_keyword as $value) { ?>
              <tr id="<?php echo $value['id'] ?>">
               <td class="font-12">
                <input type="text" value="<?php echo $value['keyword'] ?>" id="keyword" class="form-control" />
               </td>
               <td class="font-12 text-center">
                <i class="mdi mdi-delete mdi-24px hover text-danger" onclick="Probis.removeData(this)"></i>
               </td>
              </tr>
             <?php } ?>
             <tr id="baru">
              <td class="font-12">
               <input type="text" value="" placeholder="Keyword" id="keyword" class="form-control" />
              </td>
              <td class="font-12 text-center">
               <i class="mdi mdi-plus-circle mdi-24px hover text-warning" onclick="Probis.addData(this)"></i>
              </td>
             </tr>
            <?php } else { ?>
             <tr id="baru">
              <td class="font-12">
               <input type="text" value="" placeholder="Keyword" id="keyword" class="form-control" />
              </td>
              <td class="font-12 text-center">
               <i class="mdi mdi-plus-circle mdi-24px hover text-warning" onclick="Probis.addData(this)"></i>
              </td>
             </tr>
            <?php } ?>
           </tbody>
          </table>
         </div>
        </div>
       </div>


       <div class="form-actions">
        <div class="row">
         <div class="col-md-12">
          <div class="row">
           <div class="col-md-offset-3 col-md-9 text-right">
            <button type="submit" class="btn btn-success" onclick="Probis.simpan('<?php echo isset($id) ? $id : '' ?>', event)">Submit</button>
            <button type="button" class="btn btn-default" onclick="Probis.back()">Batal</button>
           </div>
          </div>
         </div>
         <div class="col-md-6"> </div>
        </div>
       </div>
      </form>
     </div>
    </div>
   </div>
  </div>
 </div>
 <!--./row--> 
</div>

<div class='container-fluid'>

 <div class="white-box stat-widget">  
  <div class="card-body">
   <h4 class="card-title"><u><?php echo $title ?></u></h4>

   <div class='row'>
    <div class='col-md-2'>
     <button id="" class="btn btn-block btn-warning" onclick="Probis.searchBtn()">Cari</button>     
    </div>
    <div class='col-md-10'>
     <input type='text' name='' id='keyword' onkeyup="Probis.search(this, event)" class='form-control' value='' placeholder="Pencarian"/>    
    </div>     
   </div>        
   <br/>
   <div class='row'>
    <div class='col-md-12'>
     <?php if (isset($keyword)) { ?>
      <?php if ($keyword != '') { ?>
       Cari Data : "<b><?php echo $keyword; ?></b>"
      <?php } ?>
     <?php } ?>
    </div>
   </div>
   <br/>
   <hr/>

   <div class='row'>
    <div class='col-md-12'>
     <div class="table-responsive">
      <table class="table color-bordered-table primary-bordered-table">
       <thead>
        <tr class="">
         <th class="font-12">No</th>
         <th class="font-12">No. Probis</th>
         <th class="font-12">Nama Probis</th>
         <th class="font-12">Tahun</th>
         <th class="font-12">Tanggal Update</th>
         <th class="font-12">Pembuat</th>
         <th class="font-12">Edisi</th>
         <th class="font-12">Revisi</th>
         <th class="font-12">Status</th>
         <th class="font-12">Keterangan</th>        
         <th class="text-center font-12">Action</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($content)) { ?>
         <?php $no = $pagination['last_no'] + 1; ?>
         <?php foreach ($content as $value) { ?>
          <tr>
           <td class='font-12'><?php echo $no++ ?></td>
           <td class='font-14'><?php echo $value['no_probis'] ?></td>
           <td class='font-12'><?php echo $value['nama_probis'] ?></td>
           <td class='font-12'><?php echo $value['tahun'] ?></td>
           <td class='font-12'><?php echo $value['tgl_update'] ?></td>
           <td class='font-12'><?php echo $value['username'] ?></td>
           <td class='font-12'><?php echo $value['edisi'] ?></td>
           <td class='font-12'><?php echo $value['revisi'] ?></td>
           <?php $color = ""; ?>
           <?php if ($value['status'] == 'PROGRESS') { ?>
            <?php $color = 'label-warning'; ?>
           <?php } ?>
           <?php if ($value['status'] == 'ONCOMING') { ?>
            <?php $color = 'label-primary'; ?>
           <?php } ?>
           <?php if ($value['status'] == 'DONE') { ?>
            <?php $color = 'label-success'; ?>
           <?php } ?>
           <?php if ($value['status'] == 'PROSES REVISI') { ?>
            <?php $color = 'label-info'; ?>
           <?php } ?>
           <td class='font-12'><label class="label <?php echo $color; ?>"><?php echo $value['status'] ?></label></td>
           <td class='font-12'><?php echo $value['keterangan'] ?></td>
           <td class="text-center">
            <?php if ($this->session->userdata('hak_akses') == 'Admin' || $this->session->userdata('hak_akses') == 'Superadmin') { ?>
             <!--             <label id="" class="label label-warning font-10" 
                                 onclick="Probis.ubah('<?php echo $value['id'] ?>')">Ubah</label>-->
             <i class="fa fa-pencil text-primary hover" data-toggle="tooltip" title="Ubah" onclick="Probis.ubah('<?php echo $value['id'] ?>')"></i>
             &nbsp;
             <!--             <label id="" class="label label-success font-10" 
                                 onclick="Probis.detail('<?php echo $value['id'] ?>')">Detail</label>-->
             <i class="fa fa-file-text-o text-primary hover" data-toggle="tooltip" title="Detail" onclick="Probis.detail('<?php echo $value['id'] ?>')"></i>
             &nbsp;
             <i class="fa fa-trash-o text-primary hover" data-toggle="tooltip" title="Hapus" onclick="Probis.delete('<?php echo $value['id'] ?>')"></i>
             <!--             <label id="" class="label label-danger font-10 hover" 
                                 onclick="Probis.delete('<?php echo $value['id'] ?>')">Hapus</label>-->
             &nbsp;
             <label id="" class="label label-info font-10 hover" 
                    onclick="Probis.changeStatus('<?php echo $value['id'] ?>')">Ubah Status</label>
                   <?php } else { ?>
             <a id="" class="hover" 
                href="<?php echo base_url() . $module . '/download/' . $value['id'] ?>">
              <!--             Download (.xls)-->
              <i class="fa fa-download text-primary hover" data-toggle="tooltip" title="Download"></i>
             </a>
             &nbsp;
             <?php if ($value['file_pdf'] != '') { ?>
              <label id="" class="label label-danger font-10 hover" 
                     onclick="Probis.previewFile('<?php echo $value['id'] ?>')">Preview (.pdf)</label>
              &nbsp;
             <?php } ?>
             <!--             <label id="" class="label label-primary font-10 hover" 
                                               onclick="Probis.feedback('<?php echo $value['id'] ?>')">Feedback</label>-->
             <i data-toggle="tooltip" title="Feedback" class="fa fa-rotate-right text-primary hover" onclick="Probis.feedback('<?php echo $value['id'] ?>')"></i>
             &nbsp; 
             <i class="fa fa-file-text-o text-primary hover" data-toggle="tooltip" title="Detail" onclick="Probis.detail('<?php echo $value['id'] ?>')"></i>
             <!--             <label id="" class="label label-success font-10" 
                                 onclick="Probis.detail('<?php echo $value['id'] ?>')">Detail</label>              -->
             &nbsp;
            <?php } ?>
            <br>
            <hr>
            <?php if (!isset($keyword)) { ?>
             <i data-toggle="tooltip" title="Maximize/Minimize" id="toogle-action" class="fa fa-chevron-down text-primary hover" 
                parent="<?php echo $value['parent'] ?>" child_first="0" 
                onclick="Probis.getDetailChildProbis(this, '1')">
             </i>

             &nbsp;
             <i data-toggle="tooltip" title="Status Data Probis" id="toogle-action" class="fa fa-chevron-down text-success hover" 
                parent="<?php echo $value['parent'] ?>" child_first="0" 
                onclick="Probis.showStatusData(this, '<?php echo $value['id'] ?>')">
             </i>
            <?php } ?>            
           </td>
          </tr>

          <!--TAMBAHAN STATUS-->
          <tr class='hide status_data' data_id="<?php echo $value['id'] ?>">
           <td colspan="9" class='text-right'> 
            <?php $checked = $value['regulasi'] == "1" ? 'checked' : '' ?>
            <?php $disabled = 'disabled' ?>
            <?php if ($this->session->userdata('hak_akses') == 'Admin' || $this->session->userdata('hak_akses') == 'Superadmin') { ?>
             <?php $disabled = '' ?>
            <?php } ?>
            <input <?php echo $disabled ?> <?php echo $checked ?> type="checkbox" data_id="<?php echo $value['id'] ?>" onchange="Probis.showKeteranganRegulasi(this, 'regulasi')">
           </td>
           <td colspan="2">
            <label for="" class='font-10'>Status Probis Terhadap Regulasi yang Berlaku</label> 
           </td>
          </tr>

          <?php if ($value['ket_regulasi'] != '' && $value['regulasi'] == 1) { ?>
           <tr class='hide status_data' data_id="<?php echo $value['id'] ?>">

            <td colspan="11" class='text-right'> 
             <label for="" class='font-10'>Ket Regulasi</label> 
             <label for="" class='font-10 text-danger'><?php echo $value['ket_regulasi'] ?></label> 
            </td>
           </tr>
          <?php } ?>

          <tr class='hide status_data' data_id="<?php echo $value['id'] ?>">
           <td colspan="9" class='text-right'> 
            <?php $checked = $value['klausul'] == "1" ? 'checked' : '' ?>
            <?php $disabled = 'disabled' ?>
            <?php if ($this->session->userdata('hak_akses') == 'Admin' || $this->session->userdata('hak_akses') == 'Superadmin') { ?>
             <?php $disabled = '' ?>
            <?php } ?>
            <input <?php echo $disabled ?> <?php echo $checked ?> type="checkbox" data_id="<?php echo $value['id'] ?>" onchange="Probis.showKeteranganKlausul(this, 'klausul')">			
           </td>
           <td colspan="2">
            <label for="" class='font-10'>Status Probis Terhadap Klausul ISO</label> 
           </td>
          </tr>

          <?php if ($value['ket_klausul'] != '' && $value['klausul'] == 1) { ?>
           <tr class='hide status_data' data_id="<?php echo $value['id'] ?>">

            <td colspan="11" class='text-right'> 
             <label for="" class='font-10'>Ket Klausul</label> 
             <label for="" class='font-10 text-danger'><?php echo $value['ket_klausul'] ?></label> 
            </td>
           </tr>
          <?php } ?>

          <tr class='hide status_data' data_id="<?php echo $value['id'] ?>">
           <td colspan="9" class='text-right'> 
            <?php $checked = $value['resiko'] == "1" ? 'checked' : '' ?>
            <?php $disabled = 'disabled' ?>
            <?php if ($this->session->userdata('hak_akses') == 'Admin' || $this->session->userdata('hak_akses') == 'Superadmin') { ?>
             <?php $disabled = '' ?>
            <?php } ?>
            <input <?php echo $disabled ?> <?php echo $checked ?> type="checkbox" data_id="<?php echo $value['id'] ?>" onchange="Probis.showKeteranganResiko(this, 'resiko')">			
           </td>
           <td colspan="2">
            <label for="" class='font-10'>Status Probis Terhadap Risiko</label> 
           </td>
          </tr>

          <?php if ($value['ket_resiko'] != '' && $value['resiko'] == 1) { ?>
           <tr class='hide status_data' data_id="<?php echo $value['id'] ?>">

            <td colspan="11" class='text-right'> 
             <label for="" class='font-10'>Ket Resiko</label> 
             <label for="" class='font-10 text-danger'><?php echo $value['ket_resiko'] ?></label> 
            </td>
           </tr>
          <?php } ?>
          <!--TAMBAHAN STATUS-->
         <?php } ?>
        <?php } else { ?>
         <tr>
          <td class="text-center font-12" colspan="12">Tidak Ada Data Ditemukan</td>
         </tr>
        <?php } ?>         
       </tbody>
      </table>
     </div>
    </div>
   </div>    
   <div class="row">
    <div class="col-md-12 text-right">
     <?php if ($this->session->userdata('hak_akses') == 'Superadmin') { ?>
      <button id="" class="btn btn-warning" onclick="Probis.add()">Tambah</button>
     <?php } ?>     
    </div>
   </div>
   <br/>
   <div class="row">
    <div class="col-md-12">
     <div class="pagination">
      <?php echo $pagination['links'] ?>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>

<input type="hidden" value="<?php echo $id ?>" id="probis_status" class="form-control" />
<div class="row">
 <div class="col-md-12">
  <table class="table">
   <thead>
    <tr>
     <th class="text-center">STATUS PROBIS</th>
    </tr>
   </thead>
   <tbody>
    <tr class="hover" onclick="Probis.updateStatus(this)">
     <td class=""><label class="label label-warning font-15">PROGRESS</label></td>
    </tr>
    <tr class="hover" onclick="Probis.updateStatus(this)">
     <td class=""><label class="label label-primary font-15">ONCOMING</label></td>
    </tr>
    <tr class="hover" onclick="Probis.updateStatus(this)">
     <td class=""><label class="label label-info font-15">PROSES REVISI</label></td>
    </tr>
    <tr class="hover" onclick="Probis.updateStatus(this)">
     <td class=""><label class="label label-success font-15">DONE</label></td>
    </tr>
   </tbody>
  </table>
 </div>
</div>
<div class="row">
 <div class="col-md-12">
  <label>Keterangan</label>
  <textarea class="form-control" id="keterangan"></textarea>
 </div>
</div>
<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <!--.row-->
 <div class="row">
  <div class="col-md-12">
   <div class="panel panel-primary">
    <div class="panel-heading"> <?php echo $title_content ?></div>
    <div class="panel-wrapper collapse in" aria-expanded="true">
     <div class="panel-body">
      <form action="#" class="form-horizontal">
       <div class="form-body">
        <h3 class="box-title">Probis <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">
        <?php if ($no_usulan != '') { ?>
         <div class="row">
          <div class="col-md-6">
           <div class="row">
            <div class="col-md-3">
             No Usulan
            </div>
            <div class="col-md-9 text-primary text-left">
             <a href="<?php echo base_url() . $module . '/detailUsulan/' . $id_usulan ?>"><?php echo $no_usulan ?></a>
            </div>
           </div>         
          </div>         
         </div>
         <br/>
        <?php } ?>

        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            No Probis
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $no_probis ?>
           </div>
          </div>         
         </div>         
        </div>
        <br/>

        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Nama Probis
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $nama_probis ?>
           </div>
          </div>
         </div>
        </div>
        <br/>

        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Tahun
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $tahun ?>
           </div>
          </div>
         </div>
        </div>
        <br/>

        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Tanggal
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $tgl_update ?>
           </div>
          </div>
         </div>
        </div>
        <br/>

        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Edisi
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $edisi ?>
           </div>
          </div>
         </div>
        </div>
        <br/>

        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Revisi
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $revisi ?>
           </div>
          </div>
         </div>
        </div>
        <br/>

        <?php if ($this->session->userdata('hak_akses') != 'General') { ?>
         <div class="row">
          <div class="col-md-6">
           <div class="row">
            <div class="col-md-3">
             File Probis (.xls)
            </div>
            <div class="col-md-9 text-primary text-left">
             <label class='label label-success font-12'><a class='text-white' href="<?php echo base_url() . 'files/berkas/probis/' . $file_excel ?>"><?php echo $file_excel ?></a></label>
            </div>
           </div>         
          </div>         
         </div>
         <br/>
        <?php } ?>        

        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            File Probis (.pdf)
           </div>
           <div class="col-md-9 text-primary text-left">
            <label class='label label-danger font-12' onclick="Probis.showLogo(this, event)"><?php echo $file_pdf ?></label>
           </div>
          </div>
         </div>
        </div>
       </div>
       <br/>
       <br/>

       <h3 class="box-title">Keyword Pencarian Probis <i class="fa fa-arrow-down"></i></h3>
       <hr class="m-t-0 m-b-40">

       <div class="row">
        <div class="col-md-12">
         <div class="table-responsive">
          <table class="table color-bordered-table primary-bordered-table" id="table_keyword">
           <thead>
            <tr class="">
             <th class="font-12">Keyword</th>
            </tr>
           </thead>
           <tbody>
            <?php if (isset($list_keyword)) { ?>
             <?php foreach ($list_keyword as $value) { ?>
              <tr id="<?php echo $value['id'] ?>">
               <td class="font-12"><?php echo $value['keyword'] ?></td>
              </tr>
             <?php } ?>
            <?php } ?>
           </tbody>
          </table>
         </div>
        </div>
       </div>

       <input type="hidden" value="<?php echo $referensi ?>" id="ref" class="form-control" />
       <div class="form-actions">
        <div class="row">
         <div class="col-md-12">
          <div class="row">
           <div class="col-md-offset-3 col-md-9 text-right">
            <button type="button" class="btn btn-default" onclick="Probis.back()">Kembali</button>
           </div>
          </div>
         </div>
         <div class="col-md-6"> </div>
        </div>
       </div>
      </form>
     </div>
    </div>
   </div>
  </div>
 </div>
 <!--./row--> 
</div>

<div class='container-fluid'>

 <div class="white-box stat-widget">  
  <div class="card-body">
   <h4 class="card-title"><u><?php echo $title ?></u></h4>

   <div class='row'>
    <div class='col-md-2'>
    </div>
    <div class='col-md-10'>
     <input type='text' name='' id='' onkeyup="UsulanRecjected.search(this, event)" class='form-control' value='' placeholder="Pencarian"/>    
    </div>     
   </div>        
   <br/>
   <div class='row'>
    <div class='col-md-12'>
     <?php if (isset($keyword)) { ?>
      <?php if ($keyword != '') { ?>
       Cari Data : "<b><?php echo $keyword; ?></b>"
      <?php } ?>
     <?php } ?>
    </div>
   </div>
   <br/>
   <hr/>

   <div class='row'>
    <div class='col-md-12'>
     <div class="table-responsive">
      <table class="table color-bordered-table primary-bordered-table">
       <thead>
        <tr class="">
         <th class="font-12">No</th>
         <th class="font-12">No. Usulan</th>
         <th class="font-12">Nama Probis</th>
         <th class="font-12">Pembuat</th>
         <th class="font-12">Status</th>
         <th class="text-center font-12">Action</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($content)) { ?>
         <?php $no = $pagination['last_no'] + 1; ?>
         <?php foreach ($content as $value) { ?>
          <tr>
           <td class='font-12'><?php echo $no++ ?></td>
           <td class='font-12'><?php echo $value['no_probis'] ?></td>
           <td class='font-12'><?php echo $value['nama_probis'] ?></td>
           <td class='font-12'><?php echo $value['username'] ?></td>
           <td class='font-12'><label class="label label-success"><?php echo $value['status'] ?></label></td>
           <td class="text-center">
            <label id="" class="label label-success font-10" 
                   onclick="UsulanRecjected.detail('<?php echo $value['id'] ?>')">Detail</label>
            &nbsp;
           </td>
          </tr>
         <?php } ?>
        <?php } else { ?>
         <tr>
          <td class="text-center font-12" colspan="8">Tidak Ada Data Ditemukan</td>
         </tr>
        <?php } ?>         
       </tbody>
      </table>
     </div>
    </div>
   </div>    
   <br/>
   <div class="row">
    <div class="col-md-12">
     <div class="pagination">
      <?php echo $pagination['links'] ?>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
<?php

class Grafik extends MX_Controller {

 public $hak_akses;
 public $upt;

 public function __construct() {
  parent::__construct();
  date_default_timezone_set("Asia/Jakarta");
  $this->hak_akses = $this->session->userdata('hak_akses');
  $this->upt = $this->session->userdata('upt_id');
 }

 public function getModuleName() {
  return 'grafik';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/jquery_min_latest.js"></script>',
      '<script src="' . base_url() . 'assets/js/moment_min.js"></script>',
      '<script src="' . base_url() . 'assets/js/daterangepicker.js"></script>',
      '<link href="' . base_url() . 'assets/css/daterangepicker.css" type="text/css" rel="stylesheet"/>',
      '<script src="' . base_url() . 'assets/js/controllers/grafik.js"></script>',
  );

  return $data;
 }

 public function index() {
  $data['view_file'] = 'v_index';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Grafik";
  $data['title_content'] = 'Grafik';
  $data['data_probis_setuju'] = $this->getDataProbisSetuju();
  $data['data_probis_dilihat'] = $this->getDataProbisDilihat();
  $data['data_probis_done'] = $this->getDataProbisSudahAda();
  $data['data_probis_proses'] = $this->getDataProbisProses();
  $data['data_probis_belum'] = $this->getDataProbisBelum();
  echo Modules::run('template', $data);
 }

 public function getDataProbisSetuju() {
  $date = date('Y-m');
  $where = "t.deleted = 0 and t.upt = '" . $this->upt . "'";
  if ($this->hak_akses == 'Superadmin') {
   $where = 't.deleted = 0';
  }

  $like = array(
      array('t.createddate', $date)
  );

  $total = Modules::run('database/count_all', array(
              'table' => 'pengajuan_probis t',
              'field' => array('t.*', 'u.username', 'ps.status'),
              'join' => array(
                  array('user u', 't.user = u.id'),
                  array('(select max(id) as id, pengajuan_probis from pengajuan_probis_has_status GROUP by pengajuan_probis) st', 't.id = st.pengajuan_probis'),
                  array('pengajuan_probis_has_status ps', "st.id = ps.id and (ps.status = 'APPROVED')")
              ),
              'is_or_like' => true,
              'like' => $like,
              'inside_brackets' => true,
              'where' => $where
  ));

  $result = array();
  $hasil['jumlah'] = $total;
  $hasil['probis'] = 'Pengajuan Probis Disetujui';
  array_push($result, $hasil);

  return array(
      'data' => json_encode($result)
  );
 }

 public function getDataProbisSudahAda() {
  $date = date('Y-m');
  $hak_akses = $this->session->userdata('hak_akses');

  $where = "t.deleted = 0 and t.upt = '" . $this->upt . "' and ps.status = 'DONE'";
  if ($this->hak_akses == 'Superadmin') {
   $where = "t.deleted = 0 and ps.status = 'DONE'";
  }

  $total = Modules::run('database/count_all', array(
              'table' => 'probis t',
              'field' => array('t.*', 'u.username', 'ps.status'),
              'join' => array(
                  array('user u', 't.user = u.id'),
                  array('upt ut', 't.upt = ut.id'),
                  array('(select max(id) id, probis from probis_status group by probis) pss', 't.id = pss.probis', 'left'),
                  array('probis_status ps', 'pss.id = ps.id'),
              ),
              'where' => $where
  ));
  
  $result = array();
  $hasil['jumlah'] = $total;
  $hasil['probis'] = 'Pengajuan Probis Sudah Ada';
  array_push($result, $hasil);

  return array(
      'data' => json_encode($result)
  );
 }
 
 public function getDataProbisProses() {
  $date = date('Y-m');
  $hak_akses = $this->session->userdata('hak_akses');

  $where = "t.deleted = 0 and t.upt = '" . $this->upt . "' and ps.status = 'PROGRESS'";
  if ($this->hak_akses == 'Superadmin') {
   $where = "t.deleted = 0 and ps.status = 'PROGRESS'";
  }

  $total = Modules::run('database/count_all', array(
              'table' => 'probis t',
              'field' => array('t.*', 'u.username', 'ps.status'),
              'join' => array(
                  array('user u', 't.user = u.id'),
                  array('upt ut', 't.upt = ut.id'),
                  array('(select max(id) id, probis from probis_status group by probis) pss', 't.id = pss.probis', 'left'),
                  array('probis_status ps', 'pss.id = ps.id'),
              ),
              'where' => $where
  ));
  
  $result = array();
  $hasil['jumlah'] = $total;
  $hasil['probis'] = 'Pengajuan Probis On Proses';
  array_push($result, $hasil);

  return array(
      'data' => json_encode($result)
  );
 }
 
 public function getDataProbisBelum() {
  $date = date('Y-m');
  $hak_akses = $this->session->userdata('hak_akses');

  $where = "t.deleted = 0 and t.upt = '" . $this->upt . "' and ps.status = 'ONCOMING' or ps.status is null";
  if ($this->hak_akses == 'Superadmin') {
   $where = "t.deleted = 0 and ps.status = 'ONCOMING' or ps.status is null";
  }

  $total = Modules::run('database/count_all', array(
              'table' => 'probis t',
              'field' => array('t.*', 'u.username', 'ps.status'),
              'join' => array(
                  array('user u', 't.user = u.id'),
                  array('upt ut', 't.upt = ut.id'),
                  array('(select max(id) id, probis from probis_status group by probis) pss', 't.id = pss.probis', 'left'),
                  array('probis_status ps', 'pss.id = ps.id'),
              ),
              'where' => $where
  ));
  
  $result = array();
  $hasil['jumlah'] = $total;
  $hasil['probis'] = 'Pengajuan Probis Belum Ada';
  array_push($result, $hasil);

  return array(
      'data' => json_encode($result)
  );
 }

 public function getDataProbisDitolak() {
  $date = date('Y-m');
  $where = "t.deleted = 0 and t.upt = '" . $this->upt . "'";
  if ($this->hak_akses == 'Superadmin') {
   $where = 't.deleted = 0';
  }

  $like = array(
      array('t.createddate', $date)
  );
  $total = Modules::run('database/count_all', array(
              'table' => 'pengajuan_probis t',
              'field' => array('t.*', 'u.username', 'ps.status'),
              'join' => array(
                  array('user u', 't.user = u.id'),
                  array('(select max(id) as id, pengajuan_probis from pengajuan_probis_has_status GROUP by pengajuan_probis) st', 't.id = st.pengajuan_probis'),
                  array('pengajuan_probis_has_status ps', "st.id = ps.id and (ps.status = 'REJECTED')")
              ),
              'is_or_like' => true,
              'like' => $like,
              'inside_brackets' => true,
              'where' => $where
  ));

  $result = array();
  $hasil['jumlah'] = $total;
  $hasil['probis'] = 'Pengajuan Probis Ditolak';
  array_push($result, $hasil);

  return array(
      'data' => json_encode($result)
  );
 }

 public function getDataProbisDilihat() {
  $date = date('Y-m');
  $where = "t.deleted = 0 and t.upt = '" . $this->upt . "'";
  if ($this->hak_akses == 'Superadmin') {
   $where = 't.deleted = 0';
  }

  $query = "SELECT count(*) jumlah, probis FROM log_probis "
          . "where createddate like '%" . $date . "%' and deleted = 0 "
          . "GROUP BY probis order by jumlah desc";

  $data = Modules::run('database/get_custom', $query);
  $result = array();

  $total = 0;
  $probis = '';
  if (!empty($data)) {
   $data = $data->row_array();
   $total = $data['jumlah'];
   $probis_data = $this->getNamaProbis($data['probis']);
   $probis = !empty($probis_data) ? $probis_data['nama_probis'] : '';
  }

  $hasil['jumlah'] = $total;
  $hasil['probis'] = $probis;
  array_push($result, $hasil);

  return array(
      'data' => json_encode($result)
  );
 }

 public function getNamaProbis($probis) {
  $data = Modules::run('database/get', array(
              'table' => 'probis',
              'where' => array('id' => $probis)
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
  }


  return $result;
 }

 public function getDataProbisDirevisi() {
  $date = date('Y-m');
  $where = "t.deleted = 0 and t.upt = '" . $this->upt . "'";
  if ($this->hak_akses == 'Superadmin') {
   $where = 't.deleted = 0';
  }

  $like = array(
      array('t.createddate', $date)
  );
  $total = Modules::run('database/count_all', array(
              'table' => 'pengajuan_probis t',
              'field' => array('t.*', 'u.username', 'ps.status'),
              'join' => array(
                  array('user u', 't.user = u.id'),
                  array('(select max(id) as id, pengajuan_probis from pengajuan_probis_has_status GROUP by pengajuan_probis) st', 't.id = st.pengajuan_probis'),
                  array('pengajuan_probis_has_status ps', "st.id = ps.id and (ps.status = 'REVISION')")
              ),
              'is_or_like' => true,
              'like' => $like,
              'inside_brackets' => true,
              'where' => $where
  ));

  $result = array();
  $hasil['jumlah'] = $total;
  $hasil['probis'] = 'Pengajuan Probis Direvisi';
  array_push($result, $hasil);

  return array(
      'data' => json_encode($result)
  );
 }

}

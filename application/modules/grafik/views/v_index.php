<input type='hidden' name='' id='hak_akses' class='form-control' value='<?php echo $this->session->userdata('hak_akses') ?>'/>
<div class="container-fluid">
 <div class='row'>
  <div class='col-md-12'>

   <div class="row">
    <div class="col-md-4">
     <h4>Grafik Probis Sudah Ada</h4>
    </div>    
    <div class="col-md-4">
     <h4>Grafik Probis On Proses</h4>
    </div> 
    <div class="col-md-4">
     <h4>Grafik Probis Belum Ada</h4>
    </div> 
   </div>   

   <div class="row">
    <div class="col-md-4">
     <div class='white-box stat-widget'>
      <div class='card-body'>             
       <h4>Probis Sudah Ada</h4>
       <br/>  
       <div id="canvas_probis_ada"></div>
       <script>
        $(function () {
         window.BarAda = Morris.Bar({
          element: 'canvas_probis_ada',
          data: <?php echo $data_probis_done['data'] ?>,
          xkey: 'probis',
          ykeys: ['jumlah'],
          ymax: 100,
          ymin: 0,
          labels: ['Jumlah'],
          resize: true,
          lineWidth: '3px',
          redraw: true,
          barColors: ["#2ecc71"]
         });

         $(window).resize(function () {
          window.BarAda.redraw();
         });
        });
       </script>
      </div>
     </div>
    </div>
    
    <div class="col-md-4">
     <div class='white-box stat-widget'>
      <div class='card-body'>             
       <h4>Probis On Progress</h4>
       <br/>  
       <div id="canvas_probis_proses"></div>
       <script>
        $(function () {
         window.BarProses = Morris.Bar({
          element: 'canvas_probis_proses',
          data: <?php echo $data_probis_proses['data'] ?>,
          xkey: 'probis',
          ykeys: ['jumlah'],
          ymax: 100,
          ymin: 0,
          labels: ['Jumlah'],
          resize: true,
          lineWidth: '3px',
          redraw: true,
          barColors: ["#e74a25"]
         });

         $(window).resize(function () {
          window.BarProses.redraw();
         });
        });
       </script>
      </div>
     </div>
    </div>
    
    <div class="col-md-4">
     <div class='white-box stat-widget'>
      <div class='card-body'>             
       <h4>Probis Belum Ada</h4>
       <br/>  
       <div id="canvas_probis_belum"></div>
       <script>
        $(function () {
         window.BarBelum = Morris.Bar({
          element: 'canvas_probis_belum',
          data: <?php echo $data_probis_belum['data'] ?>,
          xkey: 'probis',
          ykeys: ['jumlah'],
          ymax: 100,
          ymin: 0,
          labels: ['Jumlah'],
          resize: true,
          lineWidth: '3px',
          redraw: true,
          barColors: ["#ffb136"]
         });

         $(window).resize(function () {
          window.BarBelum.redraw();
         });
        });
       </script>
      </div>
     </div>
    </div>
   </div>
   
   <div class="row">
    <div class="col-md-6">
     <h4>Grafik Pengajuan Probis Disetujui <label class="badge badge-success"><?php echo date('Y F') ?></label></h4>
    </div>    
    <div class="col-md-6">
     <h4>Grafik Probis Sering Dilihat <label class="badge badge-warning"><?php echo date('Y F') ?></label></h4>
    </div> 
   </div>   

   <div class="row">
    <div class="col-md-6">   
     <div class='white-box stat-widget'>
      <div class='card-body'>             
       <h4>Probis Disetujui</h4>
       <br/>  
       <div id="canvas_probis_setuju"></div>
       <script>
        $(function () {
         window.BarSetuju = Morris.Bar({
          element: 'canvas_probis_setuju',
          data: <?php echo $data_probis_setuju['data'] ?>,
          xkey: 'probis',
          ykeys: ['jumlah'],
          ymax: 100,
          ymin: 0,
          labels: ['Jumlah'],
          resize: true,
          lineWidth: '3px',
          redraw: true,
          barColors: ["#2ecc71"]
         });

         $(window).resize(function () {
          window.BarSetuju.redraw();
         });
        });
       </script>
      </div>
     </div>
    </div>        

    <div class="col-md-6">   
     <div class='white-box stat-widget'>
      <div class='card-body'>             
       <h4>Probis Sering Dilihat</h4>
       <br/>  
       <div id="canvas_probis_dilihat"></div>
       <script>
        $(function () {
         window.BarDilihat = Morris.Bar({
          element: 'canvas_probis_dilihat',
          data: <?php echo $data_probis_dilihat['data'] ?>,
          xkey: 'probis',
          ykeys: ['jumlah'],
          ymax: 100,
          ymin: 0,
          labels: ['Jumlah'],
          resize: true,
          lineWidth: '3px',
          redraw: true,
          barColors: ["#e74a25"]
         });

         $(window).resize(function () {
          window.BarDilihat.redraw();
         });
        });
       </script>
      </div>
     </div>
    </div> 
   </div>  
  </div>
 </div> 
</div>
<div class='container-fluid'>

 <div class="white-box stat-widget">  
  <div class="card-body">
   <h4 class="card-title"><u><?php echo $title ?></u></h4>

   <div class='row'>
    <div class='col-md-2'>
     <button id="" class="btn btn-block btn-warning" onclick="RuleProbis.add()">Tambah</button>
    </div>
    <div class='col-md-10'>
     <!--<input type='text' name='' id='' onkeyup="RuleProbis.search(this, event)" class='form-control' value='' placeholder="Pencarian"/>-->    
    </div>     
   </div>        
   <br/>
   <div class='row'>
    <div class='col-md-12'>
     <?php if (isset($keyword)) { ?>
      <?php if ($keyword != '') { ?>
       Cari Data : "<b><?php echo $keyword; ?></b>"
      <?php } ?>
     <?php } ?>
    </div>
   </div>
   <br/>
   <hr/>

   <div class='row'>
    <div class='col-md-12'>
     <div class="table-responsive">
      <table class="table color-bordered-table primary-bordered-table">
       <thead>
        <tr class="">
         <th class="font-12">No</th>
         <th class="font-12">File</th>
         <th class="text-center font-12">Action</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($content)) { ?>
         <?php $no = 1; ?>
         <?php foreach ($content as $value) { ?>
          <tr>
           <td class='font-12'><?php echo $no++ ?></td>
           <td class='font-12'><?php echo $value['file'] ?></td>
           <td class="text-center">
            <?php if ($this->session->userdata('hak_akses') == 'Superadmin') { ?>
             <label id="" class="label label-warning font-10 hover" 
                    onclick="RuleProbis.ubah('<?php echo $value['id'] ?>')">Ubah</label>
             &nbsp;
             <label id="" class="label label-success font-10 hover" 
                    onclick="RuleProbis.detail('<?php echo $value['id'] ?>')">Detail</label>
             &nbsp;
             <label id="" class="label label-info font-10 hover" 
                    onclick="RuleProbis.aktif(this, '<?php echo $value['id'] ?>')"><?php echo $value['view'] ? 'Nonaktifkan' : 'Aktifkan' ?></label>
             &nbsp;
             <label id="" class="label label-danger font-10 hover" 
                    onclick="RuleProbis.delete('<?php echo $value['id'] ?>')">Hapus</label>
                   <?php } ?>             
           </td>
          </tr>
         <?php } ?>
        <?php } else { ?>
         <tr>
          <td class="text-center font-12" colspan="8">Tidak Ada Data Ditemukan</td>
         </tr>
        <?php } ?>         
       </tbody>
      </table>
     </div>
    </div>
   </div>    
   <br/>
   <div class="row">
    <div class="col-md-12">
     <div class="pagination">
      <?php echo $pagination['links'] ?>
     </div>
    </div>
   </div>
  </div>
 </div>
 <!-- <div class="row">
    column 
   <div class="col-lg-12">
    <div class="card">
     <div class="card-block">
      <div class='row'>
       <div class='col-md-4'>
        <label id="" class="label label-warning" onclick="RuleProbis.add()">Tambah</label>
       </div>
       <div class='col-md-8'>
        <input type='text' name='' id='' onkeyup="RuleProbis.search(this, event)" class='form-control' value='' placeholder="Pencarian"/>    
       </div>     
      </div>        
      <br/>
      <div class='row'>
       <div class='col-md-12'>
 <?php if (isset($keyword)) { ?>
  <?php if ($keyword != '') { ?>
                                                      Cari Data : "<b><?php echo $keyword; ?></b>"
  <?php } ?>
 <?php } ?>
       </div>
      </div>
      <br/>
      <div class='row'>
       <div class='col-md-12'>
        <div class="table-responsive">
         <table class="table table-bordered table-striped">
          <thead>
           <tr>
            <th class="text-white bg-theme">No</th>
            <th class="text-white bg-theme">Nama UPT</th>
            <th class="text-white bg-theme">Alamat</th>
            <th class="text-white bg-theme">Action</th>
           </tr>
          </thead>
          <tbody>
 <?php if (!empty($content)) { ?>
  <?php $no = 1; ?>
  <?php foreach ($content as $value) { ?>
                                                         <tr>
                                                          <td><?php echo $no++ ?></td>
                                                          <td><?php echo $value['upt'] ?></td>
                                                          <td><?php echo $value['alamat'] ?></td>
                                                          <td class="text-center">
                                                           <label id="" class="label label-warning font12" 
                                                                   onclick="RuleProbis.ubah('<?php echo $value['id'] ?>')">Ubah</label>
                                                           &nbsp;
                                                           <label id="" class="label label-success font12" 
                                                                   onclick="RuleProbis.detail('<?php echo $value['id'] ?>')">Detail</label>
                                                           &nbsp;
                                                           <label id="" class="label label-danger font12" 
                                                                   onclick="RuleProbis.delete('<?php echo $value['id'] ?>')">Hapus</label>
                                                           &nbsp;
                                                          </td>
                                                         </tr>
  <?php } ?>
 <?php } else { ?>
                                  <tr>
                                   <td class="text-center" colspan="4">Tidak Ada Data Ditemukan</td>
                                  </tr>
 <?php } ?>         
          </tbody>
         </table>
        </div>
       </div>
      </div>    
      <br/>
      <div class="row">
       <div class="col-md-12">
        <div class="pagination">
 <?php echo $pagination['links'] ?>
        </div>
       </div>
      </div>
     </div>
    </div>
   </div>
  </div>-->
</div>
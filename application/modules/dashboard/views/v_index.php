<input type='hidden' name='' id='hak_akses' class='form-control' value='<?php echo $this->session->userdata('hak_akses') ?>'/>
<link href="<?php echo base_url() ?>assets/plugins/components/vegas/vegas.min.css" rel="stylesheet">
<script src="<?php echo base_url() ?>assets/plugins/components/vegas/vegas.min.js"></script>
<div class="container-fluid">
 <div class='row'>
  <div class='col-md-12'>

   <img src="<?php echo base_url() . 'assets/images/background/image_probis.jpg' ?>" style="width: 100%;height: 300px;"/>
   <div class="row">
    <div class="col-lg-12">
          
<!--     <div class="panel panel-default">
      <div class="panel-heading">Aplikasi Proses Bisnis Manajemen Aset</div>
      <div class="panel-wrapper p-b-10 collapse in" id='header_dashboard' 
           style="height: 300px;width: 100%;
           background-size: cover;
           content: url('<?php echo base_url() . 'assets/images/background/image_probis.jpg' ?>');
           margin:0;padding:0;">

      </div>
     </div>-->
    </div>
   </div>
   <br/>

   <div class="row colorbox-group-widget">
    <div class="col-md-3 col-sm-6 info-color-box">
     <div class="white-box">
      <div class="media bg-primary">
       <div class="media-body hover" onclick="Dashboard.infoProbis()">
        <h3 class="info-count">Definisi Probis<span class="pull-right"><i class="mdi mdi-calendar-question"></i></span></h3>
        <p class="info-ot font-15">Apa itu probis (proses bisnis)<span class="label label-rounded">?</span></p>
       </div>
       <br/>
      </div>      
     </div>
    </div>
    <div class="col-md-3 col-sm-6 info-color-box">
     <div class="white-box">
      <div class="media bg-success">
       <div class="media-body hover" onclick="Dashboard.gotoProbis()">
        <h3 class="info-count">Probis UITJBTB<span class="pull-right"><i class="mdi mdi-comment-text-outline"></i></span></h3>
        <p class="info-ot font-15">Proses bisnis UITJBTB<span class="label label-rounded"></span></p>
       </div>
       <br/>
       <br/>
      </div>
     </div>
    </div>
    <div class="col-md-3 col-sm-6 info-color-box">
     <div class="white-box">
      <div class="media bg-danger">
       <div class="media-body hover" onclick="Dashboard.showPeraturanProbis()">
        <h3 class="info-count">Peraturan Probis<span class="pull-right"><i class="mdi mdi-comment-text-outline"></i></span></h3>
        <p class="info-ot font-15">Aturan yang berlaku<span class="label label-rounded"></span></p>
       </div>
       <br/>
       <br/>
      </div>
     </div>
    </div>
    <div class="col-md-3 col-sm-6 info-color-box">
     <div class="white-box">
      <div class="media bg-warning">
       <div class="media-body hover" onclick="Dashboard.gotoReferensi()">
        <h3 class="info-count">Dokumen Probis<span class="pull-right"><i class="mdi mdi-file-import"></i></span></h3>
        <p class="info-ot font-15">Daftar Dokumen Probis<span class="label label-rounded"></span></p>
       </div>
       <br/>
       <br/>
      </div>
     </div>
    </div>
   </div>

   <?php if ($this->session->userdata('hak_akses') == 'Admin' || $this->session->userdata('hak_akses') == 'Superadmin') { ?>
    <div class="white-box">
     <div class="row">
      <div class="col-md-12">
       <h4>Top <label class="badge badge-danger">10</label> Proses Usulan Bisnis</h4>
      </div>
     </div>
     <div class="row">
      <div class="col-md-12">
       <div class="table-responsive">
        <table class="table color-bordered-table primary-bordered-table">
         <thead>
          <tr class="">
           <th class="font-12">No</th>
           <th class="font-12">No. Usulan</th>
           <th class="font-12">Nama Probis</th>
           <th class="font-12">Status</th>
          </tr>
         </thead>
         <tbody>
          <?php if (!empty($pengajuan)) { ?>
           <?php $no = 1; ?>
           <?php foreach ($pengajuan as $value) { ?>
            <tr>
             <td class='font-12'><?php echo $no++ ?></td>
             <td class='font-12'><?php echo $value['no_probis'] ?></td>
             <td class='font-12'><?php echo $value['nama_probis'] ?></td>
             <?php if ($value['status'] == 'REVIEWED') { ?>
              <td class='font-12'><label class="label label-warning"><?php echo $value['status'] ?></label></td>
             <?php } ?>
             <?php if ($value['status'] == 'DRAFT') { ?>
              <td class='font-12'><label class="label label-info"><?php echo $value['status'] ?></label></td>
             <?php } ?>
            </tr>
           <?php } ?>
          <?php } else { ?>
           <tr>
            <td class="text-center font-12" colspan="8">Tidak Ada Data Ditemukan</td>
           </tr>
          <?php } ?>         
         </tbody>
        </table>
       </div>
      </div>
     </div>
    </div>
   <?php } ?>
  </div>
 </div> 
</div>

<script>
// $("#header_dashboard").vegas({
//  cover:true,
//  slides: [
//   {src: '<?php echo base_url() . 'assets/images/background/image_probis.jpg' ?>'}
//  ],
// });
</script>

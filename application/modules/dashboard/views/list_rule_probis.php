<div class='row'>
 <div class='col-md-12'>
  <div class="table-responsive">
   <table class="table color-bordered-table primary-bordered-table">
    <thead>
     <tr class="">
      <th class="font-12">No</th>
      <th class="font-12">File</th>
      <th class="text-center font-12">Action</th>
     </tr>
    </thead>
    <tbody>
     <?php if (!empty($data)) { ?>
      <?php $no = 1; ?>
      <?php foreach ($data as $value) { ?>
       <tr>
        <td class='font-12'><?php echo $no++ ?></td>
        <td class='font-12'><?php echo $value['file'] ?></td>
        <td class="text-center">
         <label id="" class="label label-warning font-10 hover" 
                onclick="Dashboard.showDetailPeraturanProbis('<?php echo $value['id'] ?>')">Lihat</label>
        </td>
       </tr>
      <?php } ?>
     <?php } else { ?>
      <tr>
       <td class="text-center font-12" colspan="8">Tidak Ada Data Ditemukan</td>
      </tr>
     <?php } ?>         
    </tbody>
   </table>
  </div>
 </div>
</div> 
<?php

class Dashboard extends MX_Controller {

 public $hak_akses;
 public $upt;

 public function __construct() {
  parent::__construct();
  date_default_timezone_set("Asia/Jakarta");
  $this->hak_akses = $this->session->userdata('hak_akses');
  $this->upt = $this->session->userdata('upt_id');
 }

 public function getModuleName() {
  return 'dashboard';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/jquery_min_latest.js"></script>',
      '<script src="' . base_url() . 'assets/js/moment_min.js"></script>',
      '<script src="' . base_url() . 'assets/js/daterangepicker.js"></script>',
      '<link href="' . base_url() . 'assets/css/daterangepicker.css" type="text/css" rel="stylesheet"/>',
      '<script src="' . base_url() . 'assets/js/controllers/dashboard.js"></script>',
  );

  return $data;
 }

 public function index() {
  $data['view_file'] = 'v_index';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Dashboard";
  $data['title_content'] = 'Dashboard';
  $data['pengajuan'] = $this->getDraftPengajuanProbis();
  echo Modules::run('template', $data);
 }

 public function getDraftPengajuanProbis() {
  $where = "t.deleted = 0 and t.upt = '" . $this->upt . "'";
  if ($this->hak_akses == 'Superadmin') {
   $where = 't.deleted = 0';
  }
  $data = Modules::run('database/get', array(
              'table' => 'pengajuan_probis t',
              'field' => array('t.*', 'u.username', 'ps.status'),
              'join' => array(
                  array('user u', 't.user = u.id'),
                  array('(select max(id) as id, pengajuan_probis from pengajuan_probis_has_status GROUP by pengajuan_probis) st', 't.id = st.pengajuan_probis'),
                  array('pengajuan_probis_has_status ps', "st.id = ps.id and (ps.status = 'DRAFT' or ps.status = 'REVIEWED')")
              ),
              'where' => $where,
              'limit' => 10
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function showInfoProbis() {
  $data = Modules::run('database/get', array(
              'table' => 'info_probis',
              'where' => "deleted = 0"
  ));
  $keterangan = '';
  if(!empty($data)){
   $keterangan = $data->row_array()['keterangan'];
  }
  $content['keterangan'] = $keterangan;
  echo $this->load->view('file_info_probis', $content, true);
 }
 
 public function showDetailRuleProbis($id) {
  $data = Modules::run('database/get', array(
              'table' => 'rule_probis',
              'where' => array('id'=> $id)
  ));
  $foto = '';
  if(!empty($data)){
   $foto = $data->row_array()['file'];
  }
  $content['file'] = str_replace(' ', '_', $foto);
  echo $this->load->view('file_rule_probis', $content, true);
 }
 
 public function showRuleProbis() {
  $data = Modules::run('database/get', array(
              'table' => 'rule_probis',
              'where' => "deleted = 0 and view = 1"
  ));
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }
  
  $content['data'] = $result;
  echo $this->load->view('list_rule_probis', $content, true);
 }

}

<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<input type='hidden' name='' id='status' class='form-control' value='<?php echo isset($status) ? $status : '' ?>'/>
<div class="container-fluid"> 
 <!--.row-->
 <div class="row">
  <div class="col-md-12">
   <div class="panel panel-primary">
    <div class="panel-heading"> <?php echo $title_content ?></div>
    <div class="panel-wrapper collapse in" aria-expanded="true">
     <div class="panel-body">
      <form action="#" class="form-horizontal">
       <div class="form-body">
        <h3 class="box-title">Usulan <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">
        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Nama Probis</label>
           <div class="col-md-9">
            <input type="text" id='nama' class="form-control required" placeholder="Nama Probis" error='Nama Probis' value="<?php echo isset($nama_probis) ? $nama_probis : '' ?>">
           </div>
          </div>
         </div>         
         <!--/span-->
        </div>       

        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">File Usulan (.xls)</label>
           <div class="col-sm-9">
            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
             <div class="form-control" data-trigger="fileinput"> 
              <i class="glyphicon glyphicon-file fileinput-exists"></i> 
              <span class="fileinput-filename"><?php echo isset($file_excel) ? $file_excel : '' ?></span>
             </div> 
             <span class="input-group-addon btn btn-default btn-file"> 
              <span class="fileinput-new">Select file</span> 
              <span class="fileinput-exists">Change</span>
              <input id='probis_xls' type="file" name="..." onchange="Usulan.getFilename(this, 'xls')"> 
             </span> 
             <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> 
            </div>
           </div>
          </div>
         </div> 
        </div>       

        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">File Usulan (.pdf)</label>
           <div class="col-sm-9">
            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
             <div class="form-control" data-trigger="fileinput"> 
              <i class="glyphicon glyphicon-file fileinput-exists"></i> 
              <span class="fileinput-filename"><?php echo isset($file_pdf) ? $file_pdf : '' ?></span>
             </div> 
             <span class="input-group-addon btn btn-default btn-file"> 
              <span class="fileinput-new">Select file</span> 
              <span class="fileinput-exists">Change</span>
              <input id='probis_pdf' type="file" name="..." onchange="Usulan.getFilename(this, 'pdf')"> 
             </span> 
             <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> 
            </div>
           </div>
          </div>
         </div> 
        </div>       
       </div>

       <div class="form-actions">
        <div class="row">
         <div class="col-md-12">
          <div class="row">
           <div class="col-md-offset-3 col-md-9 text-right">      
            <?php if (isset($status)) { ?>
             <?php if ($status == 'REVISION') { ?>
              <button type="submit" class="btn btn-success" onclick="Usulan.simpan('<?php echo isset($id) ? $id : '' ?>', event)">REVISI</button>
             <?php } ?>
            <?php } else { ?>
             <button type="submit" class="btn btn-success" onclick="Usulan.simpan('<?php echo isset($id) ? $id : '' ?>', event)">Submit</button>
            <?php } ?>
            <button type="button" class="btn btn-default" onclick="Usulan.back()">Batal</button>
           </div>
          </div>
         </div>
         <div class="col-md-6"> </div>
        </div>
       </div>


       <?php if (isset($status)) { ?>
        <?php if ($status == 'REVISION') { ?>
         <div class="row">
          <div class="col-md-12">
           <h3 class="box-title">Review Dokumen Oleh Admin <i class="fa fa-arrow-down"></i></h3>
           <hr class="m-t-0 m-b-40">
           <ul class="timeline">
            <li class="">
             <div class="timeline-badge success"><img class="img-responsive" alt="user" src="<?php echo base_url() . 'assets/images/users/images.png' ?>"> </div>
             <div class="timeline-panel">
              <div class="timeline-heading">
               <h4 class="timeline-title"><?php echo $status ?></h4>
               <p><small class="text-muted"><i class="fa fa-inbox"></i> <?php echo $createddate ?></small> </p>
               <p><small class="text-muted"><i class="fa fa-clock-o"></i> <?php echo $keterangan ?></small> </p>
              </div>
              <div class="timeline-body">
               <p><?php 'Oleh ' ?><label class="label label-info"><?php echo $nama_pegawai ?></label></p>
              </div>
             </div>
            </li>
           </ul>
          </div>
         </div>
        <?php } ?>
       <?php } ?>
      </form>
     </div>
    </div>
   </div>
  </div>
 </div>
 <!--./row--> 
</div>

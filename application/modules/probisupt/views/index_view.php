<div class='container-fluid'>

 <div class="white-box stat-widget">  
  <div class="card-body">
   <h4 class="card-title"><u><?php echo $title ?></u></h4>

   <div class='row'>
    <div class='col-md-2'>
     &nbsp;
    </div>
    <div class='col-md-10'>
     <input type='text' name='' id='' onkeyup="ProbisUpt.search(this, event)" class='form-control' value='' placeholder="Pencarian"/>    
    </div>     
   </div>        
   <br/>
   <div class='row'>
    <div class='col-md-12'>
     <?php if (isset($keyword)) { ?>
      <?php if ($keyword != '') { ?>
       Cari Data : "<b><?php echo $keyword; ?></b>"
      <?php } ?>
     <?php } ?>
    </div>
   </div>
   <br/>
   <hr/>

   <div class='row'>
    <div class='col-md-12'>
     <div class="table-responsive">
      <table class="table color-bordered-table primary-bordered-table">
       <thead>
        <tr class="">
         <th class="font-12">No</th>
         <th class="font-12">No. Probis</th>
         <th class="font-12">Nama Probis</th>
         <th class="font-12">Upt</th>
         <th class="font-12">Tahun</th>
         <th class="font-12">Tanggal Update</th>
         <th class="font-12">Pembuat</th>
         <th class="font-12">Edisi</th>
         <th class="font-12">Revisi</th>
         <th class="font-12">Status</th>
         <th class="font-12">Keterangan</th>        
         <th class="text-center font-12">Action</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($content)) { ?>
         <?php $no = $pagination['last_no'] + 1; ?>
         <?php foreach ($content as $value) { ?>
          <tr>
           <td class='font-12'><?php echo $no++ ?></td>
           <td class='font-12'><?php echo $value['no_probis'] ?></td>
           <td class='font-12'><?php echo $value['nama_probis'] ?></td>
           <td class='font-12'><?php echo $value['nama_upt'] ?></td>
           <td class='font-12'><?php echo $value['tahun'] ?></td>
           <td class='font-12'><?php echo $value['tgl_update'] ?></td>
           <td class='font-12'><?php echo $value['username'] ?></td>
           <td class='font-12'><?php echo $value['edisi'] ?></td>
           <td class='font-12'><?php echo $value['revisi'] ?></td>
           <?php $color = ""; ?>
           <?php if ($value['status'] == 'PROGRESS') { ?>
            <?php $color = 'label-warning'; ?>
           <?php } ?>
           <?php if ($value['status'] == 'ONCOMING') { ?>
            <?php $color = 'label-primary'; ?>
           <?php } ?>
           <?php if ($value['status'] == 'DONE') { ?>
            <?php $color = 'label-success'; ?>
           <?php } ?>
           <?php if ($value['status'] == 'PROSES REVISI') { ?>
            <?php $color = 'label-info'; ?>
           <?php } ?>
           <td class='font-12'><label class="label <?php echo $color; ?>"><?php echo $value['status'] ?></label></td>
           <td class='font-12'><?php echo $value['keterangan'] ?></td>
           <td class="text-center">            
            <label id="" class="label label-success font-10" 
                   onclick="ProbisUpt.detail('<?php echo $value['id'] ?>')">Detail</label>
            &nbsp;
            <a id="" class="label label-success font-10 hover" 
               href="<?php echo base_url() . $module . '/download/' . $value['id'] ?>">Download (.xls)</a>
            &nbsp;
            <?php if ($value['file_pdf'] != '') { ?>
             <label id="" class="label label-danger font-10 hover" 
                    onclick="ProbisUpt.previewFile('<?php echo $value['file_pdf'] ?>')">Preview (.pdf)</label>
             &nbsp;             
            <?php } ?>
            <label id="" class="label label-primary font-10 hover" 
                   onclick="ProbisUpt.feedback('<?php echo $value['id'] ?>')">Feedback</label>
            &nbsp;
           </td>
          </tr>
         <?php } ?>
        <?php } else { ?>
         <tr>
          <td class="text-center font-12" colspan="12">Tidak Ada Data Ditemukan</td>
         </tr>
        <?php } ?>         
       </tbody>
      </table>
     </div>
    </div>
   </div>    
   <br/>
   <div class="row">
    <div class="col-md-12">
     <div class="pagination">
      <?php echo $pagination['links'] ?>
     </div>
    </div>
   </div>
  </div>
 </div>
 <!-- <div class="row">
    column 
   <div class="col-lg-12">
    <div class="card">
     <div class="card-block">
      <div class='row'>
       <div class='col-md-4'>
        <label id="" class="label label-warning" onclick="ProbisUpt.add()">Tambah</label>
       </div>
       <div class='col-md-8'>
        <input type='text' name='' id='' onkeyup="ProbisUpt.search(this, event)" class='form-control' value='' placeholder="Pencarian"/>    
       </div>     
      </div>        
      <br/>
      <div class='row'>
       <div class='col-md-12'>
 <?php if (isset($keyword)) { ?>
  <?php if ($keyword != '') { ?>
                                                                        Cari Data : "<b><?php echo $keyword; ?></b>"
  <?php } ?>
 <?php } ?>
       </div>
      </div>
      <br/>
      <div class='row'>
       <div class='col-md-12'>
        <div class="table-responsive">
         <table class="table table-bordered table-striped">
          <thead>
           <tr>
            <th class="text-white bg-theme">No</th>
            <th class="text-white bg-theme">Nama UPT</th>
            <th class="text-white bg-theme">Alamat</th>
            <th class="text-white bg-theme">Action</th>
           </tr>
          </thead>
          <tbody>
 <?php if (!empty($content)) { ?>
  <?php $no = 1; ?>
  <?php foreach ($content as $value) { ?>
                                                                           <tr>
                                                                            <td><?php echo $no++ ?></td>
                                                                            <td><?php echo $value['upt'] ?></td>
                                                                            <td><?php echo $value['alamat'] ?></td>
                                                                            <td class="text-center">
                                                                             <label id="" class="label label-warning font12" 
                                                                                     onclick="ProbisUpt.ubah('<?php echo $value['id'] ?>')">Ubah</label>
                                                                             &nbsp;
                                                                             <label id="" class="label label-success font12" 
                                                                                     onclick="ProbisUpt.detail('<?php echo $value['id'] ?>')">Detail</label>
                                                                             &nbsp;
                                                                             <label id="" class="label label-danger font12" 
                                                                                     onclick="ProbisUpt.delete('<?php echo $value['id'] ?>')">Hapus</label>
                                                                             &nbsp;
                                                                            </td>
                                                                           </tr>
  <?php } ?>
 <?php } else { ?>
                                           <tr>
                                            <td class="text-center" colspan="4">Tidak Ada Data Ditemukan</td>
                                           </tr>
 <?php } ?>         
          </tbody>
         </table>
        </div>
       </div>
      </div>    
      <br/>
      <div class="row">
       <div class="col-md-12">
        <div class="pagination">
 <?php echo $pagination['links'] ?>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>-->
</div>
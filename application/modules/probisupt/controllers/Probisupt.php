<?php

class Probisupt extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'probisupt';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/probisupt.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'probis';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Probis upt";
  $data['title_content'] = 'Data Probis upt';
  $content = $this->getDataProbisupt();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataProbisupt($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.no_probis', $keyword),
       array('t.nama_probis', $keyword),
       array('u.username', $keyword),
       array('ps.status', $keyword),
       array('ut.nama_upt', $keyword),
   );
  }

  $where = "t.deleted = 0 and ut.nama_upt != 'Kantor Induk'";
  if ($keyword != '') {
   $total = Modules::run('database/count_all', array(
               'table' => $this->getTableName() . ' t',
               'field' => array('t.*', 'u.username'),
               'join' => array(
                   array('user u', 't.user = u.id'),
                   array('(select max(id) as id, probis from probis_status group by probis) pss', 'pss.probis = t.id', 'left'),
                   array('probis_status ps', 'ps.id = pss.id', 'left'),
                   array('upt ut', 'ut.id = t.upt'),
               ),
               'like' => $like,
               'is_or_like' => true,
               'inside_bracktes' => true,
               'limit' => $this->limit,
               'offset' => $this->last_no,
               'where' => $where
   ));
  } else {
   $total = Modules::run('database/count_all', array(
               'table' => $this->getTableName() . ' t',
               'field' => array('t.*', 'u.username'),
               'join' => array(
                   array('user u', 't.user = u.id'),
                   array('(select max(id) as id, probis from probis_status group by probis) pss', 'pss.probis = t.id', 'left'),
                   array('probis_status ps', 'ps.id = pss.id', 'left'),
                   array('upt ut', 'ut.id = t.upt'),
               ),
               'like' => $like,
               'limit' => $this->limit,
               'offset' => $this->last_no,
               'is_or_like' => true,
               'where' => $where
   ));
  }

  return $total;
 }

 public function getDataProbisupt($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.no_probis', $keyword),
       array('t.nama_probis', $keyword),
       array('u.username', $keyword),
       array('ps.status', $keyword),
       array('ut.nama_upt', $keyword),
   );
  }


  $where = "t.deleted = 0 and ut.nama_upt != 'Kantor Induk'";
  if ($keyword != '') {
   $data = Modules::run('database/get', array(
               'table' => $this->getTableName() . ' t',
               'field' => array('t.*', 'u.username', 'ps.status', 'ps.keterangan', 'ut.nama_upt'),
               'join' => array(
                   array('user u', 't.user = u.id'),
                   array('(select max(id) as id, probis from probis_status group by probis) pss', 'pss.probis = t.id', 'left'),
                   array('probis_status ps', 'ps.id = pss.id', 'left'),
                   array('upt ut', 'ut.id = t.upt'),
               ),
               'like' => $like,
               'is_or_like' => true,
               'limit' => $this->limit,
               'offset' => $this->last_no,
               'inside_brackets' => true,
               'where' => $where
   ));
  } else {
   $data = Modules::run('database/get', array(
               'table' => $this->getTableName() . ' t',
               'field' => array('t.*', 'u.username', 'ps.status', 'ps.keterangan', 'ut.nama_upt'),
               'join' => array(
                   array('user u', 't.user = u.id'),
                   array('(select max(id) as id, probis from probis_status group by probis) pss', 'pss.probis = t.id', 'left'),
                   array('probis_status ps', 'ps.id = pss.id', 'left'),
                   array('upt ut', 'ut.id = t.upt'),
               ),
               'like' => $like,
               'is_or_like' => true,
               'limit' => $this->limit,
               'offset' => $this->last_no,
               'where' => $where
   ));
  }

//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['status'] = $value['status'] == '' ? '-' : $value['status'];
    $value['tgl_update'] = $value['tgl_update'] == '' ? '' : date('d F Y', strtotime($value['tgl_update']));
    $edisi = '';
    if (trim($value['edisi']) < 9) {
     $edisi .= '0' . $value['edisi'];
    }
    $value['edisi'] = $edisi;
    $revisi = '';
    if (trim($value['revisi']) < 9) {
     $revisi .= '0' . $value['revisi'];
    }
    $value['revisi'] = $revisi;
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataProbisupt($keyword)
  );
 }

 public function getDetailDataProbisupt($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*',
                  'u.username', 'u.password',
                  'p.hak_akses', 'ut.nama_upt',
                  'u.priveledge', 'pp.no_probis as no_usulan', 'pp.id as id_usulan'),
              'join' => array(
                  array('user u', 't.user = u.id'),
                  array('priveledge p', 'u.priveledge = p.id'),
                  array('upt ut', 't.upt = ut.id'),
                  array('pengajuan_probis pp', 't.pengajuan_probis = pp.id', 'left'),
              ),
              'where' => "t.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function getListUpt() {
  $data = Modules::run('database/get', array(
              'table' => 'upt',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getListHakAkses() {
  $data = Modules::run('database/get', array(
              'table' => 'priveledge',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    if ($value['id'] != 1) {
     array_push($result, $value);
    }
   }
  }

  return $result;
 }

 public function getListHubunganWali() {
  $data = Modules::run('database/get', array(
              'table' => 'hubungan_wali',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getListSemester() {
  $data = Modules::run('database/get', array(
              'table' => 'semester',
              'where' => 'deleted = 0'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Probisupt";
  $data['title_content'] = 'Tambah Probisupt';
  $data['list_upt'] = $this->getListUpt();
  $data['list_hak_akses'] = $this->getListHakAkses();
  echo Modules::run('template', $data);
 }

 public function getListKeywordProbisupt($probis) {
  $data = Modules::run('database/get', array(
              'table' => 'keyword_probis kb',
              'field' => array('kb.*'),
              'where' => "kb.deleted = 0 and kb.probis = '" . $probis . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function ubah($id) {
  $data = $this->getDetailDataProbisupt($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Probisupt";
  $data['title_content'] = 'Ubah Probisupt';
  $data['list_keyword'] = $this->getListKeywordProbisupt($id);
  $data['list_upt'] = $this->getListUpt();
  echo Modules::run('template', $data);
 }

 public function getDetailDataOrganisasi($id) {
  $data = Modules::run('database/get', array(
              'table' => 'masjid_has_organisasi mho',
              'field' => array('mho.*', 'o.nama_organisasi'),
              'join' => array(
                  array('organisasi o', 'mho.organisasi = o.id')
              ),
              'where' => "mho.deleted = 0 and mho.masjid = '" . $id . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getDetailPengurusProbisupt($masjid) {
  $data = Modules::run('database/get', array(
              'table' => 'masjid_has_pengurus mhp',
              'field' => array('mhp.*', 'j.jabatan as nama_jabatan'),
              'join' => array(
                  array('jabatan j', 'mhp.jabatan = j.id')
              ),
              'where' => array('mhp.deleted' => 0, 'mhp.masjid' => $masjid)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataWaliMurid($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_wali_murid thw',
              'field' => array('thw.*', 'hw.hubungan'),
              'join' => array(
                  array('hubungan_wali hw', 'hw.id = thw.hubungan_wali')
              ),
              'where' => "thw.deleted = 0 and thw.taruna = '" . $taruna . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataAgama($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_agama ta',
              'field' => array('ta.*'),
              'where' => "ta.deleted = 0 and ta.taruna = '" . $taruna . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['status'] = $value['status'] == 1 ? 'Aktif' : 'Tidak Aktif';
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataKesehatan($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_kesehatan ta',
              'field' => array('ta.*'),
              'where' => "ta.deleted = 0 and ta.taruna = '" . $taruna . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['status'] = $value['status'] == 1 ? 'Aktif' : 'Tidak Aktif';
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataAkademik($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_akademik ta',
              'field' => array('ta.*', 's.semester as semester_taruna',
                  'p.prodi as program_studi'),
              'join' => array(
                  array('prodi p', 'ta.prodi = p.id'),
                  array('semester s', 'ta.semester = s.id'),
              ),
              'where' => "ta.deleted = 0 and ta.taruna = '" . $taruna . "'"
  ));


  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['status'] = $value['status'] == 1 ? 'Aktif' : 'Tidak Aktif';
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function detail($id) {
  $data = $this->getDetailDataProbisupt($id);
  $data['file_excel'] = $data['file_excel'];
  $data['file_excel'] = str_replace(' ', '_', $data['file_excel']);
  $data['file_excel'] = str_replace('.', '_', $data['file_excel']);
  $data['file_excel'] = str_replace('_xlsx', '.xlsx', $data['file_excel']);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Probisupt";
  $data['title_content'] = "Detail Probisupt";
  $data['list_keyword'] = $this->getListKeywordProbisupt($id);
  $this->updateLogProbisupt($id);
  echo Modules::run('template', $data);
 }

 public function updateLogProbisupt($id) {
  $post['probis'] = $id;
  $post['tanggal_akses'] = date('Y-m-d H:i:s');
  Modules::run('database/_insert', 'log_probis', $post);
 }

 public function getDetailFeedback($id) {
  $data = Modules::run('database/get', array(
              'table' => 'feedback_probis fb',
              'field' => array('fb.*', 'p.nama_pegawai'),
              'join' => array(
                  array('user u', 'fb.user = u.id'),
                  array('pegawai p', 'u.id = p.user'),
              ),
              'where' => "fb.deleted = 0 and fb.probis = '" . $id . "'",
              'orderby' => 'fb.id desc'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function feedback($id) {
  $data = $this->getDetailDataProbisupt($id);
  $data['file_excel'] = $data['file_excel'];
  $data['file_excel'] = str_replace(' ', '_', $data['file_excel']);
  $data['file_excel'] = str_replace('.', '_', $data['file_excel']);
  $data['file_excel'] = str_replace('_xlsx', '.xlsx', $data['file_excel']);
  $data['view_file'] = 'feedback_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Feedback Probisupt";
  $data['title_content'] = "Feedback Probisupt";
  $data['feedback'] = $this->getDetailFeedback($id);
  echo Modules::run('template', $data);
 }

 public function getPostDataProbisupt($value) {
  $data['no_probis'] = Modules::run('no_generator/generateNoProbisupt');
  $data['user'] = $this->session->userdata('user_id');

  if (isset($value->upt)) {
   $data['upt'] = $value->upt;
  } else {
   $data['upt'] = $this->upt;
  }
//  echo '<pre>';
//  print_r($data);die;
  $data['nama_probis'] = $value->nama;
  $data['tgl_update'] = $value->tgl_update;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);die;
//  echo '<pre>';
//  print_r($_FILES);
//  die;
  $id = $this->input->post('id');
  $is_valid = false;
  $message = "";
  $this->db->trans_begin();
  try {
   $post = $this->getPostDataProbisupt($data);

   $is_uploaded = true;
   if ($id == '') {
    if (!empty($_FILES)) {
     //xls
     if (isset($_FILES['file_xls'])) {
      $data_file = $this->uploadData("file_xls");
      $is_uploaded = $data_file['is_valid'];
      if ($is_uploaded) {
       $post['file_excel'] = $_FILES['file_xls']['name'];
      } else {
       $is_valid = false;
       $message = $data_file['response'];
      }
     }

     //pdf
     if (isset($_FILES['file_pdf'])) {
      $data_file = $this->uploadData("file_pdf");
      $is_uploaded = $data_file['is_valid'];
      if ($is_uploaded) {
       $post['file_pdf'] = $_FILES['file_pdf']['name'];
      } else {
       $is_valid = false;
       $message = $data_file['response'];
      }
     }
    }

    if ($is_uploaded) {
     $id = Modules::run('database/_insert', $this->getTableName(), $post);

     if (!empty($data->keyword)) {
      foreach ($data->keyword as $value) {
       $post_keyword['keyword'] = $value->keyword;
       $post_keyword['probis'] = $id;
       Modules::run('database/_insert', 'keyword_probis', $post_keyword);
      }
     }
    }
   } else {
    //update
    if (!empty($_FILES)) {
     //xls
     if (isset($_FILES['file_xls'])) {
      $data_file = $this->uploadData("file_xls");
      $is_uploaded = $data_file['is_valid'];
      if ($is_uploaded) {
       $post['file_excel'] = $_FILES['file_xls']['name'];
      } else {
       $is_valid = false;
       $message = $data_file['response'];
      }
     }

     //pdf
     if (isset($_FILES['file_pdf'])) {
      $data_file = $this->uploadData("file_pdf");
      $is_uploaded = $data_file['is_valid'];
      if ($is_uploaded) {
       $post['file_pdf'] = $_FILES['file_pdf']['name'];
      } else {
       $is_valid = false;
       $message = $data_file['response'];
      }
     }
    }

    if ($is_uploaded) {
     unset($post['no_probis']);
     Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));

     if (!empty($data->keyword)) {
      foreach ($data->keyword as $value) {
       $post_keyword['keyword'] = $value->keyword;
       $post_keyword['probis'] = $id;
       Modules::run('database/_insert', 'keyword_probis', $post_keyword);
      }
     }

     if (!empty($data->keyword_edit)) {
      foreach ($data->keyword_edit as $value) {
       $post_keyword['keyword'] = $value->keyword;
       if ($value->is_edit == 0) {
        $post_keyword['deleted'] = 1;
       }
       Modules::run('database/_update', 'keyword_probis', $post_keyword, array('id' => $value->id));
      }
     }
    }
   }
   $this->db->trans_commit();
   if ($is_uploaded) {
    $is_valid = true;
   }
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id, 'message' => $message));
 }

 public function sendFeedback($id) {
  $is_valid = false;
  $this->db->trans_begin();
//  echo '<pre>';
//  print_r($_POST);die;
  try {
   $post['probis'] = $id;
   $post['pesan'] = $this->input->post('pesan');
   $post['user'] = $this->session->userdata('user_id');
   Modules::run('database/_insert', 'feedback_probis', $post);
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }
  echo json_encode(array('is_valid' => $is_valid));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Probisupt";
  $data['title_content'] = 'Data Probisupt';
  $content = $this->getDataProbisupt($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function deleteFeedback($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', 'feedback_probis', array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/probis/';
  $config['allowed_types'] = 'pdf|xls';
  $config['max_size'] = '1000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);
  // $this->upload->do_upload($name_of_field);


  $is_valid = false;
  if (!$this->upload->do_upload($name_of_field)) {
   $response = $this->upload->display_errors();
  } else {
   $response = $this->upload->data();
   $is_valid = true;
  }

  return array(
      'is_valid' => $is_valid,
      'response' => $response
  );
 }

 public function showLogo() {
  $foto = str_replace(' ', '_', $this->input->post('file'));
  $foto = str_replace('.', '_', $foto);
  $foto = str_replace('_pdf', '.pdf', $foto);
  $foto = str_replace('__', '_', $foto);
  $data['file'] = $foto;
  echo $this->load->view('file', $data, true);
 }

 public function download($id) {
  $this->load->helper("download");
  $probis = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' js',
              'where' => "js.id = " . $id
          ))->row_array();

  $file = $probis['file_excel'];
  $data = file_get_contents('files/berkas/probis/' . $file);
  $name = $file;
  force_download($name, $data);
 }

 public function detailUsulan($id) {
  $data = Modules::run('review_usulan_probis/getDetailDataPengajuan', $id);
  $data['view_file'] = 'detail_usulan_probis';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Pengajuan";
  $data['title_content'] = "Detail Pengajuan";
  $data['list_status'] = Modules::run('review_usulan_probis/getListStatus', $id);
  $data['tembusan'] = Modules::run('review_usulan_probis/getDataTembusan', $id);
  echo Modules::run('template', $data);
 }

 public function getStatus() {
  $id = $this->input->post('id');
  $data['id'] = $id;
  echo $this->load->view('status_probis', $data, true);
 }

 public function updateStatus() {
  $id = $this->input->post('id');
  $status = $this->input->post('status');
  $keterangan = $this->input->post('keterangan');
  $is_valid = false;
  $this->db->trans_begin();
  try {
   $insert['probis'] = $id;
   $insert['status'] = $status;
   if ($keterangan != '') {
    $insert['keterangan'] = $keterangan;
   }

   Modules::run('database/_insert', 'probis_status', $insert);
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}

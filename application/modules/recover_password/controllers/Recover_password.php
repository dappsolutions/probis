<?php

class Recover_password extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 function getModuleName() {
  return 'recover_password';
 }

 function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/recover_password.js"></script>',
  );

  return $data;
 }

 function getTableName() {
  return 'forgot_password';
 }

 function index() {
  $data['view_file'] = 'index';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Recover Password";
  $data['title_content'] = 'Recover Password';
  echo $this->load->view('index', $data, true);
 }

 function getDataUser($email) {
  $data = Modules::run('database/get', array(
              'table' => 'user u',
              'field' => array('u.*'),
              'join' => array(
                  array('pegawai p', 'u.id = p.user')
              ),
              'where' => "u.deleted = 0 and p.email = '" . $email . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 function recover() {
  $email = $this->input->post('email');

  $is_valid = false;
  $message = "Email tidak dipakai di pengguna manapun";
  try {
   $data = $this->getDataUser($email);
   if (!empty($data)) {
    $is_valid = true;

    $post['email'] = $email;
    Modules::run('database/_insert', $this->getTableName(), $post);
   }
  } catch (Exception $exc) {
   $is_valid = false;
  }

  $hak_akses = '';
  if ($is_valid) {
   $message = '';
  }

  echo json_encode(array('is_valid' => $is_valid,
      'message' => $message));
 }

 function lupa_password() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_lupa_password';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Lupa Password";
  $data['title_content'] = 'Data Lupa Password';
  $content = $this->getDataRecover();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/lupa_password/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataRecover($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.email', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*'),
              'where' => "t.deleted = 0"
  ));

  return $total;
 }

 public function getDataRecover($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.email', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*'),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "t.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataRecover($keyword)
  );
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_lupa_password';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Lupa Password";
  $data['title_content'] = 'Data Lupa Password';
  $content = $this->getDataRecover($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/search/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function check($id) {
  $data = Modules::run('database/get', array(
              'table' => 'user u',
              'field' => array('u.*'),
              'join' => array(
                  array('pegawai p', 'u.id = p.user'),
                  array('forgot_password fp', 'p.email = fp.email'),
              ),
              'where' => "u.deleted = 0 and fp.id = '" . $id . "'"
  ));

  $password = "Password Tidak Ditemukan";
  if (!empty($data)) {
   $password = 'Password ditemukan <b>' . $data->row_array()['password'] . '</b> Kirim Password ini, Lewat email yang diajukan';
  }

  echo $password;
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}

<!DOCTYPE html>
<html lang="en">

 <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="keywords" content="">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url() ?>assets/css/images/favicon.png">
  <title>Login</title>
  <!-- ===== Bootstrap CSS ===== -->
  <link href="<?php echo base_url() ?>assets/css/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <!-- ===== Plugin CSS ===== -->
  <!-- ===== Animation CSS ===== -->
  <link href="<?php echo base_url() ?>assets/css/animate.css" rel="stylesheet">
  <!-- ===== Custom CSS ===== -->
  <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
  <!-- ===== Color CSS ===== -->
  <link href="<?php echo base_url() ?>assets/css/colors/default.css" id="theme" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/toastr.min.css">  
 </head>

 <body class="mini-sidebar">
  <!-- Preloader -->
  <div class="preloader">
   <div class="cssload-speeding-wheel"></div>
  </div>
  <section id="wrapper" class="login-register">
   <div class="login-box">
    <div class="white-box">
     <!--<center><img src="<?php echo base_url() . 'assets/images/logo_mobile.jpg' ?>" height="100" width="100"/></center>-->
     <center><h4 class="text-warning">RECOVER PASSWORD</h4></center>
     <hr/>
     <form class="form-horizontal form-material" id="loginform" action="<?php echo base_url() ?>">
      <h3 class="box-title m-b-20">Lupa Password</h3>
      <div class="form-group">
       <div class="col-xs-12">
        <input class="form-control" type="email" required="" placeholder="Email" id='email'>
       </div>
      </div>
      <div class="form-group text-center m-t-20">
       <div class="col-xs-12">
        <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit" onclick="RecoverPassword.recover(this, event)">Proses</button>
       </div>
      </div>
     </form>
    </div>
   </div>
  </section>
  <!-- jQuery -->
  <script src="<?php echo base_url() ?>assets/plugins/components/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap Core JavaScript -->
  <script src="<?php echo base_url() ?>assets/css/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- Menu Plugin JavaScript -->
  <script src="<?php echo base_url() ?>assets/js/sidebarmenu.js"></script>
  <!--slimscroll JavaScript -->
  <script src="<?php echo base_url() ?>assets/js/jquery.slimscroll.js"></script>
  <!--Wave Effects -->
  <script src="<?php echo base_url() ?>assets/js/waves.js"></script>
  <!-- Custom Theme JavaScript -->
  <script src="<?php echo base_url() ?>assets/js/custom.js"></script>
  <!--Style Switcher -->
  <script src="<?php echo base_url() ?>assets/plugins/components/styleswitcher/jQuery.style.switcher.js"></script>


  <script src="<?php echo base_url() ?>assets/js/toastr.min.js"></script>
  <script src="<?php echo base_url() ?>assets/js/url.js"></script>
  <script src="<?php echo base_url() ?>assets/js/message.js"></script>
  <script src="<?php echo base_url() ?>assets/js/validation.js"></script>
  <script src="<?php echo base_url() ?>assets/js/controllers/recover_password.js"></script>
 </body>

</html>

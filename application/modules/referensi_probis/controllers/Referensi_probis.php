<?php

class Referensi_probis extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $upt;
 public $akses;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
  $this->upt = $this->session->userdata('upt_id');
  $this->akses = $this->session->userdata('hak_akses');
 }

 public function getModuleName() {
  return 'referensi_probis';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/referensi_probis.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'referensi_probis';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Referensi Probis";
  $data['title_content'] = 'Data Referensi Probis';
  $content = $this->getDataProbis();
//  echo '<pre>';
//  print_r($content);die;
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataProbis($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.no_probis', $keyword),
       array('t.nama_probis', $keyword),
       array('u.username', $keyword),
       array('ps.status', $keyword),
   );
  }

  $where = 't.deleted = 0 and pp.child_first = 0';
  if ($this->upt != '') {
   $where = "t.deleted = 0 and t.upt = '" . $this->upt . "' and pp.child_first = 0";
  }
  $total = Modules::run('database/count_all', array(
              'table' => 'probis t',
              'field' => array('t.*', 'u.username', 'pp.parent', 'pp.child_first',
                  'pp.child_second', 'pp.child_third'),
              'join' => array(
                  array('user u', 't.user = u.id'),
                  array('(select max(id) as id, probis from probis_status group by probis) pss', 'pss.probis = t.id', 'left'),
                  array('probis_status ps', 'ps.id = pss.id', 'left'),
                  array('path_probis pp', 'pp.probis = t.id', 'left')
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => $where,
              'orderby' => 'pp.parent, pp.child_first, pp.child_second'
  ));

  return $total;
 }

 public function getDataProbis($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.no_probis', $keyword),
       array('t.nama_probis', $keyword),
       array('u.username', $keyword),
       array('ps.status', $keyword),
   );
  }


  $where = 't.deleted = 0 and pp.child_first = 0';
  if ($this->upt != '') {
   $where = "t.deleted = 0 and t.upt = '" . $this->upt . "' and pp.child_first = 0";
  }
  $data = Modules::run('database/get', array(
              'table' => 'probis t',
              'field' => array('t.*', 'u.username', 'ps.status', 'ps.keterangan',
                  'pp.parent', 'pp.child_first',
                  'pp.child_second', 'pp.child_third'),
              'join' => array(
                  array('user u', 't.user = u.id'),
                  array('(select max(id) as id, probis from probis_status group by probis) pss', 'pss.probis = t.id', 'left'),
                  array('probis_status ps', 'ps.id = pss.id', 'left'),
                  array('path_probis pp', 'pp.probis = t.id', 'left')
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => $where,
              'orderby' => 'pp.parent, pp.child_first, pp.child_second'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['status'] = $value['status'] == '' ? '-' : $value['status'];
    $value['tgl_update'] = $value['tgl_update'] == '' ? '' : date('d F Y', strtotime($value['tgl_update']));
    $edisi = '';
    if (trim($value['edisi']) < 9) {
     $edisi .= '0' . $value['edisi'];
    }
    $value['edisi'] = $edisi;
    $revisi = '';
    if (trim($value['revisi']) < 9) {
     $revisi .= '0' . $value['revisi'];
    }
    $value['revisi'] = $revisi;
    if ($value['parent'] != '') {
     $value['no_probis'] = $value['parent'] . '.' . $value['child_first'] .
             '.' . $value['child_second'] . '.' . $value['child_third'];
    }
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataProbis($keyword)
  );
 }

 public function getDetailDataProbis($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*', 'p.no_probis', 'p.nama_probis', 'p.file_pdf'),
              'join' => array(
                  array('probis p', 't.probis = p.id')
              ),
              'where' => "t.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }
 
 public function getDetailDataProbisData($id) {
  $data = Modules::run('database/get', array(
              'table' => 'probis t',
              'field' => array('t.*'),
              'where' => "t.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function getDetailDataDokumen($table, $id) {
  $data = Modules::run('database/get', array(
              'table' => $table . ' t',
              'field' => array('t.*', 'p.no_probis', 'p.nama_probis'),
              'join' => array(
                  array('probis p', 't.probis = p.id')
              ),
              'where' => "t.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function getListUpt() {
  $data = Modules::run('database/get', array(
              'table' => 'upt',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListProbis() {
  $where = "upt = '" . $this->upt . "' and deleted = 0";
  if ($this->akses == 'Superadmin') {
   $where = "deleted = 0";
  }
  $data = Modules::run('database/get', array(
              'table' => 'probis',
              'where' => $where
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getListHubunganWali() {
  $data = Modules::run('database/get', array(
              'table' => 'hubungan_wali',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getListSemester() {
  $data = Modules::run('database/get', array(
              'table' => 'semester',
              'where' => 'deleted = 0'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function add($id) {
//  echo $id;die;
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Referensi Probis";
  $data['title_content'] = 'Tambah Referensi Probis';
  $data['list_probis'] = $this->getListProbis();
  $data['probis'] = $id;
//  echo '<pre>';
//  print_r($data);die;
  echo Modules::run('template', $data);
 }

 public function addInput($id) {
//  echo $id;die;
  $data['view_file'] = 'form_add_input';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Dokumen Input";
  $data['title_content'] = 'Tambah Dokumen Input';
  $data['list_probis'] = $this->getListProbis();
  $data['probis'] = $id;
//  echo '<pre>';
//  print_r($data);die;
  echo Modules::run('template', $data);
 }

 public function addOutput($id) {
//  echo $id;die;
  $data['view_file'] = 'form_add_output';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Dokumen Output";
  $data['title_content'] = 'Tambah Dokumen Output';
  $data['list_probis'] = $this->getListProbis();
  $data['probis'] = $id;
//  echo '<pre>';
//  print_r($data);die;
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataProbis($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Probis";
  $data['title_content'] = 'Ubah Probis';
  $data['list_probis'] = $this->getListProbis();
  echo Modules::run('template', $data);
 }

 public function ubahDokumen($id, $jenis) {
  $table = $jenis == 'input' ? 'dokumen_input' : 'dokumen_output';
  $data = $this->getDetailDataDokumen($table, $id);
  $data['view_file'] = $jenis == 'input' ? 'form_add_input' : 'form_add_output';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Dokumen";
  $data['title_content'] = 'Ubah Dokumen';
  $data['list_probis'] = $this->getListProbis();
  $data['jenis'] = $jenis;
  echo Modules::run('template', $data);
 }

 public function getDetailDataOrganisasi($id) {
  $data = Modules::run('database/get', array(
              'table' => 'masjid_has_organisasi mho',
              'field' => array('mho.*', 'o.nama_organisasi'),
              'join' => array(
                  array('organisasi o', 'mho.organisasi = o.id')
              ),
              'where' => "mho.deleted = 0 and mho.masjid = '" . $id . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getDetailPengurusProbis($masjid) {
  $data = Modules::run('database/get', array(
              'table' => 'masjid_has_pengurus mhp',
              'field' => array('mhp.*', 'j.jabatan as nama_jabatan'),
              'join' => array(
                  array('jabatan j', 'mhp.jabatan = j.id')
              ),
              'where' => array('mhp.deleted' => 0, 'mhp.masjid' => $masjid)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataWaliMurid($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_wali_murid thw',
              'field' => array('thw.*', 'hw.hubungan'),
              'join' => array(
                  array('hubungan_wali hw', 'hw.id = thw.hubungan_wali')
              ),
              'where' => "thw.deleted = 0 and thw.taruna = '" . $taruna . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataAgama($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_agama ta',
              'field' => array('ta.*'),
              'where' => "ta.deleted = 0 and ta.taruna = '" . $taruna . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['status'] = $value['status'] == 1 ? 'Aktif' : 'Tidak Aktif';
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataKesehatan($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_kesehatan ta',
              'field' => array('ta.*'),
              'where' => "ta.deleted = 0 and ta.taruna = '" . $taruna . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['status'] = $value['status'] == 1 ? 'Aktif' : 'Tidak Aktif';
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataAkademik($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_akademik ta',
              'field' => array('ta.*', 's.semester as semester_taruna',
                  'p.prodi as program_studi'),
              'join' => array(
                  array('prodi p', 'ta.prodi = p.id'),
                  array('semester s', 'ta.semester = s.id'),
              ),
              'where' => "ta.deleted = 0 and ta.taruna = '" . $taruna . "'"
  ));


  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['status'] = $value['status'] == 1 ? 'Aktif' : 'Tidak Aktif';
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function detail($id) {
  $data = $this->getDetailDataProbis($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Probis";
  $data['title_content'] = "Detail Probis";
  echo Modules::run('template', $data);
 }

 public function detailInput($id) {
  $data = $this->getDetailDataDokumen('dokumen_input', $id);
  $data['view_file'] = 'detail_dokumen';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Dokumen Input";
  $data['title_content'] = "Detail Dokumen Input";
  $data['jenis'] = "input";
  echo Modules::run('template', $data);
 }

 public function detailOutput($id) {
  $data = $this->getDetailDataDokumen('dokumen_output', $id);
  $data['view_file'] = 'detail_dokumen';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Dokumen Output";
  $data['title_content'] = "Detail Dokumen Output";
  $data['jenis'] = "output";
  echo Modules::run('template', $data);
 }

 public function getPostDataProbis($value) {
  $data['probis'] = $value->probis;
  $data['upt'] = $value->upt;
  $data['nama_referensi'] = $value->nama;
  return $data;
 }

 public function getPostDataDokumen($value) {
  $data['probis'] = $value->probis;
  $data['upt'] = $value->upt;
  $data['nama_dokumen'] = $value->nama;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);die;

  $id = $this->input->post('id');
  $is_valid = false;
  $message = "";
  $this->db->trans_begin();
  try {
   $post = $this->getPostDataProbis($data);

   $is_uploaded = true;
   if ($id == '') {
    if (!empty($_FILES)) {
     if (isset($_FILES['file'])) {
      $data_file = $this->uploadData("file");
      $is_uploaded = $data_file['is_valid'];
      if ($is_uploaded) {
       $post['file'] = $_FILES['file']['name'];
      } else {
       $is_valid = false;
       $message = $data_file['response'];
      }
     }
    }

    if ($is_uploaded) {
     $id = Modules::run('database/_insert', $this->getTableName(), $post);
    }
   } else {
    //update
    if (!empty($_FILES)) {
     if (isset($_FILES['file'])) {
      $data_file = $this->uploadData("file");
      $is_uploaded = $data_file['is_valid'];
      if ($is_uploaded) {
       $post['file'] = $_FILES['file']['name'];
      } else {
       $is_valid = false;
       $message = $data_file['response'];
      }
     }
    }

    if ($is_uploaded) {
     Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
    }
   }
   $this->db->trans_commit();
   if ($is_uploaded) {
    $is_valid = true;
   }
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id, 'message' => $message));
 }

 public function simpanInput() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);die;

  $id = $this->input->post('id');
  $is_valid = false;
  $message = "";
  $this->db->trans_begin();
  try {
   $post = $this->getPostDataDokumen($data);

   $is_uploaded = true;
   if ($id == '') {
    if (!empty($_FILES)) {
     if (isset($_FILES['file'])) {
      $data_file = $this->uploadData("file", 'dokumen_input');
      $is_uploaded = $data_file['is_valid'];
      if ($is_uploaded) {
       $post['file'] = $_FILES['file']['name'];
      } else {
       $is_valid = false;
       $message = $data_file['response'];
      }
     }
    }

    if ($is_uploaded) {
     $id = Modules::run('database/_insert', 'dokumen_input', $post);
    }
   } else {
    //update
    if (!empty($_FILES)) {
     if (isset($_FILES['file'])) {
      $data_file = $this->uploadData("file", 'dokumen_input');
      $is_uploaded = $data_file['is_valid'];
      if ($is_uploaded) {
       $post['file'] = $_FILES['file']['name'];
      } else {
       $is_valid = false;
       $message = $data_file['response'];
      }
     }
    }

    if ($is_uploaded) {
     Modules::run('database/_update', 'dokumen_input', $post, array('id' => $id));
    }
   }
   $this->db->trans_commit();
   if ($is_uploaded) {
    $is_valid = true;
   }
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id, 'message' => $message));
 }

 public function simpanOutput() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);die;

  $id = $this->input->post('id');
  $is_valid = false;
  $message = "";
  $this->db->trans_begin();
  try {
   $post = $this->getPostDataDokumen($data);

   $is_uploaded = true;
   if ($id == '') {
    if (!empty($_FILES)) {
     if (isset($_FILES['file'])) {
      $data_file = $this->uploadData("file", 'dokumen_output');
      $is_uploaded = $data_file['is_valid'];
      if ($is_uploaded) {
       $post['file'] = $_FILES['file']['name'];
      } else {
       $is_valid = false;
       $message = $data_file['response'];
      }
     }
    }

    if ($is_uploaded) {
     $id = Modules::run('database/_insert', 'dokumen_output', $post);
    }
   } else {
    //update
    if (!empty($_FILES)) {
     if (isset($_FILES['file'])) {
      $data_file = $this->uploadData("file", 'dokumen_output');
      $is_uploaded = $data_file['is_valid'];
      if ($is_uploaded) {
       $post['file'] = $_FILES['file']['name'];
      } else {
       $is_valid = false;
       $message = $data_file['response'];
      }
     }
    }

    if ($is_uploaded) {
     Modules::run('database/_update', 'dokumen_output', $post, array('id' => $id));
    }
   }
   $this->db->trans_commit();
   if ($is_uploaded) {
    $is_valid = true;
   }
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id, 'message' => $message));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Probis";
  $data['title_content'] = 'Data Probis';
  $content = $this->getDataProbis($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function deleteDokumen($id) {
  $is_valid = false;
  $table = $_POST['table'];
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $table, array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function uploadData($name_of_field, $berkas = '') {
  $dir = 'files/berkas/referensi/';
  if ($berkas != '') {
   $dir = 'files/berkas/' . $berkas . '/';
  }
  $config['upload_path'] = $dir;
  $config['allowed_types'] = 'pdf';
  $config['max_size'] = '13000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);
  // $this->upload->do_upload($name_of_field);


  $is_valid = false;
  if (!$this->upload->do_upload($name_of_field)) {
   $response = $this->upload->display_errors();
  } else {
   $response = $this->upload->data();
   $is_valid = true;
  }

  return array(
      'is_valid' => $is_valid,
      'response' => $response
  );
 }

 public function showLogo($id = "") {
  $data = $this->getDetailDataProbis($id);
//   echo '<pre>';
//   print_r($data);die;
  $foto = str_replace(' ', '_', $data['file']);
  $data_foto = explode('.', $foto);
  $tipe_file = $data_foto[count($data_foto) - 1];

  if (!empty($data_foto)) {
   $foto = implode('_', $data_foto);
   $foto = str_replace("_" . $tipe_file, '.' . $tipe_file, $foto);
  }

  $foto = str_replace('__', '_', $foto);

  $data['file_fix'] = $foto;

  $data['view_file'] = 'file';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Referensi";
  $data['title_content'] = "Detail Referensi";
  echo $this->load->view('file', $data, true);
 }
 
 public function showFileReferensi($id = "") {
  $data = $this->getDetailDataProbis($id);
//   echo '<pre>';
//   print_r($data);die;
  $foto = str_replace(' ', '_', $data['file_pdf']);
  $data_foto = explode('.', $foto);
  $tipe_file = $data_foto[count($data_foto) - 1];

  if (!empty($data_foto)) {
   $foto = implode('_', $data_foto);
   $foto = str_replace("_" . $tipe_file, '.' . $tipe_file, $foto);
  }

  $foto = str_replace('__', '_', $foto);

  $data['file_fix'] = $foto;

  $data['view_file'] = 'file';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Referensi";
  $data['title_content'] = "Detail Referensi";
  echo $this->load->view('file', $data, true);
 }
 
 public function showLogoProbis($id = "") {
  $data = $this->getDetailDataProbisData($id);
//   echo '<pre>';
//   print_r($data);die;
  $foto = str_replace(' ', '_', $data['file_pdf']);
  $data_foto = explode('.', $foto);
  $tipe_file = $data_foto[count($data_foto) - 1];

  if (!empty($data_foto)) {
   $foto = implode('_', $data_foto);
   $foto = str_replace("_" . $tipe_file, '.' . $tipe_file, $foto);
  }

  $foto = str_replace('__', '_', $foto);

  $data['file_fix'] = $foto;

  $data['view_file'] = 'file_probis';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Referensi";
  $data['title_content'] = "Detail Referensi";
  echo $this->load->view('file_probis', $data, true);
 }

 public function showDokumen($id = "", $jenis) {
  $table = $jenis == 'input' ? 'dokumen_input' : 'dokumen_output';
  $data = $this->getDetailDataDokumen($table, $id);
  $foto = str_replace(' ', '_', $data['file']);
  $data_foto = explode('.', $foto);
  $tipe_file = $data_foto[count($data_foto) - 1];

  if (!empty($data_foto)) {
   $foto = implode('_', $data_foto);
   $foto = str_replace("_" . $tipe_file, '.' . $tipe_file, $foto);
  }

  $foto = str_replace('__', '_', $foto);

  $data['file_fix'] = $foto;

  $data['view_file'] = 'file';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Dokumen";
  $data['title_content'] = "Detail Dokumen";
  $data['dir'] = $table;
  echo $this->load->view('file_dokumen', $data, true);
 }

// public function showLogo() {
////  $foto = str_replace(' ', '_', $this->input->post('file'));
//
//  $foto = str_replace(' ', '_', $this->input->post('file'));
//  $data_foto = explode('.', $foto);
//  $tipe_file = $data_foto[count($data_foto) - 1];
////  echo '<pre>';
////  print_r($data_foto);die;
//  if (!empty($data_foto)) {
////   echo $tipe_file;die;
//   $foto = implode('_', $data_foto);
//   $foto = str_replace("_" . $tipe_file, '.' . $tipe_file, $foto);
//  }
//
////  echo $foto;die;
////  echo $foto;die;
//  $data['file'] = $foto;
//  echo $this->load->view('file', $data, true);
// }

 public function daftarReferensi() {
  $probis = $_POST['probis'];
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' rp',
              'field' => array('rp.*'),
              'where' => "rp.deleted = 0 and rp.probis = '" . $probis . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  $content['content'] = $result;
  echo $this->load->view('table_referensi', $content, true);
 }

 public function daftarInput() {
  $probis = $_POST['probis'];
  $data = Modules::run('database/get', array(
              'table' => 'dokumen_input rp',
              'field' => array('rp.*'),
              'where' => "rp.deleted = 0 and rp.probis = '" . $probis . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  $content['content'] = $result;
  echo $this->load->view('table_input', $content, true);
 }
 
 public function daftarOutput() {
  $probis = $_POST['probis'];
  $data = Modules::run('database/get', array(
              'table' => 'dokumen_output rp',
              'field' => array('rp.*'),
              'where' => "rp.deleted = 0 and rp.probis = '" . $probis . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  $content['content'] = $result;
  echo $this->load->view('table_output', $content, true);
 }

 public function getDataProbisChild() {

  $parent = $_POST['parent'];
  $level = $_POST['level'];

  if ($level == '1') {
   $where = "t.deleted = 0 and pp.parent = '" . $parent . "' and pp.child_first != 0 and pp.child_second = 0";
  }
  if ($level == '2') {
   $child_first = $_POST['child_first'];
   $where = "t.deleted = 0 and pp.parent = '" . $parent . "' and pp.child_first = '" . $child_first . "' and pp.child_second != 0";
  }

  $data = Modules::run('database/get', array(
              'table' => 'probis t',
              'field' => array(
                  't.*', 'u.username', 'ps.status', 'ps.keterangan',
                  'pp.parent', 'pp.child_first',
                  'pp.child_second', 'pp.child_third'
              ),
              'join' => array(
                  array('user u', 't.user = u.id'),
                  array('(select max(id) as id, probis from probis_status group by probis) pss', 'pss.probis = t.id', 'left'),
                  array('probis_status ps', 'ps.id = pss.id', 'left'),
                  array('path_probis pp', 'pp.probis = t.id', 'left')
              ),
              'where' => $where,
              'orderby' => 'pp.parent, pp.child_first, pp.child_second, pp.child_third'
  ));

  // echo '<pre>';
  // echo $this->db->last_query();die;
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['status'] = $value['status'] == '' ? '-' : $value['status'];
    $value['tgl_update'] = $value['tgl_update'] == '' ? '' : date('d F Y', strtotime($value['tgl_update']));
    $edisi = '';
    if (trim($value['edisi']) < 9) {
     $edisi .= '0' . $value['edisi'];
    }
    $value['edisi'] = $edisi;
    $revisi = '';
    if (trim($value['revisi']) < 9) {
     $revisi .= '0' . $value['revisi'];
    }
    $value['revisi'] = $revisi;
    if ($value['parent'] != '') {
     $value['no_probis'] = $value['parent'] . '.' . $value['child_first'] .
             '.' . $value['child_second'] . '.' . $value['child_third'];
    }
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getDetailChildProbis() {
  $data = $this->getDataProbisChild();
  $content['content'] = $data;
  //  echo '<pre>';
  //  print_r($data);die;
  $content['module'] = $this->getModuleName();
  if ($_POST['level'] == '1') {
   echo $this->load->view('table_child_fist', $content, true);
  }

  if ($_POST['level'] == '2') {
   echo $this->load->view('table_child_second', $content, true);
  }
 }

}

<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <!--.row-->
 <div class="row">
  <div class="col-md-12">
   <div class="panel panel-primary">
    <div class="panel-heading"> <?php echo $title_content ?></div>
    <div class="panel-wrapper collapse in" aria-expanded="true">
     <div class="panel-body">
      <form action="#" class="form-horizontal">
       <div class="form-body">        
        <hr class="m-t-0 m-b-40">
        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Probis
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $no_probis . ' - ' . $nama_probis ?>
           </div>
          </div>         
         </div>         
        </div>
        <br/>

        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Nama Dokumen
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $nama_dokumen ?>
           </div>
          </div>         
         </div>         
        </div>
        <br/>

        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            File Referensi (.pdf)
           </div>
           <div class="col-md-9 text-primary text-left">
            <button jenis="<?php echo $jenis ?>" onclick="ReferensiProbis.showDokumen(this, event)"><?php echo $file ?></button>
           </div>
          </div>
         </div>
        </div>
       </div>


       <div class="form-actions">
        <div class="row">
         <div class="col-md-12">
          <div class="row">
           <div class="col-md-offset-3 col-md-9 text-right">
            <button type="button" class="btn btn-default" onclick="ReferensiProbis.back()">Kembali</button>
           </div>
          </div>
         </div>
         <div class="col-md-6"> </div>
        </div>
       </div>
      </form>
     </div>
    </div>
   </div>
  </div>
 </div>
 <!--./row--> 
</div>

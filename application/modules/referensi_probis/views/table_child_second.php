<div class='row'>
 <div class='col-md-12'>
  <div class="table-responsive">
   <table class="table color-bordered-table danger-bordered-table">
    <thead>
     <tr class="">
      <th class="font-12">No</th>
      <th class="font-12">No. Probis</th>
      <th class="font-12">Nama Probis</th>
      <th class="font-12">Tahun</th>
      <th class="font-12">Tanggal Update</th>
      <th class="font-12">Pembuat</th>
      <th class="font-12">Edisi</th>
      <th class="font-12">Revisi</th>
      <th class="font-12">Status</th>
      <th class="font-12">Keterangan</th>        
      <th class="text-center font-12">Action</th>
     </tr>
    </thead>
    <tbody>
     <?php if (!empty($content)) { ?>
      <?php $no = 1; ?>
      <?php foreach ($content as $value) { ?>
       <tr>
        <td class='font-12'><?php echo $no++ ?></td>
        <td class='font-12'><?php echo $value['no_probis'] ?></td>
        <td class='font-12'><?php echo $value['nama_probis'] ?></td>
        <td class='font-12'><?php echo $value['tahun'] ?></td>
        <td class='font-12'><?php echo $value['tgl_update'] ?></td>
        <td class='font-12'><?php echo $value['username'] ?></td>
        <td class='font-12'><?php echo $value['edisi'] ?></td>
        <td class='font-12'><?php echo $value['revisi'] ?></td>
        <?php $color = ""; ?>
        <?php if ($value['status'] == 'PROGRESS') { ?>
         <?php $color = 'label-warning'; ?>
        <?php } ?>
        <?php if ($value['status'] == 'ONCOMING') { ?>
         <?php $color = 'label-primary'; ?>
        <?php } ?>
        <?php if ($value['status'] == 'DONE') { ?>
         <?php $color = 'label-success'; ?>
        <?php } ?>
        <?php if ($value['status'] == 'PROSES REVISI') { ?>
         <?php $color = 'label-info'; ?>
        <?php } ?>
        <td class='font-12'><label class="label <?php echo $color; ?>"><?php echo $value['status'] ?></label></td>
        <td class='font-12'><?php echo $value['keterangan'] ?></td>
        <td class="text-center">
         <?php if ($this->session->userdata('hak_akses') == 'Admin' || $this->session->userdata('hak_akses') == 'Superadmin') { ?>
          <!--          <label id="" class="label label-success font-10" 
                           onclick="ReferensiProbis.detailProbis('<?php echo $value['id'] ?>')">Detail</label>
                    &nbsp;-->
          <label id="" class="label label-warning font-10" 
                 onclick="ReferensiProbis.add('<?php echo $value['id'] ?>')">Tambah Referensi</label>
          &nbsp;
          <label id="" class="label label-warning font-10" 
                 onclick="ReferensiProbis.addInput('<?php echo $value['id'] ?>')">Tambah Dokumen Input</label>
          &nbsp;
          <label id="" class="label label-warning font-10" 
                 onclick="ReferensiProbis.addOutput('<?php echo $value['id'] ?>')">Tambah Dokumen Output</label>
          &nbsp;
          <label id="" class="label label-primary font-10" 
                 onclick="ReferensiProbis.daftarReferensi('<?php echo $value['id'] ?>')">Daftar Referensi</label>
          &nbsp;
          <label id="" class="label label-primary font-10" 
                 onclick="ReferensiProbis.daftarInput('<?php echo $value['id'] ?>')">Daftar Dokumen Input</label>
          &nbsp;             
          <label id="" class="label label-primary font-10" 
                 onclick="ReferensiProbis.daftarOutput('<?php echo $value['id'] ?>')">Daftar Dokumen Output</label>
          &nbsp;      
         <?php } else { ?>
          <label id="" class="label label-primary font-10" 
                 onclick="ReferensiProbis.daftarReferensi('<?php echo $value['id'] ?>')">Daftar Referensi</label>
          &nbsp;
          <label id="" class="label label-primary font-10" 
                 onclick="ReferensiProbis.daftarInput('<?php echo $value['id'] ?>')">Daftar Dokumen Input</label>
          &nbsp;             
          <label id="" class="label label-primary font-10" 
                 onclick="ReferensiProbis.daftarOutput('<?php echo $value['id'] ?>')">Daftar Dokumen Output</label>
          &nbsp;      
          <?php if ($value['file_pdf'] != '') { ?>
           <label id="" class="label label-danger font-10 hover" 
                  onclick="ReferensiProbis.previewFile('<?php echo $value['id'] ?>')">Preview (.pdf)</label>
           &nbsp;
          <?php } ?>
         <?php } ?>  
        </td>
       </tr>
      <?php } ?>
     <?php } else { ?>
      <tr>
       <td class="text-center font-12" colspan="12">Tidak Ada Data Ditemukan</td>
      </tr>
     <?php } ?>         
    </tbody>
   </table>
  </div>
 </div>
</div>    

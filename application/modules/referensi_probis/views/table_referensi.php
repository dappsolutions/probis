<div class="row">
 <div class="col-md-12">
  <div class="table-responsive">
   <table class="table color-bordered-table primary-bordered-table">
    <thead>
     <tr class="">
      <th class="font-12">No</th>
      <th class="font-12">Nama Referensi</th>
      <th class="text-center font-12">Action</th>
     </tr>
    </thead>
    <tbody>
     <?php if (!empty($content)) { ?>
      <?php $no = 1; ?>
      <?php foreach ($content as $value) { ?>
       <tr>
        <td class='font-12'><?php echo $no++ ?></td>
        <td class='font-12'><?php echo $value['nama_referensi'] ?></td>
        <td class="text-center">
         <?php
         if ($this->session->userdata('hak_akses') == 'Admin' ||
                 $this->session->userdata('hak_akses') == 'Superadmin') {
          ?>
          <label id="" class="label label-warning font-10" 
                 onclick="ReferensiProbis.ubah('<?php echo $value['id'] ?>')">Ubah</label>
          &nbsp;
         <?php } ?>
         <label id="" class="label label-success font-10" 
                onclick="ReferensiProbis.showFileReferensi('<?php echo $value['id'] ?>')">Tampilkan</label>
         &nbsp;
         <?php
         if ($this->session->userdata('hak_akses') == 'Admin' ||
                 $this->session->userdata('hak_akses') == 'Superadmin') {
          ?>
          <label id="" class="label label-danger font-10 hover" 
                 onclick="ReferensiProbis.delete('<?php echo $value['id'] ?>')">Hapus</label>
          &nbsp;
         <?php } ?>
        </td>
       </tr>
      <?php } ?>
     <?php } else { ?>
      <tr>
       <td class="text-center font-12" colspan="8">Tidak Ada Data Ditemukan</td>
      </tr>
     <?php } ?>         
    </tbody>
   </table>
  </div>
 </div>
</div>

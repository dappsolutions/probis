<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <!--.row-->
 <div class="row">
  <div class="col-md-12">
   <div class="panel panel-primary">
    <div class="panel-heading"> <?php echo $title_content ?></div>
    <div class="panel-wrapper collapse in" aria-expanded="true">
     <div class="panel-body">
      <form action="#" class="form-horizontal">
       <div class="form-body">
        <h3 class="box-title">Referensi Probis <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">

        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Probis</label>
           <div class="col-md-9">
            <select disabled id='probis' class="form-control required" error='Probis'>
             <?php if (!empty($list_probis)) { ?>
              <?php foreach ($list_probis as $value) { ?>
               <?php $selected = '' ?>
               <?php if (isset($probis)) { ?>
                <?php $selected = $probis == $value['id'] ? 'selected' : '' ?>
               <?php } ?>
               <option upt="<?php echo $value['upt'] ?>" <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['no_probis'] . " - " . $value['nama_probis'] ?></option>
              <?php } ?>
             <?php } ?>
            </select>
           </div>
          </div>
         </div> 
        </div>

        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Nama Dokumen</label>
           <div class="col-md-9">
            <input type="text" id='nama' class="form-control required" placeholder="Nama Dokumen" error='Nama Dokumen' value="<?php echo isset($nama_dokumen) ? $nama_dokumen : '' ?>">
           </div>
          </div>
         </div>         
         <!--/span-->
        </div>       

        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">File (.pdf)</label>
           <div class="col-sm-9">
            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
             <div class="form-control" data-trigger="fileinput"> 
              <i class="glyphicon glyphicon-file fileinput-exists"></i> 
              <span class="fileinput-filename"><?php echo isset($file) ? $file : '' ?></span>
             </div> 
             <span class="input-group-addon btn btn-default btn-file"> 
              <span class="fileinput-new">Select file</span> 
              <span class="fileinput-exists">Change</span>
              <input id='file' type="file" name="..." onchange="ReferensiProbis.getFilename(this, 'pdf')"> 
             </span> 
             <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> 
            </div>
           </div>
          </div>
         </div> 
        </div>       
       </div>


       <div class="form-actions">
        <div class="row">
         <div class="col-md-12">
          <div class="row">
           <div class="col-md-offset-3 col-md-9 text-right">
            <button type="submit" class="btn btn-success" onclick="ReferensiProbis.simpanOutput('<?php echo isset($id) ? $id : '' ?>', event)">Submit</button>
            <button type="button" class="btn btn-default" onclick="ReferensiProbis.back()">Batal</button>
           </div>
          </div>
         </div>
         <div class="col-md-6"> </div>
        </div>
       </div>
      </form>
     </div>
    </div>
   </div>
  </div>
 </div>
 <!--./row--> 
</div>

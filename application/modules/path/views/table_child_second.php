<table class="table color-bordered-table danger-bordered-table" id="tabel_path">
 <thead>
  <tr class="">
   <th class="font-12 text-center" colspan="3" width="0">No.</th>
   <th class="font-12" width="350">No Dokumen</th>
   <th class="font-12" width="500">Probis</th>
   <th class="font-12" width="500">Status</th>
   <th class="font-12" width="500">Keterangan</th>
   <th class="text-center font-12" width="100">Action</th>
  </tr>
 </thead>
 <tbody>
  <?php if (!empty($content)) { ?>
   <?php $no = 1; ?>
   <?php foreach ($content as $value) { ?>
    <tr id="<?php echo $value['id'] ?>">
     <td class='font-16 text-center'><label class="text-primary"><u><b><?php echo $value['parent'] ?></b></u></label></td>
     <td class='font-16 text-center'><label class="text-warning"><u><b><?php echo $value['child_first'] ?></b></u></label></td>
     <td class='font-16 text-center'><label class="text-success"><u><b><?php echo $value['child_second'] ?></b></u></label></td>
     <td class='font-14'><?php echo $value['no_probis'] ?></td>
     <td class='font-14'><?php echo $value['nama_probis'] ?></td>
     <?php $color = ""; ?>
     <?php if ($value['status'] == 'PROGRESS') { ?>
      <?php $color = 'text-warning'; ?>
     <?php } ?>
     <?php if ($value['status'] == 'ONCOMING') { ?>
      <?php $color = 'text-primary'; ?>
     <?php } ?>
     <?php if ($value['status'] == 'PROSES REVISI') { ?>
      <?php $color = 'text-info'; ?>
     <?php } ?>
     <?php if ($value['status'] == 'DONE') { ?>
      <?php $color = 'text-success'; ?>
     <?php } ?>
     <td class='font-14 text-center'><label class="<?php echo $color; ?>"><?php echo $value['status'] ?></label></td>
     <td class='font-14'><?php echo $value['keterangan'] ?></td>
     <td class="text-center">
      <i class="fa fa-trash-o text-danger hover" data-toggle="tooltip" title="Hapus" onclick="Path.deletePath(this)"></i>
     </td>
    </tr>
   <?php } ?>
  <?php } else { ?>
   <tr>
    <td colspan="8">Tidak ada data ditemukan</td>
   </tr>
  <?php } ?>
 </tbody>
</table>

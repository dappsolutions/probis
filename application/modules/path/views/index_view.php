<div class='container-fluid'>

 <div class="white-box stat-widget">  
  <div class="card-body">
   <h4 class="card-title"><u><?php echo $title ?></u></h4>

   <div class='row'>
    <div class='col-md-2'>
     <!--<button id="" class="btn btn-block btn-warning" onclick="Path.add()">Tambah</button>-->
    </div>
    <!--    <div class='col-md-10'>
         <input type='text' name='' id='' onkeyup="Path.search(this, event)" class='form-control' value='' placeholder="Pencarian"/>    
        </div>     -->
   </div>        
   <div class='row'>
    <div class='col-md-12'>
     <?php if (isset($keyword)) { ?>
      <?php if ($keyword != '') { ?>
       Cari Data : "<b><?php echo $keyword; ?></b>"
      <?php } ?>
     <?php } ?>
    </div>
   </div>
   <br/>
   <hr/>

   <div class='row'>
    <div class='col-md-12'>
     <div class="table-responsive">
      <table class="table color-bordered-table primary-bordered-table" id="tabel_path">
       <thead>
        <tr class="">
         <th class="font-12 text-center" colspan="3" width="0">No.</th>
         <th class="font-12" width="350">No Dokumen</th>
         <th class="font-12" width="500">Probis</th>
         <th class="font-12" width="500">Status</th>
         <th class="font-12" width="500">Keterangan</th>
         <th class="text-center font-12" width="100">Action</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($content)) { ?>
         <?php $no = 1; ?>
         <?php foreach ($content as $value) { ?>
          <tr id="<?php echo $value['id'] ?>">
           <td class='font-16 text-center'><label class="text-primary"><u><b><?php echo $value['parent'] ?></b></u></label></td>
           <td class='font-16 text-center'><label class="text-warning"><u><b><?php echo $value['child_first'] ?></b></u></label></td>
           <td class='font-16 text-center'><label class="text-success"><u><b><?php echo $value['child_second'] ?></b></u></label></td>
           <td class='font-14'><?php echo $value['no_probis'] ?></td>
           <td class='font-14'><?php echo $value['nama_probis'] ?></td>
           <?php $color = ""; ?>
           <?php if ($value['status'] == 'PROGRESS') { ?>
            <?php $color = 'text-warning'; ?>
           <?php } ?>
           <?php if ($value['status'] == 'ONCOMING') { ?>
            <?php $color = 'text-primary'; ?>
           <?php } ?>
           <?php if ($value['status'] == 'PROSES REVISI') { ?>
            <?php $color = 'text-info'; ?>
           <?php } ?>
           <?php if ($value['status'] == 'DONE') { ?>
            <?php $color = 'text-success'; ?>
           <?php } ?>
           <td class='font-14 text-center'><label class="<?php echo $color; ?>"><?php echo $value['status'] ?></label></td>
           <td class='font-14'><?php echo $value['keterangan'] ?></td>
           <td class="text-center">
            <i class="fa fa-trash-o text-danger hover" data-toggle="tooltip" title="Hapus" onclick="Path.deletePath(this)"></i>
            &nbsp;
            <i id="toogle-action" data-toggle="tooltip" title="Maximize/Minimize" class="fa fa-chevron-down text-primary hover" 
               parent="<?php echo $value['parent'] ?>" child_first="0" 
               onclick="Path.getDetailChildProbis(this, '1')"></i>
           </td>
          </tr>
         <?php } ?>
        <?php } ?>
        <tr class="baru">
         <td>
          <input type="text" value="" id="parent" class="form-control" style="width: 50px;"/>
         </td>
         <td>
          <input type="text" value="" id="child_first" class="form-control" style="width: 50px;"/>
         </td>
         <td>
          <input type="text" value="" id="child_second" class="form-control" style="width: 50px;"/>
         </td>
         <td>
          <input type="text" value="" id="no_dokumen" class="form-control" disabled=""/>
         </td>
         <td colspan="3">
          <select class="form-control" id="probis" onchange="Path.chooseProbis(this)">
           <option>Pilih Probis</option>
           <?php if (!empty($data_probis)) { ?>
            <?php foreach ($data_probis as $value) { ?>
             <option no_dokumen="<?php echo $value['no_probis'] ?>" value="<?php echo $value['id'] ?>"><?php echo $value['nama_probis'] ?> <label id="keyword"><b><?php echo ' (keyword : ' . $value['keyword'] . ')' ?><b/></label></option>
            <?php } ?>
           <?php } ?>
          </select>
         </td>
         <td class="text-center">
          <!--<i id="simpan" class="fa fa-check-circle fa-2x hover display-none" style="color:#2ecc71;" onclick="Path.saveDetail(this)"></i>-->
          <i id="tambah" data-toggle="tooltip" title="Tambah" class="fa fa-plus hover" style="color:#ffb136;" onclick="Path.addDetail(this)"></i>
         </td>
        </tr>
       </tbody>
      </table>
     </div>
    </div>
   </div>    
   <br/>
   <div class="row">
    <div class="col-md-12 text-right">
     <button class="btn btn-success" onclick="Path.simpanDetail()">SIMPAN</button>
    </div>
   </div>
  </div>
 </div>
</div>

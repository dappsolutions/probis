<?php

class Path extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $upt;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
  $this->upt = $this->session->userdata('upt_id');
 }

 public function getModuleName() {
  return 'path';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/plugins/components/custom-select/custom-select.min.js" type="text/javascript"></script>',
      '<script src = "' . base_url() . 'assets/plugins/components/bootstrap-select/bootstrap-select.min.js" type = "text/javascript"></script>',
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/path.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'path_probis';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;


  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Path";
  $data['title_content'] = 'Data Path';
  $content = $this->getDataPath();
  $data['content'] = $content['data'];
  $data['data_probis'] = $this->getDataProbis();
//  echo '<pre>';
//  print_r($data['content']);die;
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataPath($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.no_probis', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*'),
              'join' => array(
                  array('probis p', 't.probis = p.id'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => "t.deleted = 0 and t.child_first = 0",
              'orderby' => 't.parent, t.child_first, child_second, child_third'
  ));

  return $total;
 }

 public function getDataPath($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.no_probis', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*', 'p.nama_probis', 'p.no_probis', 'p.file_pdf',
                  'p.file_excel', 'ps.status', 'ps.keterangan'),
              'join' => array(
                  array('probis p', 't.probis = p.id'),
                  array('(select max(id) as id, probis from probis_status group by probis) pss', 'pss.probis = p.id', 'left'),
                  array('probis_status ps', 'ps.id = pss.id', 'left'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => "t.deleted = 0 and t.child_first = 0",
              'orderby' => 't.parent, t.child_first, child_second, child_third'
  ));

//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['status'] = $value['status'] == '' ? '-' : $value['status'];
    $value['no_data'] = $value['parent'] . $value['child_first'] . $value['child_second'];
    $value['child_third'] = $value['child_third'] == '0' || $value['child_third'] == '' ? '' : $value['child_third'];
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataPath($keyword)
  );
 }

 public function getDataProbis() {
  $data = Modules::run('database/get', array(
              'table' => 'probis t',
              'field' => array('t.*', 'u.username', 'ps.status', 't.deleted as hapus_probis'),
              'join' => array(
                  array('user u', 't.user = u.id'),
                  array('path_probis pp', 't.id = pp.probis and pp.deleted = 0', 'left'),
                  array('(select max(id) as id, probis from probis_status group by probis) pss', 'pss.probis = t.id', 'left'),
                  array('probis_status ps', 'ps.id = pss.id', 'left'),
              ),
              'where' => "t.deleted = 0 and (pp.probis is null)",
              'orderby' => 'pp.parent, pp.child_first, pp.child_second, pp.child_third'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
//    echo '<pre>';
//    print_r($value);die;    
    $value['status'] = $value['status'] == '' ? '-' : $value['status'];
    $value['keyword'] = $this->getKeywordProbis($value['id']);
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getKeywordProbis($probis) {
  $data = Modules::run('database/get', array(
              'table' => 'keyword_probis kb',
              'field' => array('kb.*'),
              'where' => "kb.deleted = 0 and kb.probis = '" . $probis . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $result[] = $value['keyword'];
   }
  }

  $label = "-";
  if (!empty($result)) {
   $label = implode(',', $result);
  }

  return $label;
 }

 public function getListProbis() {
  $data['data_probis'] = $this->getDataProbis();
  $data['index'] = $this->input->post('index');
  echo $this->load->view('list_probis', $data, true);
 }

 public function getDetailDataPath($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*',
                  'u.username', 'u.password',
                  'p.hak_akses', 'ut.nama_upt',
                  'u.priveledge', 'pp.no_probis as no_usulan', 'pp.id as id_usulan'),
              'join' => array(
                  array('user u', 't.user = u.id'),
                  array('priveledge p', 'u.priveledge = p.id'),
                  array('upt ut', 't.upt = ut.id'),
                  array('pengajuan_probis pp', 't.pengajuan_probis = pp.id', 'left'),
              ),
              'where' => "t.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function getListUpt() {
  $data = Modules::run('database/get', array(
              'table' => 'upt',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getListHakAkses() {
  $data = Modules::run('database/get', array(
              'table' => 'priveledge',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    if ($value['id'] != 1) {
     array_push($result, $value);
    }
   }
  }

  return $result;
 }

 public function getListHubunganWali() {
  $data = Modules::run('database/get', array(
              'table' => 'hubungan_wali',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getListSemester() {
  $data = Modules::run('database/get', array(
              'table' => 'semester',
              'where' => 'deleted = 0'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Path";
  $data['title_content'] = 'Tambah Path';
  $data['list_upt'] = $this->getListUpt();
  $data['list_hak_akses'] = $this->getListHakAkses();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataPath($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Path";
  $data['title_content'] = 'Ubah Path';
  echo Modules::run('template', $data);
 }

 public function getDetailDataOrganisasi($id) {
  $data = Modules::run('database/get', array(
              'table' => 'masjid_has_organisasi mho',
              'field' => array('mho.*', 'o.nama_organisasi'),
              'join' => array(
                  array('organisasi o', 'mho.organisasi = o.id')
              ),
              'where' => "mho.deleted = 0 and mho.masjid = '" . $id . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getDetailPengurusPath($masjid) {
  $data = Modules::run('database/get', array(
              'table' => 'masjid_has_pengurus mhp',
              'field' => array('mhp.*', 'j.jabatan as nama_jabatan'),
              'join' => array(
                  array('jabatan j', 'mhp.jabatan = j.id')
              ),
              'where' => array('mhp.deleted' => 0, 'mhp.masjid' => $masjid)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataWaliMurid($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_wali_murid thw',
              'field' => array('thw.*', 'hw.hubungan'),
              'join' => array(
                  array('hubungan_wali hw', 'hw.id = thw.hubungan_wali')
              ),
              'where' => "thw.deleted = 0 and thw.taruna = '" . $taruna . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataAgama($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_agama ta',
              'field' => array('ta.*'),
              'where' => "ta.deleted = 0 and ta.taruna = '" . $taruna . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['status'] = $value['status'] == 1 ? 'Aktif' : 'Tidak Aktif';
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataKesehatan($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_kesehatan ta',
              'field' => array('ta.*'),
              'where' => "ta.deleted = 0 and ta.taruna = '" . $taruna . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['status'] = $value['status'] == 1 ? 'Aktif' : 'Tidak Aktif';
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataAkademik($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_akademik ta',
              'field' => array('ta.*', 's.semester as semester_taruna',
                  'p.prodi as program_studi'),
              'join' => array(
                  array('prodi p', 'ta.prodi = p.id'),
                  array('semester s', 'ta.semester = s.id'),
              ),
              'where' => "ta.deleted = 0 and ta.taruna = '" . $taruna . "'"
  ));


  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['status'] = $value['status'] == 1 ? 'Aktif' : 'Tidak Aktif';
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function detail($id) {
  $data = $this->getDetailDataPath($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Path";
  $data['title_content'] = "Detail Path";
  echo Modules::run('template', $data);
 }

 public function getDetailFeedback($id) {
  $data = Modules::run('database/get', array(
              'table' => 'feedback_probis fb',
              'field' => array('fb.*', 'p.nama_pegawai'),
              'join' => array(
                  array('user u', 'fb.user = u.id'),
                  array('pegawai p', 'u.id = p.user'),
              ),
              'where' => "fb.deleted = 0 and fb.probis = '" . $id . "'",
              'orderby' => 'fb.id desc'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function feedback($id) {
  $data = $this->getDetailDataPath($id);
  $data['view_file'] = 'feedback_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Feedback Path";
  $data['title_content'] = "Feedback Path";
  $data['feedback'] = $this->getDetailFeedback($id);
  echo Modules::run('template', $data);
 }

 public function getPostDataPath($value) {
  $data['no_probis'] = Modules::run('no_generator/generateNoPath');
  $data['user'] = $this->session->userdata('user_id');
  $data['upt'] = $this->upt;
  $data['nama_probis'] = $value->nama;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));

//  echo '<pre>';
//  print_r($_FILES);
//  die;
  $id = $this->input->post('id');
  $is_valid = false;
  $message = "";
  $this->db->trans_begin();
  try {
   $post = $this->getPostDataPath($data);

   $is_uploaded = true;
   if ($id == '') {
    if (!empty($_FILES)) {
     //xls
     if (isset($_FILES['file_xls'])) {
      $data_file = $this->uploadData("file_xls");
      $is_uploaded = $data_file['is_valid'];
      if ($is_uploaded) {
       $post['file_excel'] = $_FILES['file_xls']['name'];
      } else {
       $is_valid = false;
       $message = $data_file['response'];
      }
     }

     //pdf
     if (isset($_FILES['file_pdf'])) {
      $data_file = $this->uploadData("file_pdf");
      $is_uploaded = $data_file['is_valid'];
      if ($is_uploaded) {
       $post['file_pdf'] = $_FILES['file_pdf']['name'];
      } else {
       $is_valid = false;
       $message = $data_file['response'];
      }
     }
    }

    if ($is_uploaded) {
     $id = Modules::run('database/_insert', $this->getTableName(), $post);
    }
   } else {
    //update
    if (!empty($_FILES)) {
     //xls
     if (isset($_FILES['file_xls'])) {
      $data_file = $this->uploadData("file_xls");
      $is_uploaded = $data_file['is_valid'];
      if ($is_uploaded) {
       $post['file_excel'] = $_FILES['file_xls']['name'];
      } else {
       $is_valid = false;
       $message = $data_file['response'];
      }
     }

     //pdf
     if (isset($_FILES['file_pdf'])) {
      $data_file = $this->uploadData("file_pdf");
      $is_uploaded = $data_file['is_valid'];
      if ($is_uploaded) {
       $post['file_pdf'] = $_FILES['file_pdf']['name'];
      } else {
       $is_valid = false;
       $message = $data_file['response'];
      }
     }
    }

    if ($is_uploaded) {
     unset($post['no_probis']);
     Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
    }
   }
   $this->db->trans_commit();
   if ($is_uploaded) {
    $is_valid = true;
   }
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id, 'message' => $message));
 }

 public function sendFeedback($id) {
  $is_valid = false;
  $this->db->trans_begin();
//  echo '<pre>';
//  print_r($_POST);die;
  try {
   $post['probis'] = $id;
   $post['pesan'] = $this->input->post('pesan');
   $post['user'] = $this->session->userdata('user_id');
   Modules::run('database/_insert', 'feedback_probis', $post);
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }
  echo json_encode(array('is_valid' => $is_valid));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Path";
  $data['title_content'] = 'Data Path';
  $content = $this->getDataPath($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function deleteFeedback($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', 'feedback_probis', array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/probis/';
  $config['allowed_types'] = 'pdf|xls';
  $config['max_size'] = '1000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);
  // $this->upload->do_upload($name_of_field);


  $is_valid = false;
  if (!$this->upload->do_upload($name_of_field)) {
   $response = $this->upload->display_errors();
  } else {
   $response = $this->upload->data();
   $is_valid = true;
  }

  return array(
      'is_valid' => $is_valid,
      'response' => $response
  );
 }

 public function showLogo() {
  $foto = str_replace(' ', '_', $this->input->post('file'));
  $data['file'] = $foto;
  echo $this->load->view('file', $data, true);
 }

 public function download($id) {
  $this->load->helper("download");
  $probis = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' js',
              'where' => "js.id = " . $id
          ))->row_array();

  $file = $probis['file_excel'];
  $data = file_get_contents('files/berkas/probis/' . $file);
  $name = $file;
  force_download($name, $data);
 }

 public function detailUsulan($id) {
  $data = Modules::run('review_usulan_probis/getDetailDataPengajuan', $id);
  $data['view_file'] = 'detail_usulan_probis';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Pengajuan";
  $data['title_content'] = "Detail Pengajuan";
  $data['list_status'] = Modules::run('review_usulan_probis/getListStatus', $id);
  $data['tembusan'] = Modules::run('review_usulan_probis/getDataTembusan', $id);
  echo Modules::run('template', $data);
 }

 public function simpanDetail() {
  $data = $this->input->post('data');

  $is_valid = false;
  $this->db->trans_begin();
  try {
   if (!empty($data)) {
    foreach ($data as $value) {
     if (count($value) > 2) {
      if ($value['parent'] != '' && $value['child_first'] != '' && $value['child_second'] != '') {
       $post_data['parent'] = $value['parent'];
       $post_data['child_first'] = $value['child_first'];
	   $post_data['child_second'] = $value['child_second'];
	   
	   $data_number = explode('.', $value['child_second']);
	   if(count($data_number) == 2){
		$post_data['child_third'] = $data_number[1];
	   }
       $post_data['probis'] = $value['probis'];
       if (!isset($value['id'])) {
        Modules::run('database/_insert', $this->getTableName(), $post_data);
       }
      }
     }
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function getDataPathChild() {
  $parent = $_POST['parent'];
  $level = $_POST['level'];

  if ($level == '1') {
   $where = "t.deleted = 0 and t.parent = '" . $parent . "' and t.child_first != 0 and t.child_second = 0";
  }
  if ($level == '2') {
   $child_first = $_POST['child_first'];
   $where = "t.deleted = 0 and t.parent = '" . $parent . "' and t.child_first = '" . $child_first . "' and t.child_second != 0";
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*', 'p.nama_probis', 'p.no_probis', 'p.file_pdf',
                  'p.file_excel', 'ps.status', 'ps.keterangan'),
              'join' => array(
                  array('probis p', 't.probis = p.id'),
                  array('(select max(id) as id, probis from probis_status group by probis) pss', 'pss.probis = p.id', 'left'),
                  array('probis_status ps', 'ps.id = pss.id', 'left'),
              ),
              'where' => $where,
              'orderby' => 't.parent, t.child_first, t.child_second, t.child_third'
  ));

//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['status'] = $value['status'] == '' ? '-' : $value['status'];
    $value['no_data'] = $value['parent'] . $value['child_first'] . $value['child_second'];
    $value['child_third'] = $value['child_third'] == '0' || $value['child_third'] == '' ? '' : $value['child_third'];
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getDetailChildProbis() {
  $data = $this->getDataPathChild();
  $content['content'] = $data;
//  echo '<pre>';
//  print_r($data);die;
  if ($_POST['level'] == '1') {
   echo $this->load->view('table_child_fist', $content, true);
  }
  
  if ($_POST['level'] == '2') {
   echo $this->load->view('table_child_second', $content, true);
  }
 }

}

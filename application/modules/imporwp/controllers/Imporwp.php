<?php

//

class Imporwp extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $upt;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'imporwp';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/imporwp.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'upt';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Import Data";
  $data['title_content'] = 'Import Data';
  echo Modules::run('template', $data);
 }

 public function getDataUptId($upt) {
  $data = Modules::run('database/get', array(
              'table' => 'upt ut',
              'field' => array('ut.*'),
              'where' => "ut.deleted = 0 and ut.nama = '" . $upt . "'"
  ));

  $upt_id = 0;
  if (!empty($data)) {
   $data = $data->row_array();
   $upt_id = $data['id'];
  } else {
   $data_upt['nama'] = trim($upt);
   $upt_id = Modules::run('database/_insert', 'upt', $data_upt);
  }


  return $upt_id;
 }

 public function getDataGi($value) {
  $data = Modules::run('database/get', array(
              'table' => 'gardu_induk gi',
              'field' => array('gi.*'),
              'where' => "gi.deleted = 0 and gi.nama_gardu = '" . trim($value[0]) . "'"
  ));

  $id = 0;
  if (!empty($data)) {
   $data = $data->row_array();
   $id = $data['id'];
  } else {
   $id = 0;
  }


  return $id;
 }

 public function getDataGiFix($gardu) {
  $data = Modules::run('database/get', array(
              'table' => 'gardu_induk gi',
              'field' => array('gi.*'),
              'where' => "gi.deleted = 0 and gi.nama_gardu = '" . trim($gardu) . "'"
  ));

  $id = 0;
  if (!empty($data)) {
   $data = $data->row_array();
   $id = $data['id'];
  } else {
   $id = 0;
  }


  return $id;
 }
 
 public function getDataUltgFix($ultg) {
  $data = Modules::run('database/get', array(
              'table' => 'sub_upt su',
              'field' => array('su.*'),
              'where' => "su.deleted = 0 and su.nama = '" . trim($ultg) . "'"
  ));

  $id = 0;
  if (!empty($data)) {
   $data = $data->row_array();
   $id = $data['id'];
  } else {
   $id = 0;
  }


  return $id;
 }

 public function getDataPegawai($value) {
  $data = Modules::run('database/get', array(
              'table' => 'pegawai pg',
              'field' => array('pg.*'),
              'where' => "pg.deleted = 0 and pg.nip = '" . trim($value[1]) . "'"
  ));

  $id = 0;
  if (!empty($data)) {
   $data = $data->row_array();
   $id = $data['id'];
  } else {
   $id = 0;
  }


  return $id;
 }

 public function getPostDataRegister($value) {
//  $data['no_pengajuan'] = Modules::run("no_generator/generateNoPengajuanVendor");
  $data['no_pengajuan'] = trim($value[1]);
  $data['nama_vendor'] = trim($value[2]);
  $data['pimpinan'] = trim($value[3]);
  $data['email'] = trim($value[6]);
  $data['no_hp'] = trim($value[7]);

  return $data;
 }

 public function getPostDataVendor($value) {
  $data['nama_vendor'] = trim($value[2]);
  $data['pimpinan'] = trim($value[3]);
  $data['email'] = trim($value[6]);
  $data['no_hp'] = trim($value[7]);

  return $data;
 }

 public function getDataPegawaiDanUser($value) {
  $sql = "select p.*
 , usr.id as user_id
from pegawai p
left join `user` usr
	on usr.pegawai = p.id
where p.nip = '" . trim($value[1]) . "'";

  $data = Modules::run('database/get_custom', $sql);
  $result = array();
  if (!empty($data)) {
   $data = $data->row_array();
   $update['email'] = trim($value[2]);
   Modules::run('database/_update', 'pegawai', $update, array('id' => $data['id']));

   $data['email'] = trim($value[2]);
   if ($data['user_id'] == '') {
    $user_post['username'] = trim($value[2]);
    $user_post['password'] = trim($value[3]);
    $user_post['pegawai'] = 1;
    $user = Modules::run('database/_insert', 'user', $user_post);
    $data['user_id'] = $user;
   }

   $result = $data;
  }


  return $result;
 }

 public function importFileApproval() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);die;
  $is_valid = false;
  $result = array();

  $this->db->trans_begin();
  try {
   if (!empty($data)) {
    // echo '<pre>';
    // print_r($data);die;
    for ($i = 0; $i < count($data); $i++) {
     $value = $data[$i];

     $data_pegawai = $this->getDataPegawaiDanUser($value);
     if (!empty($data_pegawai)) {
      $upt_id = $this->getDataUptId(trim($value[7]));
      //struktur_approval internal      
      $post_struktur = array();
      $post_struktur['upt'] = $upt_id;
      $post_struktur['tipe_permit'] = 1;
      $post_struktur['user'] = $data_pegawai['user_id'];
      $post_struktur['level'] = trim($value[6]);
      if (trim($value[6]) == '3') {
       $post_struktur['is_last'] = true;
      }
      $post_struktur['ttd'] = trim($value[8]);
      $struktur_approval_internal = Modules::run('database/_insert', 'struktur_approval', $post_struktur);

      $post_struktur = array();
      $post_struktur['upt'] = $upt_id;
      $post_struktur['tipe_permit'] = 2;
      $post_struktur['user'] = $data_pegawai['user_id'];
      $post_struktur['level'] = trim($value[6]);
      if (trim($value[6]) == '3') {
       $post_struktur['is_last'] = true;
      }
      $post_struktur['ttd'] = trim($value[8]);
      $struktur_approval_eksternal = Modules::run('database/_insert', 'struktur_approval', $post_struktur);

      if (trim($value[9]) != '') {
       //struktur_approval_paraf
       $paraf = array();
       $paraf['struktur_approval'] = $struktur_approval_internal;
       $paraf['file'] = trim($value[9]);
       Modules::run('database/_insert', 'struktur_approval_paraf', $paraf);

       //struktur_approval_paraf
       $paraf = array();
       $paraf['struktur_approval'] = $struktur_approval_eksternal;
       $paraf['file'] = trim($value[9]);
       Modules::run('database/_insert', 'struktur_approval_paraf', $paraf);
      }


      array_push($result, $value);
     }
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $is_valid = false;
   $this->db->trans_rollback();
  }

  $content['result'] = $result;
  $view = $this->load->view('table_data', $content, true);
  echo json_encode(array('is_valid' => $is_valid, 'view' => $view));
 }

 public function importFileVEndor() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);die;
  $is_valid = false;
  $result = array();

  $this->db->trans_begin();
  try {
   if (!empty($data)) {
    // echo '<pre>';
    // print_r($data);die;
    for ($i = 0; $i < count($data); $i++) {
     $value = $data[$i];

     //pengjuan registrasi
     $post_data_register = $this->getPostDataRegister($value);
     $register = Modules::run('database/_insert', 'pengajuan_vendor', $post_data_register);

     //DRAFT
     $vendor_statud = array();
     $vendor_statud['status'] = 'DRAFT';
     $vendor_statud['pengajuan_vendor'] = $register;
     Modules::run('database/_insert', 'status_pengajuan_vendor', $vendor_statud);

     $is_approve = trim($value[9]);
     if ($is_approve) {
      //insert vendor
      $post_data_vendor = $this->getPostDataVendor($value);
      $post_data_vendor['pengajuan_vendor'] = $register;
      $vendor = Modules::run('database/_insert', 'vendor', $post_data_vendor);

      //insert user
      $post_user['username'] = trim($value[6]);
      $post_user['password'] = trim($value[8]);
      $post_user['vendor'] = $vendor;
      Modules::run('database/_insert', 'user', $post_user);

      //APPROVED
      $vendor_statud = array();
      $vendor_statud['status'] = 'APPROVED';
      $vendor_statud['pengajuan_vendor'] = $register;
      Modules::run('database/_insert', 'status_pengajuan_vendor', $vendor_statud);
     }
     array_push($result, $value);
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $is_valid = false;
   $this->db->trans_rollback();
  }

  $content['result'] = $result;
  $view = $this->load->view('table_data', $content, true);
  echo json_encode(array('is_valid' => $is_valid, 'view' => $view));
 }

 public function importFilePegawai() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);die;
  $is_valid = false;
  $result = array();

  $this->db->trans_begin();
  try {
   if (!empty($data)) {
    // echo '<pre>';
    // print_r($data);die;
    for ($i = 0; $i < count($data); $i++) {
     $value = $data[$i];

     $upt_id = $this->getDataUptId(trim($value[9]));
     $post_gi['upt'] = $upt_id;
     $post_gi['nip'] = trim($value[2]);
     $post_gi['nama'] = trim($value[3]);
     $post_gi['email'] = trim($value[6]);
     $post_gi['no_hp'] = trim($value[7]);
     $post_gi['posisi'] = trim($value[4]);
     $post_gi['posisi_lengkap'] = trim($value[5]);
     $pegawai_id = Modules::run('database/_insert', 'pegawai', $post_gi);

     if (trim($value[8]) != '') {
      //user
      $post_user['pegawai'] = $pegawai_id;
      $post_user['username'] = trim($value[6]);
      $post_user['password'] = trim($value[8]);
      Modules::run('database/_insert', 'user', $post_user);
     }
     array_push($result, $value);
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $is_valid = false;
   $this->db->trans_rollback();
  }

  $content['result'] = $result;
  $view = $this->load->view('table_data', $content, true);
  echo json_encode(array('is_valid' => $is_valid, 'view' => $view));
 }

 public function importFileSubUnit() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);die;
  $is_valid = false;
  $result = array();

  $this->db->trans_begin();
  try {
   if (!empty($data)) {
    // echo '<pre>';
    // print_r($data);die;
    for ($i = 0; $i < count($data); $i++) {
     $value = $data[$i];

     $upt_id = $this->getDataUptId(trim($value['2']));
     $post_gi['upt'] = $upt_id;
     $post_gi['nama'] = trim($value[1]);
     Modules::run('database/_insert', 'sub_upt', $post_gi);
     array_push($result, $value);
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $is_valid = false;
   $this->db->trans_rollback();
  }

  $content['result'] = $result;
  $view = $this->load->view('table_data', $content, true);
  echo json_encode(array('is_valid' => $is_valid, 'view' => $view));
 }

 public function importFileGI() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);die;
  $is_valid = false;
  $result = array();

  $this->db->trans_begin();
  try {
   if (!empty($data)) {
    // echo '<pre>';
    // print_r($data);die;
    for ($i = 0; $i < count($data); $i++) {
     $value = $data[$i];

     $upt_id = $this->getDataUptId(trim($value['4']));
     $post_gi['upt'] = $upt_id;
     $post_gi['nama_gardu'] = trim($value[1]);
     Modules::run('database/_insert', 'gardu_induk', $post_gi);
     array_push($result, $value);
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $is_valid = false;
   $this->db->trans_rollback();
  }

  $content['result'] = $result;
  $view = $this->load->view('table_data', $content, true);
  echo json_encode(array('is_valid' => $is_valid, 'view' => $view));
 }

 public function importFileCatatan() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);die;
  $is_valid = false;
  $result = array();

  $this->db->trans_begin();
  try {
   if (!empty($data)) {
    // echo '<pre>';
    // print_r($data);die;
    for ($i = 0; $i < count($data); $i++) {
     $value = $data[$i];

     $post_gi['catatan'] = trim($value[1]);
     Modules::run('database/_insert', 'catatan_khusus', $post_gi);
     array_push($result, $value);
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $is_valid = false;
   $this->db->trans_rollback();
  }

  $content['result'] = $result;
  $view = $this->load->view('table_data', $content, true);
  echo json_encode(array('is_valid' => $is_valid, 'view' => $view));
 }

 public function importFileSld() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);die;
  $is_valid = false;
  $result = array();

  $this->db->trans_begin();
  try {
   if (!empty($data)) {
    // echo '<pre>';
    // print_r($data);die;
    for ($i = 0; $i < count($data); $i++) {
     $value = $data[$i];

     $post_gi['nama'] = trim($value[1]);
     $post_gi['file'] = trim($value[2]);
     Modules::run('database/_insert', 'sld', $post_gi);
     array_push($result, $value);
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $is_valid = false;
   $this->db->trans_rollback();
  }

  $content['result'] = $result;
  $view = $this->load->view('table_data', $content, true);
  echo json_encode(array('is_valid' => $is_valid, 'view' => $view));
 }

 public function importFileSPVGi() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);die;
  $is_valid = false;
  $result = array();

  $this->db->trans_begin();
  try {
   if (!empty($data)) {
    // echo '<pre>';
    // print_r($data);die;
    for ($i = 0; $i < count($data); $i++) {
     $value = $data[$i];

     $gi = $this->getDataGi($value);
     $pegawai = $this->getDataPegawai($value);

     $post_gi['gardu_induk'] = $gi;
     $post_gi['pegawai'] = $pegawai;
     Modules::run('database/_insert', 'gardu_induk_has_pegawai', $post_gi);
     array_push($result, $value);
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $is_valid = false;
   $this->db->trans_rollback();
  }

  $content['result'] = $result;
  $view = $this->load->view('table_data', $content, true);
  echo json_encode(array('is_valid' => $is_valid, 'view' => $view));
 }

 public function importFileSutet() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);
//  die;
  $is_valid = false;
  $result = array();

  $this->db->trans_begin();
  try {
   if (!empty($data)) {
    // echo '<pre>';
    // print_r($data);die;
    for ($i = 0; $i < count($data); $i++) {
     $value = $data[$i];

     $gi = $this->getDataGiFix($value[1]);
//     echo $gi;die;
     if ($gi != 0) {
      $post_gi['gardu_induk'] = $gi;
      $post_gi['nama'] = trim($value[3]);
      Modules::run('database/_insert', 'sutet', $post_gi);
     }
     array_push($result, $value);
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $is_valid = false;
   $this->db->trans_rollback();
  }

  $content['result'] = $result;
  $view = $this->load->view('table_data', $content, true);
  echo json_encode(array('is_valid' => $is_valid, 'view' => $view));
 }
 
 public function importFileBay() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);
//  die;
  $is_valid = false;
  $result = array();

  $this->db->trans_begin();
  try {
   if (!empty($data)) {
    // echo '<pre>';
    // print_r($data);die;
    for ($i = 0; $i < count($data); $i++) {
     $value = $data[$i];

     $gi = $this->getDataGiFix($value[2]);
//     echo $gi;die;
     if ($gi != 0) {
      $post_gi['gardu_induk'] = $gi;
      $post_gi['bay'] = trim($value[1]);
      Modules::run('database/_insert', 'gardu_induk_bay', $post_gi);
     }
     array_push($result, $value);
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $is_valid = false;
   $this->db->trans_rollback();
	}
}
 
	public function importFile() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);
//  die;
  $is_valid = false;
  $result = array();

  $this->db->trans_begin();
  try {
   if (!empty($data)) {
    // echo '<pre>';
    // print_r($data);die;
    for ($i = 0; $i < count($data); $i++) {
     $value = $data[$i];

     $gi = $this->getDataGiFix($value[1]);
     $sub_upt = $this->getDataUltgFix($value[2]);
//     echo $gi;die;
     if ($gi != 0 && $sub_upt != 0) {
      $post_gi['gardu_induk'] = $gi;
      $post_gi['sub_upt'] = $sub_upt;
      Modules::run('database/_insert', 'sub_upt_has_gardu', $post_gi);
     }
     array_push($result, $value);
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $is_valid = false;
   $this->db->trans_rollback();
  }

  $content['result'] = $result;
  $view = $this->load->view('table_data', $content, true);
  echo json_encode(array('is_valid' => $is_valid, 'view' => $view));
 }

}

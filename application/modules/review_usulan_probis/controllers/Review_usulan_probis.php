<?php

class Review_usulan_probis extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $upt;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
  $this->upt = $this->session->userdata('upt_id');
 }

 public function getModuleName() {
  return 'review_usulan_probis';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/helper.js"></script>',
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/review_usulan_probis.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'pengajuan_probis';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Pengajuan";
  $data['title_content'] = 'Data Pengajuan';
  $content = $this->getDataPengajuan();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataPengajuan($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.no_probis', $keyword),
       array('t.nama_probis', $keyword),
       array('u.username', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*', 'u.username', 'ps.status'),
              'join' => array(
                  array('user u', 't.user = u.id'),
                  array('(select max(id) as id, pengajuan_probis from pengajuan_probis_has_status GROUP by pengajuan_probis) st', 't.id = st.pengajuan_probis'),
                  array('pengajuan_probis_has_status ps', "st.id = ps.id")
              ),
              'where' => "t.deleted = 0 and t.upt = '" . $this->upt . "' and (ps.status != 'REJECTED' and ps.status != 'APPROVED')"
  ));

  return $total;
 }

 public function getDataPengajuan($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.no_probis', $keyword),
       array('t.nama_probis', $keyword),
       array('u.username', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*', 'u.username', 'ps.status'),
              'join' => array(
                  array('user u', 't.user = u.id'),
                  array('(select max(id) as id, pengajuan_probis from pengajuan_probis_has_status GROUP by pengajuan_probis) st', 't.id = st.pengajuan_probis'),
                  array('pengajuan_probis_has_status ps', "st.id = ps.id")
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "t.deleted = 0 and t.upt = '" . $this->upt . "' and (ps.status != 'REJECTED' and ps.status != 'APPROVED')"
  ));

//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataPengajuan($keyword)
  );
 }

 public function getDetailDataPengajuan($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*',
                  'u.username', 'u.password',
                  'p.hak_akses', 'ut.nama_upt',
                  'u.priveledge'),
              'join' => array(
                  array('user u', 't.user = u.id'),
                  array('priveledge p', 'u.priveledge = p.id'),
                  array('upt ut', 't.upt = ut.id'),
              ),
              'where' => "t.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function getListUpt() {
  $data = Modules::run('database/get', array(
              'table' => 'upt',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getListHakAkses() {
  $data = Modules::run('database/get', array(
              'table' => 'priveledge',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    if ($value['id'] != 1) {
     array_push($result, $value);
    }
   }
  }

  return $result;
 }

 public function getListHubunganWali() {
  $data = Modules::run('database/get', array(
              'table' => 'hubungan_wali',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getListSemester() {
  $data = Modules::run('database/get', array(
              'table' => 'semester',
              'where' => 'deleted = 0'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Pengajuan";
  $data['title_content'] = 'Tambah Pengajuan';
  $data['list_upt'] = $this->getListUpt();
  $data['list_hak_akses'] = $this->getListHakAkses();
  echo Modules::run('template', $data);
 } 
 
 public function ubah($id) {
  $data = $this->getDetailDataPengajuan($id);  
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Pengajuan";
  $data['title_content'] = 'Ubah Pengajuan';
  echo Modules::run('template', $data);
 }

 public function getDetailDataOrganisasi($id) {
  $data = Modules::run('database/get', array(
              'table' => 'masjid_has_organisasi mho',
              'field' => array('mho.*', 'o.nama_organisasi'),
              'join' => array(
                  array('organisasi o', 'mho.organisasi = o.id')
              ),
              'where' => "mho.deleted = 0 and mho.masjid = '" . $id . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getDetailPengurusPengajuan($masjid) {
  $data = Modules::run('database/get', array(
              'table' => 'masjid_has_pengurus mhp',
              'field' => array('mhp.*', 'j.jabatan as nama_jabatan'),
              'join' => array(
                  array('jabatan j', 'mhp.jabatan = j.id')
              ),
              'where' => array('mhp.deleted' => 0, 'mhp.masjid' => $masjid)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataWaliMurid($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_wali_murid thw',
              'field' => array('thw.*', 'hw.hubungan'),
              'join' => array(
                  array('hubungan_wali hw', 'hw.id = thw.hubungan_wali')
              ),
              'where' => "thw.deleted = 0 and thw.taruna = '" . $taruna . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataAgama($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_agama ta',
              'field' => array('ta.*'),
              'where' => "ta.deleted = 0 and ta.taruna = '" . $taruna . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['status'] = $value['status'] == 1 ? 'Aktif' : 'Tidak Aktif';
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataKesehatan($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_kesehatan ta',
              'field' => array('ta.*'),
              'where' => "ta.deleted = 0 and ta.taruna = '" . $taruna . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['status'] = $value['status'] == 1 ? 'Aktif' : 'Tidak Aktif';
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataAkademik($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_akademik ta',
              'field' => array('ta.*', 's.semester as semester_taruna',
                  'p.prodi as program_studi'),
              'join' => array(
                  array('prodi p', 'ta.prodi = p.id'),
                  array('semester s', 'ta.semester = s.id'),
              ),
              'where' => "ta.deleted = 0 and ta.taruna = '" . $taruna . "'"
  ));


  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['status'] = $value['status'] == 1 ? 'Aktif' : 'Tidak Aktif';
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListStatus($id) {
  $data = Modules::run('database/get', array(
              'table' => "pengajuan_probis_has_status ph",
              'field' => array('ph.*', 'p.nama_pegawai'),
              'join' => array(
                  array('user u', 'ph.user = u.id'),
                  array('pegawai p', 'p.user = u.id'),
              ),
              'where' => array('ph.pengajuan_probis' => $id),
              'orderby' => 'ph.id asc'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['createddate'] = Modules::run('helper/getIndoDate', $value['createddate']);
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataTembusan($id) {
  $data = Modules::run('database/get', array(
              'table' => 'approval_probis ap',
              'field' => array('ap.*', 'p.nip', 'p.nama_pegawai'),
              'join' => array(
                  array('pegawai p', 'ap.pegawai = p.id')
              ),
              'where' => "ap.deleted = 0 and ap.pengajuan_probis = '".$id."'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function detail($id) {
  $data = $this->getDetailDataPengajuan($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Pengajuan";
  $data['title_content'] = "Detail Pengajuan";
  $data['list_status'] = $this->getListStatus($id);
  $data['tembusan'] = $this->getDataTembusan($id);
  echo Modules::run('template', $data);
 }

 public function getPostDataPengajuan($value) {
  $data['no_probis'] = Modules::run('no_generator/generateNoUsulan');
  $data['user'] = $this->session->userdata('user_id');
  $data['upt'] = $this->upt;
  $data['nama_probis'] = $value->nama;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));

//  echo '<pre>';
//  print_r($_FILES);
//  die;
  $id = $this->input->post('id');
  $is_valid = false;
  $message = "";
  $this->db->trans_begin();
  try {
   $post = $this->getPostDataPengajuan($data);

   $is_uploaded = true;
   if ($id == '') {
    if (!empty($_FILES)) {
     //xls
     if (isset($_FILES['file_xls'])) {
      $data_file = $this->uploadData("file_xls");
      $is_uploaded = $data_file['is_valid'];
      if ($is_uploaded) {
       $post['file_excel'] = $_FILES['file_xls']['name'];
      } else {
       $is_valid = false;
       $message = $data_file['response'];
      }
     }

     //pdf
     if (isset($_FILES['file_pdf'])) {
      $data_file = $this->uploadData("file_pdf");
      $is_uploaded = $data_file['is_valid'];
      if ($is_uploaded) {
       $post['file_pdf'] = $_FILES['file_pdf']['name'];
      } else {
       $is_valid = false;
       $message = $data_file['response'];
      }
     }
    }

    if ($is_uploaded) {
     $id = Modules::run('database/_insert', $this->getTableName(), $post);

     //insert ke pengajuan has status
     $post_status['pengajuan_probis'] = $id;
     $post_status['user'] = $this->session->userdata('user_id');
     $post_status['status'] = 'DRAFT';
     $id = Modules::run('database/_insert', 'pengajuan_probis_has_status', $post_status);
    }
   } else {
    //update
    if (!empty($_FILES)) {
     //xls
     if (isset($_FILES['file_xls'])) {
      $data_file = $this->uploadData("file_xls");
      $is_uploaded = $data_file['is_valid'];
      if ($is_uploaded) {
       $post['file_excel'] = $_FILES['file_xls']['name'];
      } else {
       $is_valid = false;
       $message = $data_file['response'];
      }
     }

     //pdf
     if (isset($_FILES['file_pdf'])) {
      $data_file = $this->uploadData("file_pdf");
      $is_uploaded = $data_file['is_valid'];
      if ($is_uploaded) {
       $post['file_pdf'] = $_FILES['file_pdf']['name'];
      } else {
       $is_valid = false;
       $message = $data_file['response'];
      }
     }
    }

    if ($is_uploaded) {
     unset($post['no_probis']);
     Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
    }
   }
   $this->db->trans_commit();
   if ($is_uploaded) {
    $is_valid = true;
   }
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id, 'message' => $message));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Pengajuan";
  $data['title_content'] = 'Data Pengajuan';
  $content = $this->getDataPengajuan($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/probis/';
  $config['allowed_types'] = 'pdf|xls|xlsx';
  $config['max_size'] = '1000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);
  // $this->upload->do_upload($name_of_field);


  $is_valid = false;
  if (!$this->upload->do_upload($name_of_field)) {
   $response = $this->upload->display_errors();
  } else {
   $response = $this->upload->data();
   $is_valid = true;
  }

  return array(
      'is_valid' => $is_valid,
      'response' => $response
  );
 }

 public function showLogo() {
  $foto = str_replace(' ', '_', $this->input->post('file'));
  $data['file'] = $foto;
  echo $this->load->view('file', $data, true);
 }

 public function download($id) {
  $this->load->helper("download");
  $probis = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' js',
              'where' => "js.id = " . $id
          ))->row_array();

  $file = $probis['file_excel'];
  $data = file_get_contents('files/berkas/probis/' . $file);
  $name = $file;
  force_download($name, $data);
 }

 public function rejected($id) {
  $post['user'] = $this->session->userdata('user_id');
  $post['pengajuan_probis'] = $id;
  $post['status'] = 'REJECTED';
  $post['keterangan'] = $this->input->post('keterangan');

  Modules::run('database/_insert', 'pengajuan_probis_has_status', $post);
 }

 public function approved($id) {
  $data = array();
  $content = $this->getDataPegawai();
  $data['content'] = $content['data'];
  echo $this->load->view('approve_view', $data, true);
 }

 public function getTotalDataPegawai($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.nip', $keyword),
       array('t.nama_pegawai', $keyword),
       array('u.nama_upt', $keyword),
       array('pv.hak_akses', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => 'pegawai t',
              'field' => array('t.*', 'u.nama_upt', 'pv.hak_akses'),
              'join' => array(
                  array('upt u', 'u.id = t.upt'),
                  array('user us', 't.user = us.id'),
                  array('priveledge pv', 'us.priveledge = pv.id'),
              ),
              'where' => "t.deleted = 0"
  ));

  return $total;
 }

 public function getDataPegawai($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.nip', $keyword),
       array('t.nama_pegawai', $keyword),
       array('u.nama_upt', $keyword),
       array('pv.hak_akses', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => 'pegawai t',
              'field' => array('t.*', 'u.nama_upt', 'pv.hak_akses'),
              'join' => array(
                  array('upt u', 'u.id = t.upt'),
                  array('user us', 't.user = us.id'),
                  array('priveledge pv', 'us.priveledge = pv.id'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => "t.deleted = 0 and u.id = '" . $this->upt . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataPegawai($keyword)
  );
 }

 public function reviewed($id) {
  $post['user'] = $this->session->userdata('user_id');
  $post['pengajuan_probis'] = $id;
  $post['status'] = 'REVIEWED';

  Modules::run('database/_insert', 'pengajuan_probis_has_status', $post);
 }

 public function getDataPengajuanDetail($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' p',
              'field' => array('p.*'),
              'where' => array('p.id' => $id)
  ));

  return $data->row_array();
 }

 public function execApproved($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   $data = $this->input->post('data');
   $post['user'] = $this->session->userdata('user_id');
   $post['pengajuan_probis'] = $id;
   $post['status'] = 'APPROVED';
   Modules::run('database/_insert', 'pengajuan_probis_has_status', $post);

   //insert  
   foreach ($data as $value) {
    $post_app['pengajuan_probis'] = $id;
    $post_app['pegawai'] = $value['pegawai'];
    Modules::run('database/_insert', 'approval_probis', $post_app);
   }

   //insert ke master probis
   $data_pengajuan = $this->getDataPengajuanDetail($id);
   $post_probis['user'] = $data_pengajuan['user'];
   $post_probis['upt'] = $data_pengajuan['upt'];
   $post_probis['no_probis'] = Modules::run('no_generator/generateNoProbis');
   $post_probis['nama_probis'] = $data_pengajuan['nama_probis'];
   $post_probis['file_excel'] = $data_pengajuan['file_excel'];
   $post_probis['file_pdf'] = $data_pengajuan['file_pdf'];
   $post_probis['pengajuan_probis'] = $id;
   Modules::run('database/_insert', 'probis', $post_probis);
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function revision($id) {
  $post['user'] = $this->session->userdata('user_id');
  $post['pengajuan_probis'] = $id;
  $post['status'] = 'REVISION';
  $post['keterangan'] = $this->input->post('keterangan');

  Modules::run('database/_insert', 'pengajuan_probis_has_status', $post);
 }

}

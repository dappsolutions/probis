<div class='container-fluid'>

 <div class="white-box stat-widget">  
  <div class="card-body">
   <h4 class="card-title"><u><?php echo 'Pilih Tembusan dari Probis yang Diajukan' ?></u></h4>

   <div class='row'>
    <div class='col-md-12'>
     <input type='text' name='' id='' onkeyup="Review.searchInTable(this, event)" class='form-control' value='' placeholder="Pencarian"/>    
    </div>     
   </div>        
   <br/>
   <div class='row'>
    <div class='col-md-12'>
     <?php if (isset($keyword)) { ?>
      <?php if ($keyword != '') { ?>
       Cari Data : "<b><?php echo $keyword; ?></b>"
      <?php } ?>
     <?php } ?>
    </div>
   </div>
   <br/>

   <div class='row'>
    <div class='col-md-12'>
     <div class="table-responsive">
      <table class="table color-bordered-table primary-bordered-table" id="tb_tembusan">
       <thead>
        <tr class="">
         <th class="font-12">No</th>
         <th class="font-12">NIP</th>
         <th class="font-12">Nama Pegawai</th>
         <th class="font-12">Action</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($content)) { ?>
         <?php $no = 1; ?>
         <?php foreach ($content as $value) { ?>
          <tr id="<?php echo $value['id'] ?>">
           <td class='font-12'><?php echo $no++ ?></td>
           <td class='font-12'><?php echo $value['nip'] ?></td>
           <td class='font-12'><?php echo $value['nama_pegawai'] ?></td>
           <td class='font-12'>
            <label id="" class="label label-success font-10 hover" 
                   onclick="Review.choose(this)">Pilih</label>
           </td>
          </tr>
         <?php } ?>
        <?php } else { ?>
         <tr>
          <td class="text-center font-12" colspan="8">Tidak Ada Data Ditemukan</td>
         </tr>
        <?php } ?>         
       </tbody>
      </table>
     </div>
    </div>
   </div>    
   <br/>
   <div class="row">
    <div class="col-md-12 text-right">
     <button class="btn btn-success font-10" onclick="Review.prosesApprove()">Proses</button>
     <button class="btn btn-danger font-10" onclick="message.closeDialog()">Batal</button>
    </div>
   </div>
  </div>
 </div>
</div>
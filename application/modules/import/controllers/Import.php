<?php

//

class Import extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $upt;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'import';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/import.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'probis';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Import Data";
  $data['title_content'] = 'Import Data';
  echo Modules::run('template', $data);
 }

 public function getDataUptId($upt) {
  $data = Modules::run('database/get', array(
              'table' => 'upt ut',
              'field' => array('ut.*'),
              'where' => "ut.deleted = 0 and ut.nama_upt = '" . $upt . "'"
  ));

  $upt_id = 0;
  if (!empty($data)) {
   $data = $data->row_array();
   $upt_id = $data['id'];
  } else {
   $data_upt['nama_upt'] = trim($upt);
   $upt_id = Modules::run('database/_insert', 'upt', $data_upt);
  }


  return $upt_id;
 }

 public function importFilePersebaranCV() {
  $data = json_decode($this->input->post('data'));

	// echo '<pre>';
	// print_r($data);die;
  $is_valid = false;
  $result = array();

  $this->db->trans_begin();
  try {
   if (!empty($data)) {
    foreach ($data as $value) {
			$params = array();
     $params['nama'] = trim($value[3]);
     $params['email'] = trim($value[10]);
     $params['alamat'] = trim($value[5]);
     $params['kecamatan'] = trim($value[6]);
     $params['lat'] = trim($value[1]);
     $params['lng'] = trim($value[2]);
     $params['no_telp'] = trim($value[9]);
     $params['jenis_penyedia_jasa'] = trim($value[7]);
     $params['tahun_berdiri'] = trim($value[4]);
     $params['pemilik'] = trim($value[8]);

     $info_cv = Modules::run('database/_insert', 'info_cv', $params);

    //  if (trim($value[14]) != '') {
      // $image = explode('&&', trim($value[14]));
      // if (!empty($image)) {
      //  foreach ($image as $v_img) {
        // if ($v_img != '') {
		$img_data = array();
		$img_data['info_cv'] = $info_cv;
		$img_data['file'] = trim($value[11]).'.jpg';
		Modules::run('database/_insert', 'cv_picture', $img_data);
        // }
      //  }
      // }
    //  }
     array_push($result, $value);
    }
   }

   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $is_valid = false;
   $this->db->trans_rollback();
  }

  $content['result'] = $result;
  $view = $this->load->view('table_data', $content, true);
  echo json_encode(array('is_valid' => $is_valid, 'view' => $view));
 }

 public function importFileTrayek() {
  $data = json_decode($this->input->post('data'));

  $is_valid = false;
  $result = array();

//  $this->db->trans_begin();
  try {
   if (!empty($data)) {
    $temp_jalur = "";
    $temp_alamat = "";
    foreach ($data as $value) {
//     if (trim($value[0]) != '' && is_numeric($value[0])) {
     $nama_jalur = $temp_jalur == trim($value[1]) ? $temp_jalur : trim($value[1]);
     if ($nama_jalur == '') {
      $nama_jalur = $temp_jalur;
     }

     $insert['jenis'] = trim($nama_jalur);
     $data = Modules::run('database/get', array(
                 'table' => 'jenis_angkot ja',
                 'field' => array('ja.*'),
                 'where' => "ja.deleted = 0 and ja.jenis = '" . trim($nama_jalur) . "'"
     ));

     $jenis = 0;
     if (!empty($data)) {
      $jenis = $data->row_array()['id'];
     } else {
      $jenis = Modules::run('database/_insert', 'jenis_angkot', $insert);
     }

     $insert_jalur['jenis_angkot'] = $jenis;
     $alamat = $temp_alamat == trim($value[2]) ? $temp_alamat : trim($value[2]);
     $insert_jalur['alamat'] = $alamat == '' ? $temp_alamat : $alamat;
     $insert_jalur['latitude'] = trim($value[4]);
     $insert_jalur['longitude'] = trim($value[3]);
     Modules::run('database/_insert', 'jalur_angkot', $insert_jalur);

     if (trim($value[1]) != '') {
      $temp_jalur = trim($value[1]);
     }

     if (trim($value[2]) != '') {
      $temp_alamat = trim($value[2]);
     }
     array_push($result, $value);
//     }
    }
   }
//   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $is_valid = false;
   $this->db->trans_rollback();
  }

  $content['result'] = $result;
  $view = $this->load->view('table_data', $content, true);
  echo json_encode(array('is_valid' => $is_valid, 'view' => $view));
 }

 public function getDataJabatanId($params) {
  $data = Modules::run('database/get', array(
              'table' => 'jabatan',
              'where' => "deleted = 0 and jabatan = '" . trim($params[3]) . "' "
              . "and nama_panjang = '" . trim($params[4]) . "'"
  ));

  $id = 0;
  if (!empty($data)) {
   $data = $data->row_array();
   $id = $data['id'];
  } else {
   $post['jabatan'] = trim($params[3]);
   $post['nama_panjang'] = trim($params[4]);
   $id = Modules::run('database/_insert', 'jabatan', $post);
  }

  return $id;
 }

 public function getDataUltgId($params) {
  $data = Modules::run('database/get', array(
              'table' => 'ultg',
              'where' => "deleted = 0 and nama = '" . trim($params[5]) . "'"
  ));

  $id = 0;
  if (!empty($data)) {
   $data = $data->row_array();
   $id = $data['id'];
  } else {
   $post['nama'] = trim($params[5]);
   $id = Modules::run('database/_insert', 'ultg', $post);
  }

  return $id;
 }

 public function getTipeProduct($tipe) {
  $data = Modules::run('database/get', array(
              'table' => 'tipe_product',
              'where' => "deleted = 0 and tipe = '" . trim($tipe) . "'"
  ));

  $id = 0;
  if (!empty($data)) {
   $data = $data->row_array();
   $id = $data['id'];
  } else {
   $post['tipe'] = trim($tipe);
   $id = Modules::run('database/_insert', 'tipe_product', $post);
  }

  return $id;
 }

 public function getSatuan($satuan, $parent = "") {
  $where = "deleted = 0 and nama_satuan = '" . trim($satuan) . "'";
  if ($parent != "") {
   $where = "deleted = 0 and nama_satuan = '" . trim($satuan) . "' and parent = '" . $parent . "'";
  }
  $data = Modules::run('database/get', array(
              'table' => 'satuan',
              'where' => $where
  ));

//  if($parent != ''){
//   echo "<pre>";
//   echo $this->db->last_query();
//   die;
//  }
  $id = 0;
  if (!empty($data)) {
   $data = $data->row_array();
   $id = $data['id'];
  } else {
   $post['nama_satuan'] = trim($satuan);
   if ($parent != "") {
    $post['parent'] = trim($parent);
   }
   $id = Modules::run('database/_insert', 'satuan', $post);
  }

  return $id;
 }

 public function getChildSatuan($satuan, $parent) {
  $data = Modules::run('database/get', array(
              'table' => 'satuan',
              'where' => "deleted = 0 and parent = '" . $parent . "' and nama_satuan = '" . trim($satuan) . "'"
  ));

  $id = 0;
  if (!empty($data)) {
   $data = $data->row_array();
   $id = $data['id'];
  } else {
   $post['nama_satuan'] = trim($satuan);
   $post['parent'] = $parent;
   $id = Modules::run('database/_insert', 'satuan', $post);
  }

  return $id;
 }

 public function getDataSubUltgId($params, $ultg) {
  $data = Modules::run('database/get', array(
              'table' => 'sub_ultg',
              'where' => "deleted = 0 and ultg = '" . $ultg . "' and sub_ultg = '" . trim($params[6]) . "'"
  ));

  $id = 0;
  if (!empty($data)) {
   $data = $data->row_array();
   $id = $data['id'];
  } else {
   $post['ultg'] = $ultg;
   $post['sub_ultg'] = trim($params[6]);
   $id = Modules::run('database/_insert', 'sub_ultg', $post);
  }

  return $id;
 }

 public function getProductId($params) {
  $data = Modules::run('database/get', array(
              'table' => 'product',
              'where' => "deleted = 0 and product = '" . trim($params[1]) . "'"
  ));

  $id = 0;
  if (!empty($data)) {
   $data = $data->row_array();
   $id = $data['id'];
  } else {
   $post['product'] = trim($params[1]);
   $post['kode_product'] = trim($params[0]);
   $post['tipe_product'] = 28;
   $post['kategori_product'] = 1;
   $id = Modules::run('database/_insert', 'product', $post);
  }

  return $id;
 }

 public function importFilePegawai() {
  $data = json_decode($this->input->post('data'));

  $is_valid = false;
  $result = array();

//  $this->db->trans_begin();
  try {
   if (!empty($data)) {
    $temp_jalur = "";
    $temp_alamat = "";
    foreach ($data as $value) {
     $jabatan_id = $this->getDataJabatanId($value);
     $ultg = $this->getDataUltgId($value);
     $sub_ultg = $this->getDataSubUltgId($value, $ultg);

     $post_insert['jabatan'] = $jabatan_id;
     $post_insert['ultg'] = $ultg;
     $post_insert['sub_ultg'] = $sub_ultg;
     $post_insert['nip'] = trim($value[1]);
     $post_insert['nama'] = trim($value[2]);
     $post_insert['email'] = trim($value[7]);
     $pegawai = Modules::run('database/_insert', 'pegawai', $post_insert);

     $post_user['username'] = trim($value[1]);
     $post_user['password'] = trim($value[1]);
     $post_user['pegawai'] = $pegawai;
     $user = Modules::run('database/_insert', 'user', $post_user);

     $post_fitur['feature'] = 1;
     $post_fitur['user'] = $user;
     Modules::run('database/_insert', 'feature_set', $post_fitur);
     array_push($result, $value);
    }
   }
//   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $is_valid = false;
   $this->db->trans_rollback();
  }
//
//  echo '<pre>';
//  print_r($result);die;
  $content['result'] = $result;
  $view = $this->load->view('table_data', $content, true);
  echo json_encode(array('is_valid' => $is_valid, 'view' => $view));
 }

 public function importFile() {
  $data = json_decode($this->input->post('data'));
  $is_valid = false;
  $result = array();

	// echo '<pre>';
	// print_r($data);die;
  $this->db->trans_begin();
  try {
		$counter = 1;
   if (!empty($data)) {
    foreach ($data as $value) {
     if (trim($value[0]) != '' && is_numeric($value[0])) {
      $insert_probis['nama'] = trim($value[1]);
      $insert_probis['kontak'] = trim($value[7]);
      $insert_probis['address'] = trim($value[9]);
      $insert_probis['kecamatan'] = trim($value[8]);
      $insert_probis['latitude'] = trim($value[2]);
      $insert_probis['longitude'] = trim($value[3]);
      $insert_probis['pendiri'] = trim($value[4]);
      $insert_probis['pengasuh'] = trim($value[5]);
      $insert_probis['tahun_berdiri'] = trim($value[6]);
      $insert_probis['jenis_ponpes'] = trim($value[10]);
      $insert_probis['luas_pondok'] = trim($value[11]);
      $insert_probis['santri_laki'] = trim($value[12]);
      $insert_probis['santri_wati'] = trim($value[13]);
      $insert_probis['ekstrakulikuler'] = trim($value[14]);
      $insert_probis['materi_ponpes'] = trim($value[15]);

      $gedung = Modules::run('database/_insert', "ponpes", $insert_probis);

      // if (trim($value[11]) != '') {
      //  $data_fasilitas = explode('[&&]', trim($value[11]));
      //  foreach ($data_fasilitas as $v_s) {
      //   $fas['gedung'] = $gedung;
      //   $fas['fasilitas'] = $v_s;
      //   Modules::run('database/_insert', 'gedung_fasilitas', $fas);
      //  }
      // }

      // if (trim($value[12]) != '') {
      //  $data_fasilitas = explode('[&&]', trim($value[12]));
      //  foreach ($data_fasilitas as $v_s) {
        $gd['ponpes'] = $gedung;
        // $gd['file'] = $v_s;
        $gd['file'] = $counter.'.jpg';
				Modules::run('database/_insert', 'ponpes_picture', $gd);
				$counter +=1;
      //  }
      // }
      array_push($result, $value);
     }
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $is_valid = false;
   $this->db->trans_rollback();
  }

  $content['result'] = $result;
  $view = $this->load->view('table_data', $content, true);
  echo json_encode(array('is_valid' => $is_valid, 'view' => $view));
 }

 public function checkProbisIsExist($nama_probis) {
  $data = Modules::run('database/get', array(
              'table' => 'probis',
              'where' => "deleted = 0 and nama_probis = '" . $nama_probis . "'"
  ));


  if (!empty($data)) {
   return 1;
  }

  return 0;
 }

 public function importFilePOS() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);die;
  $is_valid = false;
  $result = array();

  $this->db->trans_begin();
  try {
   if (!empty($data)) {
    $temp_product = "";
    // echo '<pre>';
    // print_r($data);die;
    for ($i = 0; $i < count($data); $i++) {
     $value = $data[$i];
//     echo '<pre>';
//     print_r($value);die;
     $product_id = $this->getProductId($value);

     $parent_stauan = trim($value[4]);
     $qty = trim($value[5]);
     //  echo trim($value[1]);die;
     if ($temp_product != trim($value[1])) {
      $satuan_id = $this->getSatuan($parent_stauan);

      //insert product_satuan
      $post_pro_sa['product'] = $product_id;
      $post_pro_sa['satuan'] = $satuan_id;
      $post_pro_sa['qty'] = $qty == '' ? 1 : $qty;
      $post_pro_sa['harga_beli'] = trim(str_replace(',', '', $value[7]));
      $post_pro_sa['harga'] = trim(str_replace(',', '', $value[8]));
      $product_satuan = Modules::run('database/_insert', 'product_satuan', $post_pro_sa);

      //insert product stock
      $post_pro_stok['product_satuan'] = $product_satuan;
      $post_pro_stok['gudang'] = '1';
      $post_pro_stok['rak'] = '1';
      $post_pro_stok['stock'] = trim($value[3]) == '' ? 0 : trim($value[3]);
      Modules::run('database/_insert', 'product_stock', $post_pro_stok);
     } else {
      //  echo $data[$i - 1][4].' dan '.$parent_stauan;die;			 
      $par_stauan = $this->getSatuan(trim($data[$i - 1][4]));
      $satuan_id = $this->getSatuan($parent_stauan, $par_stauan);

      $post_pro_sa = array();
      $post_pro_sa['product'] = $product_id;
      $post_pro_sa['satuan'] = $satuan_id;
      $post_pro_sa['qty'] = $qty == '' ? 1 : $qty;
      $post_pro_sa['harga_beli'] = trim(str_replace(',', '', $value[7]));
      $post_pro_sa['harga'] = trim(str_replace(',', '', $value[8]));
      $product_satuan = Modules::run('database/_insert', 'product_satuan', $post_pro_sa);

      //insert product stock
      $post_pro_stok = array();
      $post_pro_stok['product_satuan'] = $product_satuan;
      $post_pro_stok['gudang'] = '1';
      $post_pro_stok['rak'] = '1';
      $post_pro_stok['stock'] = trim($value[3]) == '' ? 0 : trim($value[3]);
      Modules::run('database/_insert', 'product_stock', $post_pro_stok);
     }

     $temp_product = trim($value[1]);
     array_push($result, $value);
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $is_valid = false;
   $this->db->trans_rollback();
  }

  $content['result'] = $result;
  $view = $this->load->view('table_data', $content, true);
  echo json_encode(array('is_valid' => $is_valid, 'view' => $view));
 }

 public function importFilePembeli() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);die;
  $is_valid = false;
  $result = array();

  $this->db->trans_begin();
  try {
   if (!empty($data)) {
    foreach ($data as $value) {
     $post_cus['nama'] = $value[1];
     $post_cus['pembeli_kategori'] = 1;
     $post_cus['alamat'] = $value[4] . ', ' . $value[3] . ', ' . $value[2];

     Modules::run('database/_insert', 'pembeli', $post_cus);

     array_push($result, $value);
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $is_valid = false;
   $this->db->trans_rollback();
  }

  $content['result'] = $result;
  $view = $this->load->view('table_data', $content, true);
  echo json_encode(array('is_valid' => $is_valid, 'view' => $view));
 }

 public function importFileProbis() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);die;
  $is_valid = false;
  $result = array();

  $this->db->trans_begin();
  try {
   if (!empty($data)) {
    foreach ($data as $value) {
     if (trim($value[0]) != '' && is_numeric($value[0])) {
//      echo '<pre>';
//      print_r($value);die;
      $insert_probis['upt'] = $this->getDataUptId(trim($value[3]));
      $insert_probis['nama_probis'] = trim($value[4]);
      if (trim($value[2] != '')) {
       $insert_probis['no_probis'] = trim($value[2]);
      } else {
       $insert_probis['no_probis'] = Modules::run('no_generator/generateNoProbis');
      }
      $insert_probis['user'] = $this->session->userdata('user_id');

      if (trim($value[7]) != '') {
       $insert_probis['tgl_update'] = date('Y-m-d', strtotime(trim($value[7])));
      }

      if (trim($value[6]) != '') {
       $insert_probis['tahun'] = trim($value[6]);
      }
      if (trim($value[8]) != '') {
       $insert_probis['edisi'] = trim($value[8]);
      }
      if (trim($value[9]) != '') {
       $insert_probis['revisi'] = trim($value[9]);
      }
//      echo '<pre>';
//      print_r($insert_probis);die;
      $nama_probis = trim($value[4]);
      $is_exist_probis = $this->checkProbisIsExist($nama_probis);
      if ($is_exist_probis == 0) {
       $probis = Modules::run('database/_insert', $this->getTableName(), $insert_probis);
       $this->insertKeywordProbis($probis, $value);

       $this->insertPathProbis($probis, $value);
       //insert status
       if (trim($value[10]) == '') {
        $value[10] = 'ONCOMING';
       }
       if (trim($value[10]) == 'proses') {
        $value[10] = 'PROGRESS';
       }
       if (trim($value[10]) == 'proses revisi') {
        $value[10] = 'REVISION ON PROGRESS';
       }
       $status['status'] = strtoupper(trim($value[10]));
       $status['probis'] = $probis;

       if (trim($value[12]) != '') {
        $status['keterangan'] = trim($value[12]);
       }
       Modules::run('database/_insert', 'probis_status', $status);

       array_push($result, $value);
      }
     }
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $is_valid = false;
   $this->db->trans_rollback();
  }

  $content['result'] = $result;
  $view = $this->load->view('table_data', $content, true);
  echo json_encode(array('is_valid' => $is_valid, 'view' => $view));
 }

 public function insertPathProbis($probis, $value) {
  $data_path = explode('.', $value[1]);
  if (!empty($data_path)) {
   $insert['probis'] = $probis;
   $insert['parent'] = isset($data_path[0]) ? trim($data_path[0]) : 0;
   $insert['child_first'] = isset($data_path[1]) ? trim($data_path[1]) : 0;
   $insert['child_second'] = isset($data_path[2]) ? trim($data_path[2]) : 0;
   $insert['child_third'] = isset($data_path[3]) ? trim($data_path[3]) : 0;
   Modules::run('database/_insert', 'path_probis', $insert);
  }
 }

 public function insertKeywordProbis($probis, $value) {
  $data_keyword = explode('[&&]', $value[5]);
  if (!empty($data_keyword)) {
   foreach ($data_keyword as $v) {
    $insert['probis'] = $probis;
    $insert['keyword'] = trim($v);
    Modules::run('database/_insert', 'keyword_probis', $insert);
   }
  }
 }

}

<table class="table color-bordered-table primary-bordered-table">
 <thead>
  <tr class="bg-success text-white">
   <th class="font-12">No</th>
   <th class="font-12">Path</th>
   <th class="font-12">No Probis</th>
   <th class="font-12">UPT</th>
   <th class="font-12">Probis</th>
   <th class="font-12">Status</th>
  </tr>
 </thead>
 <tbody>
  <?php if (!empty($result)) { ?>
   <?php $no = 1 ?>
   <?php foreach ($result as $value) { ?>
    <tr>
     <td class="font-10"><?php echo $no++ ?></td>
     <td class="font-10"><?php echo $value[1] ?></td>
     <td class="font-10"><?php echo $value[2] ?></td>
     <td class="font-10"><?php echo $value[3] ?></td>
     <td class="font-10"><?php echo $value[4] ?></td>
     <td class="font-10"><?php echo 'Uploaded' ?></td>
    </tr>
   <?php } ?>
  <?php } else { ?>
   <tr>
    <td class="text-center font-10" colspan="5">Tidak Ada Data yang Diimport</td>
   </tr>
  <?php } ?>
 </tbody>
</table>
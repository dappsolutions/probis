<?php

class Login extends MX_Controller {

 public function __construct() {
  parent::__construct();  
 }

 public function getModuleName() {
  return 'login';
 }

 public function getHeaderJSandCSS() {
  $data = array(
  '<script src="' . base_url() . 'assets/js/controllers/login.js"></script>',
  );

  return $data;
 }

 public function index() {
  $data['view_file'] = 'index';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Login";
  $data['title_content'] = 'Login';
  echo $this->load->view('index', $data, true);
 }

 public function getDataUserDb($username, $password) {
  $data = Modules::run('database/get', array(
  'table' => 'user u',
  'field'=> array('u.*', 'pv.hak_akses', 'ut.id as upt_id', 'ut.nama_upt'),
  'join' => array(
  array('priveledge pv', 'u.priveledge = pv.id'),
  array('pegawai p', 'u.id = p.user', 'left'),
  array('upt ut', 'p.upt = ut.id', 'left'),
  ),
  'where' => array(
  'u.username' => $username,
  'u.password' => $password,
  'u.deleted' => 0
  )
  ));
  
  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
  }

  
  return $result;
 }

 public function getDataPegawai($username, $password) {
  $data = Modules::run('database/get', array(
  'table' => 'user u',
  'field' => array('u.*', 'pv.priveledge as hak_akses'),
  'join' => array(
  array('priveledge pv', 'u.priveledge = pv.id'),
  ),
  'where' => array(
  'u.username' => $username,
  'u.password' => $password,
  'u.is_active' => true
  )
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
  }

  return $result;
 }

 public function getDataUser($username, $password) {
  $data = $this->getDataUserDb($username, $password);
//  echo '<pre>';
//  print_r($data);
//  die;
  $result = array();
  if (!empty($data)) {
   $result = $data;
  }
//  else{
//   $data = $this->getDataPegawai($username, $password);
//   if(!empty($data)){
//    $result = $data;
//   }   
//  }

  return $result;
 }

 public function sign_in() {
  $username = $this->input->post('username');
  $password = $this->input->post('password');

  $is_valid = false;
  try {
   $data = $this->getDataUser($username, $password);
//   echo '<pre>';
//   echo $this->db->last_query();
//   die;
   if (!empty($data)) {
    $is_valid = true;
   }
  } catch (Exception $exc) {
   $is_valid = false;
  }

  $hak_akses = '';
  if ($is_valid) {
   $this->setSessionData($data);
   $hak_akses = $data['hak_akses'];
  }
  echo json_encode(array('is_valid' => $is_valid, 'hak_akses' => $hak_akses));
 }

 public function setSessionData($data) {
  $session['user_id'] = $data['id'];
  $session['username'] = $data['username'];
  $session['hak_akses'] = $data['hak_akses'];
  $session['priveledge'] = $data['priveledge'];
  $session['upt_id'] = $data['upt_id'];
  $session['nama_upt'] = $data['nama_upt'];
  $this->session->set_userdata($session);
 }

 public function sign_out() {
  $this->session->sess_destroy();
  redirect(base_url());
 }

}

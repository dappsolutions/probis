<?php

//

class Imporhukum extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $upt;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'imporhukum';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/imporhukum.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'produk';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Import Data";
  $data['title_content'] = 'Import Data';
  echo Modules::run('template', $data);
 }

 public function getLemariId($value) {
  $data = Modules::run('database/get', array(
              'table' => 'lemari ut',
              'field' => array('ut.*'),
              'where' => "ut.deleted = 0 and ut.kode_lemari = '" . trim($value[3]) . "'"
  ));

  $id = 0;
  if (!empty($data)) {
   $data = $data->row_array();
   $id = $data['id'];
  } else {
   $data_upt['kode_lemari'] = trim($value[3]);
   $id = Modules::run('database/_insert', 'lemari', $data_upt);
  }


  return $id;
 }
 
 public function getRuangId($value) {
  $data = Modules::run('database/get', array(
              'table' => 'ruang ut',
              'field' => array('ut.*'),
              'where' => "ut.deleted = 0 and ut.kode_ruang = '" . trim($value[4]) . "'"
  ));

  $id = 0;
  if (!empty($data)) {
   $data = $data->row_array();
   $id = $data['id'];
  } else {
   $data_upt['kode_ruang'] = trim($value[4]);
   $id = Modules::run('database/_insert', 'ruang', $data_upt);
  }


  return $id;
 }
 
 public function getRakId($value) {
  $data = Modules::run('database/get', array(
              'table' => 'rak ut',
              'field' => array('ut.*'),
              'where' => "ut.deleted = 0 and ut.kode_rak = '" . trim($value[5]) . "'"
  ));

  $id = 0;
  if (!empty($data)) {
   $data = $data->row_array();
   $id = $data['id'];
  } else {
   $data_upt['kode_rak'] = trim($value[5]);
   $id = Modules::run('database/_insert', 'rak', $data_upt);
  }


  return $id;
 }
 
 public function getJenisProdukId($value) {
  $data = Modules::run('database/get', array(
              'table' => 'jenis_produk ut',
              'field' => array('ut.*'),
              'where' => "ut.deleted = 0 and ut.kode_jenis = '" . trim($value[6]) . "'"
  ));

  $id = 0;
  if (!empty($data)) {
   $data = $data->row_array();
   $id = $data['id'];
  } else {
   $data_upt['kode_jenis'] = trim($value[6]);
   $id = Modules::run('database/_insert', 'jenis_produk', $data_upt);
  }


  return $id;
 }
 
 public function getTahunId($value) {
  $data = Modules::run('database/get', array(
              'table' => 'tahun ut',
              'field' => array('ut.*'),
              'where' => "ut.tahun = '" . trim($value[13]) . "'"
  ));

  $id = 0;
  if (!empty($data)) {
   $data = $data->row_array();
   $id = $data['id'];
  } else {
   $data_upt['tahun'] = trim($value[13]);
   $id = Modules::run('database/_insert', 'tahun', $data_upt);
  }


  return $id;
 }

 public function importFile() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);die;
  $is_valid = false;
  $result = array();

  $this->db->trans_begin();
  try {
   if (!empty($data)) {
    // echo '<pre>';
    // print_r($data);die;
    for ($i = 0; $i < count($data); $i++) {
     $value = $data[$i];

     $lemari = $this->getLemariId($value);
     $ruang = $this->getRuangId($value);
     $rak = $this->getRakId($value);
     $jenis_produk = $this->getJenisProdukId($value);
     $tahun = $this->getTahunId($value);
     $post_data['no_arsip'] = trim($value[1]);
     $post_data['no_produk'] = trim($value[2]);
     $post_data['lemari'] = $lemari;
     $post_data['ruang'] = $ruang;
     $post_data['rak'] = $rak;
     $post_data['jenis_produk'] = $jenis_produk;
     if(trim($value[7]) != ''){
      $post_data['tgl_penetapan'] = trim($value[7]);
     }     
     $post_data['judul'] = trim($value[8]);
     $post_data['menimbang'] = trim($value[9]);
     $post_data['mengingat'] = trim($value[10]);
     $post_data['memperhatikan'] = trim($value[11]);
     $post_data['menetapkan'] = trim($value[12]);
     $post_data['tahun'] = $tahun;
     $post_data['status'] = trim($value[14]) == 'AKTIF' ? 1 : 2;
     $post_data['file'] = trim($value[15]);
     Modules::run('database/_insert', 'produk', $post_data);
     array_push($result, $value);
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $is_valid = false;
   $this->db->trans_rollback();
  }

  $content['result'] = $result;
  $view = $this->load->view('table_data', $content, true);
  echo json_encode(array('is_valid' => $is_valid, 'view' => $view));
 }

}

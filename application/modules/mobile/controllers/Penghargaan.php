<?php

class Penghargaan extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function getModuleName()
  {
    return 'penghargaan';
  }

  public function getDataPenghargaanTaruna()
  {
    $taruna = $this->input->post('taruna');
    $data = Modules::run('database/get', array(
      'table' => 'penghargaan ph',
      'field' => "sum(p.skor) as jumlah",
      'join' => array(
        array('taruna t', 'ph.taruna = t.id'),
        array('prestasi p', 'ph.prestasi = p.id'),
      ),
      'where' => array(
        'ph.taruna' => $taruna,
      )
    ));

    $jumlah = 0;
    if (!empty($data)) {
      $jumlah = $data->row_array()['jumlah'];
      $jumlah = $jumlah == null ? 0: $jumlah;
    }


    echo json_encode(array(
      'jumlah'=> $jumlah
    ));
  }
  
  public function getDataPenghargaan()
  {
    $taruna = $this->input->post('taruna');
    $data = Modules::run('database/get', array(
      'table' => 'penghargaan ph',
      'field' => array('ph.id', 'p.prestasi', 'p.skor', 'ph.tanggal', 't.nama as nama_taruna', 'ph.semester'),
      'join' => array(
        array('taruna t', 'ph.taruna = t.id'),
        array('prestasi p', 'ph.prestasi = p.id'),
      ),
      'where' => array(
        'ph.taruna' => $taruna,
      ),
      'orderby'=> "ph.id desc"
    ));

    $result = array();
    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        $value["tanggal"] = Modules::run("helper/getIndoDate", $value["tanggal"]);
        array_push($result, $value);
      }
    }


    echo json_encode(array(
      'data'=> $result
    ));
  }
}

<?php

class Izin extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function getModuleName()
  {
    return 'izin';
  }
  
  public function getDataIzinTaruna()
  {
    $taruna = $this->input->post('taruna');
    $data = Modules::run('database/get', array(
      'table' => 'jadwal_luar jl',
      'field' => array('jl.id', 't.nama as nama_taruna', 'jl.semester', 
      'jl.tanggal_awal', 'jl.tanggal_akhir', 'jl.jam', 'jjl.jenis', 'jl.keterangan'),
      'join' => array(
        array('taruna t', 'jl.taruna = t.id'),
        array('jenis_jadwal_luar jjl', 'jl.jenis_jadwal_luar = jjl.id'),
      ),
      'where' => array(
        'jl.taruna' => $taruna,
      ),
      'orderby'=> "jl.id desc"
    ));

    $result = array();
    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        $value["tanggal_awal"] = Modules::run("helper/getIndoDate", $value["tanggal_awal"]);
        $value["tanggal_akhir"] = Modules::run("helper/getIndoDate", $value["tanggal_akhir"]);
        $value['tanggal'] = $value['tanggal_awal'].' - '.$value['tanggal_akhir'];        
        array_push($result, $value);
      }
    }


    echo json_encode(array(
      'data'=> $result
    ));
  }
}

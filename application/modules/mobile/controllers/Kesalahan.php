<?php

class Kesalahan extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function getModuleName()
  {
    return 'kesalahan';
  }

  public function getDataKesalahanTaruna()
  {
    $taruna = $this->input->post('taruna');
    $data = Modules::run('database/get', array(
      'table' => 'kesalahan k',
      'field' => "sum(p.point) as jumlah",
      'join' => array(
        array('taruna t', 'k.taruna = t.id'),
        array('pelanggaran p', 'k.pelanggaran = p.id'),
      ),
      'where' => array(
        'k.taruna' => $taruna,
      )
    ));

    $jumlah = 0;
    if (!empty($data)) {
      $jumlah = $data->row_array()['jumlah'];
      $jumlah = $jumlah == null ? 0: $jumlah;
    }


    echo json_encode(array(
      'jumlah'=> $jumlah
    ));
  }
  
  public function getDataKesalahan()
  {
    $taruna = $this->input->post('taruna');
    $data = Modules::run('database/get', array(
      'table' => 'kesalahan k',
      'field' => array('k.id', 'p.jenis', 'p.point', 'k.tanggal', 't.nama as nama_taruna', 'k.semester'),
      'join' => array(
        array('taruna t', 'k.taruna = t.id'),
        array('pelanggaran p', 'k.pelanggaran = p.id'),
      ),
      'where' => array(
        'k.taruna' => $taruna,
      ),
      'orderby'=> 'k.id desc'
    ));

    $result = array();
    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        $value["tanggal"] = Modules::run("helper/getIndoDate", $value["tanggal"]);
        array_push($result, $value);
      }
    }


    echo json_encode(array(
      'data'=> $result
    ));
  }
}

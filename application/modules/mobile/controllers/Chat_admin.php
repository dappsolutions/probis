<?php

class Chat_admin extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function getModuleName()
  {
    return 'chat_admin';
  }

  public function getDataChat()
  {
    $user = $this->input->post('user');
    $data = Modules::run('database/get', array(
      'table' => 'chat_admin ca',
      'field' => array('ca.*'),
      'where' => "ca.from = '".$user."' or ca.to = '".$user."'"
    ));

    $result = array();
    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        array_push($result, $value);
      }
    }


    echo json_encode(array(
      'data'=> $result
    ));
  }

  public function sendMessage(){
    $post_data["message"] = $this->input->post("message");
    $post_data["from"] = $this->input->post("from");
    $post_data["to"] = 1;

    $is_valid = false;
    $this->db->trans_begin();
    try {
      Modules::run("database/_insert", 'chat_admin', $post_data);
      $this->db->trans_commit();
      $is_valid = true;
    } catch (\Throwable $th) {
      $is_valid = false;
      $this->db->trans_rollback();
    }

    echo json_encode(array(
      "is_valid"=> $is_valid
    ));
  }
}

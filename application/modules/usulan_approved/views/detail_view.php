<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <!--.row-->
 <div class="row">
  <div class="col-md-12">
   <div class="panel panel-primary">
    <div class="panel-heading"> <?php echo $title_content ?></div>
    <div class="panel-wrapper collapse in" aria-expanded="true">
     <div class="panel-body">
      <form action="#" class="form-horizontal">
       <div class="form-body">
        <h3 class="box-title">Usulan <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">
        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            No Usulan
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $no_probis ?>
           </div>
          </div>         
         </div>         
        </div>
        <br/>

        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Nama Usulan
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $nama_probis ?>
           </div>
          </div>
         </div>
        </div>
        <br/>

        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            File Usulan (.xls)
           </div>
           <div class="col-md-9 text-primary text-left">
            <button><a href="<?php echo base_url() . 'files/berkas/probis/' . $file_excel ?>"><?php echo $file_excel ?></a></button>
           </div>
          </div>         
         </div>         
        </div>
        <br/>

        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            File Usulan (.pdf)
           </div>
           <div class="col-md-9 text-primary text-left">
            <button onclick="UsulanApproved.showLogo(this, event)"><?php echo $file_pdf ?></button>
           </div>
          </div>
         </div>
        </div>
       </div>
       <br/>

       <?php if (isset($list_status)) { ?>
        <?php if (!empty($list_status)) { ?>
         <div class="row">
          <div class="col-md-12">
           <h3 class="box-title">Riwayat Dokumen Usulan <i class="fa fa-arrow-down"></i></h3>
           <hr class="m-t-0 m-b-40">
           <ul class="timeline">
            <?php $no = 1 ?>
            <?php foreach ($list_status as $value) { ?>          
             <?php $class = $no % 2 == 0 ? 'timeline-inverted' : '' ?>
             <li class="<?php echo $class ?> ">
              <div class="timeline-badge success"><img class="img-responsive" alt="user" src="<?php echo base_url() . 'assets/images/users/images.png' ?>"> </div>
              <div class="timeline-panel">
               <div class="timeline-heading">
                <h4 class="timeline-title"><?php echo $value['status'] ?></h4>
                <p><small class="text-muted"><i class="fa fa-clock-o"></i> <?php echo $value['createddate'] ?></small> </p>
               </div>
               <div class="timeline-body">
                <p><?php 'Oleh ' ?><label class="label label-info"><?php echo $value['nama_pegawai'] ?></label></p>
               </div>
              </div>
             </li>
             <?php $no += 1; ?>
            <?php } ?>
           </ul>
          </div>
         </div>
        <?php } ?>
       <?php } ?>       

       <?php if (isset($tembusan)) { ?>
        <?php if (!empty($tembusan)) { ?>
         <div class="row">
          <div class="col-md-12">
           <h3 class="box-title">Tembusan Probis <i class="fa fa-arrow-down"></i></h3>
           <hr class="m-t-0 m-b-40">
           <?php $no = 1 ?>                  
           <div class="table-responsive">
            <table class="table color-bordered-table primary-bordered-table" id="tb_tembusan">
             <thead>
              <tr class="">
               <th class="font-10">No</th>
               <th class="font-10">NIP</th>
               <th class="font-10">Nama Pegawai</th>
              </tr>
             </thead>
             <tbody>
              <?php if (!empty($tembusan)) { ?>
               <?php $no = 1; ?>
               <?php foreach ($tembusan as $value) { ?>   
                <tr id="<?php echo $value['id'] ?>">
                 <td class='font-10'><?php echo $no++ ?></td>
                 <td class='font-10'><?php echo $value['nip'] ?></td>
                 <td class='font-10'><?php echo $value['nama_pegawai'] ?></td>
                </tr>
               <?php } ?>
              <?php } else { ?>
               <tr>
                <td class="text-center font-12" colspan="8">Tidak Ada Data Ditemukan</td>
               </tr>
              <?php } ?>         
             </tbody>
            </table>
           </div>
          </div>
         </div>
        <?php } ?>
       <?php } ?>       

       
       <div class="form-actions">
        <div class="row">
         <div class="col-md-12">
          <div class="row">
           <div class="col-md-offset-3 col-md-9 text-right">
            <button type="button" class="btn btn-default" onclick="UsulanApproved.back()">Kembali</button>
           </div>
          </div>
         </div>
         <div class="col-md-6"> </div>
        </div>
       </div>
      </form>
     </div>
    </div>
   </div>
  </div>
 </div>
 <!--./row--> 
</div>

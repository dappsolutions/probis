<?php

class Pegawai extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $upt;
 public $akses;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
  $this->upt = $this->session->userdata('upt_id');
  $this->akses = $this->session->userdata('hak_akses');
 }

 public function getModuleName() {
  return 'pegawai';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/pegawai.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'pegawai';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Pegawai";
  $data['title_content'] = 'Data Pegawai';
  $content = $this->getDataPegawai();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataPegawai($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.nip', $keyword),
       array('t.nama_pegawai', $keyword),
       array('u.nama_upt', $keyword),
       array('pv.hak_akses', $keyword),
   );
  }

  $where = "t.deleted = 0 and u.id = '" . $this->upt . "'";
  if ($this->akses == 'Superadmin') {
   $where = "t.deleted = 0";
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*', 'u.nama_upt', 'pv.hak_akses'),
              'join' => array(
                  array('upt u', 'u.id = t.upt'),
                  array('user us', 't.user = us.id'),
                  array('priveledge pv', 'us.priveledge = pv.id'),
              ),
              'where' => $where
  ));

  return $total;
 }

 public function getDataPegawai($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.nip', $keyword),
       array('t.nama_pegawai', $keyword),
       array('u.nama_upt', $keyword),
       array('pv.hak_akses', $keyword),
   );
  }

  $where = "t.deleted = 0 and u.id = '" . $this->upt . "'";
  if ($this->akses == 'Superadmin') {
   $where = "t.deleted = 0";
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*', 'u.nama_upt', 'pv.hak_akses', 'us.password'),
              'join' => array(
                  array('upt u', 'u.id = t.upt'),
                  array('user us', 't.user = us.id'),
                  array('priveledge pv', 'us.priveledge = pv.id'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => $where
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataPegawai($keyword)
  );
 }

 public function getDetailDataPegawai($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*',
                  'u.username', 'u.password',
                  'p.hak_akses', 'ut.nama_upt',
                  'u.priveledge'),
              'join' => array(
                  array('user u', 't.user = u.id'),
                  array('priveledge p', 'u.priveledge = p.id'),
                  array('upt ut', 't.upt = ut.id'),
              ),
              'where' => "t.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function getListUpt() {
  $data = Modules::run('database/get', array(
              'table' => 'upt',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getListHakAkses() {
  $data = Modules::run('database/get', array(
              'table' => 'priveledge',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    if ($value['id'] != 1) {
     array_push($result, $value);
    }
   }
  }

  return $result;
 }

 public function getListHubunganWali() {
  $data = Modules::run('database/get', array(
              'table' => 'hubungan_wali',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getListSemester() {
  $data = Modules::run('database/get', array(
              'table' => 'semester',
              'where' => 'deleted = 0'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Pegawai";
  $data['title_content'] = 'Tambah Pegawai';
  $data['list_upt'] = $this->getListUpt();
  $data['list_hak_akses'] = $this->getListHakAkses();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataPegawai($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Pegawai";
  $data['title_content'] = 'Ubah Pegawai';
  $data['list_upt'] = $this->getListUpt();
  $data['list_hak_akses'] = $this->getListHakAkses();
  echo Modules::run('template', $data);
 }

 public function getDetailDataOrganisasi($id) {
  $data = Modules::run('database/get', array(
              'table' => 'masjid_has_organisasi mho',
              'field' => array('mho.*', 'o.nama_organisasi'),
              'join' => array(
                  array('organisasi o', 'mho.organisasi = o.id')
              ),
              'where' => "mho.deleted = 0 and mho.masjid = '" . $id . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getDetailPengurusPegawai($masjid) {
  $data = Modules::run('database/get', array(
              'table' => 'masjid_has_pengurus mhp',
              'field' => array('mhp.*', 'j.jabatan as nama_jabatan'),
              'join' => array(
                  array('jabatan j', 'mhp.jabatan = j.id')
              ),
              'where' => array('mhp.deleted' => 0, 'mhp.masjid' => $masjid)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataWaliMurid($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_wali_murid thw',
              'field' => array('thw.*', 'hw.hubungan'),
              'join' => array(
                  array('hubungan_wali hw', 'hw.id = thw.hubungan_wali')
              ),
              'where' => "thw.deleted = 0 and thw.taruna = '" . $taruna . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataAgama($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_agama ta',
              'field' => array('ta.*'),
              'where' => "ta.deleted = 0 and ta.taruna = '" . $taruna . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['status'] = $value['status'] == 1 ? 'Aktif' : 'Tidak Aktif';
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataKesehatan($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_kesehatan ta',
              'field' => array('ta.*'),
              'where' => "ta.deleted = 0 and ta.taruna = '" . $taruna . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['status'] = $value['status'] == 1 ? 'Aktif' : 'Tidak Aktif';
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataAkademik($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_akademik ta',
              'field' => array('ta.*', 's.semester as semester_taruna',
                  'p.prodi as program_studi'),
              'join' => array(
                  array('prodi p', 'ta.prodi = p.id'),
                  array('semester s', 'ta.semester = s.id'),
              ),
              'where' => "ta.deleted = 0 and ta.taruna = '" . $taruna . "'"
  ));


  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['status'] = $value['status'] == 1 ? 'Aktif' : 'Tidak Aktif';
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function detail($id) {
  $data = $this->getDetailDataPegawai($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Pegawai";
  $data['title_content'] = "Detail Pegawai";
  echo Modules::run('template', $data);
 }

 function generateRandomString($length = 6) {
  return substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);
 }

 public function getPostDataUser($value) {
  $data['username'] = $value->nip;
  $data['password'] = $this->generateRandomString();
  $data['priveledge'] = $value->hak_akses;
  return $data;
 }

 public function getPostDataPegawai($value, $user) {
  $data['nip'] = $value->nip;
  $data['nama_pegawai'] = $value->nama;
  $data['upt'] = $value->upt;
  $data['email'] = $value->email;
  $data['user'] = $user;
  return $data;
 }

 public function getPostDataPegawaiEdit($value) {
  $data['nip'] = $value->nip;
  $data['nama_pegawai'] = $value->nama;
  $data['email'] = $value->email;
  return $data;
 }

 public function getDataUser($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName(),
              'where' => array('id' => $id)
  ));

  return $data->row_array();
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;

  $this->db->trans_begin();
  try {
   if ($id == '') {
    $post_user = $this->getPostDataUser($data->pegawai);
    $user = Modules::run('database/_insert', 'user', $post_user);
    $post = $this->getPostDataPegawai($data->pegawai, $user);
    $id = Modules::run('database/_insert', $this->getTableName(), $post);
   } else {
    //update
    $pegawai = $this->getDataUser($id);
    Modules::run('database/_update', 'user', array('priveledge' => $data->pegawai->hak_akses), array('id' => $pegawai['user']));
    $post = $this->getPostDataPegawaiEdit($data->pegawai);
    Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Pegawai";
  $data['title_content'] = 'Data Pegawai';
  $content = $this->getDataPegawai($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/taruna/';
  $config['allowed_types'] = 'png|jpg';
  $config['max_size'] = '1000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);
  $this->upload->do_upload($name_of_field);
 }

 public function showLogo() {
  $foto = str_replace(' ', '_', $this->input->post('foto'));
  $data['foto'] = $foto;
  echo $this->load->view('foto', $data, true);
 }

}

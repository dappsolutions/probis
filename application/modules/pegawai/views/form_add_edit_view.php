<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <!--.row-->
 <div class="row">
  <div class="col-md-12">
   <div class="panel panel-primary">
    <div class="panel-heading"> <?php echo $title_content ?></div>
    <div class="panel-wrapper collapse in" aria-expanded="true">
     <div class="panel-body">
      <form action="#" class="form-horizontal">
       <div class="form-body">
        <h3 class="box-title">Pegawai <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">
        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">NIP</label>
           <div class="col-md-9">
            <input type="text" id='nip' maxlength="30" class="form-control required" error='NIP Pegawai' placeholder="NIP Pegawai" value="<?php echo isset($nip) ? $nip : '' ?>">
           </div>
          </div>
         </div>
         <!--/span-->
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Nama</label>
           <div class="col-md-9">
            <input type="text" id='nama' class="form-control required" placeholder="Nama Lengkap" error='Nama Pegawai' value="<?php echo isset($nama_pegawai) ? $nama_pegawai : '' ?>">
           </div>
          </div>
         </div>
         <!--/span-->
        </div>       

        <!--/row-->
        <div class="row">
         <!--/span-->
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">UPT</label>
           <div class="col-md-9">
            <select id='upt' class="form-control required" error='UPT'>
             <?php if (!empty($list_upt)) { ?>
              <?php foreach ($list_upt as $value) { ?>
               <?php $selected = '' ?>
               <?php if (isset($upt)) { ?>
                <?php $selected = $upt == $value['id'] ? 'selected' : '' ?>
               <?php } ?>
               <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama_upt'] ?></option>
              <?php } ?>
             <?php } ?>
            </select>
           </div>
          </div>
         </div>         
         <!--/span-->
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Hak Akses</label>
           <div class="col-md-9">
            <select id='hak_akses' class="form-control required" error='Hak Akses'>
             <?php if (!empty($list_hak_akses)) { ?>
              <?php foreach ($list_hak_akses as $value) { ?>
               <?php $selected = '' ?>
               <?php if (isset($priveledge)) { ?>
                <?php $selected = $priveledge == $value['id'] ? 'selected' : '' ?>
               <?php } ?>
               <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['hak_akses'] ?></option>
              <?php } ?>
             <?php } ?>
            </select>
           </div>
          </div>
         </div>         
        </div>                
       </div>

       <div class="row">
        <div class="col-md-6">
         <div class="form-group">
          <label class="control-label col-md-3">Email</label>
          <div class="col-md-9">
           <input type="email" id='email' class="form-control" placeholder="Email" error='Email' value="<?php echo isset($email) ? $email : '' ?>">
          </div>
         </div>
        </div>
       </div>

       <div class="form-actions">
        <div class="row">
         <div class="col-md-12">
          <div class="row">
           <div class="col-md-offset-3 col-md-9 text-right">
            <button type="submit" class="btn btn-success" onclick="Pegawai.simpan('<?php echo isset($id) ? $id : '' ?>', event)">Submit</button>
            <button type="button" class="btn btn-default" onclick="Pegawai.back()">Batal</button>
           </div>
          </div>
         </div>
         <div class="col-md-6"> </div>
        </div>
       </div>
      </form>
     </div>
    </div>
   </div>
  </div>
 </div>
 <!--./row--> 
</div>

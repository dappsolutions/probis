<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <!--.row-->
 <div class="row">
  <div class="col-md-12">
   <div class="panel panel-primary">
    <div class="panel-heading"> <?php echo $title_content ?></div>
    <div class="panel-wrapper collapse in" aria-expanded="true">
     <div class="panel-body">
      <form action="#" class="form-horizontal">
       <div class="form-body">
        <h3 class="box-title">Identitas Pegawai <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">
        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            NIP
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $nip ?>
           </div>
          </div>         
         </div>

         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Nama Pegawai
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $nama_pegawai ?>
           </div>
          </div>
         </div>
        </div>
        <br/>

        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            UPT
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $nama_upt ?>
           </div>
          </div>         
         </div>

         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Username
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $username ?>
           </div>
          </div>
         </div>
        </div>
        <br/>

        <!--/row-->
        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Password
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $password ?>
           </div>
          </div>         
         </div>

         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Hak Akses
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $hak_akses ?>
           </div>
          </div>         
         </div>
        </div>
        <br/>

        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Email
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $email ?>
           </div>
          </div>         
         </div>
        </div>
       </div>


       <div class="form-actions">
        <div class="row">
         <div class="col-md-12">
          <div class="row">
           <div class="col-md-offset-3 col-md-9 text-right">
            <button type="button" class="btn btn-default" onclick="Pegawai.back()">Kembali</button>
           </div>
          </div>
         </div>
         <div class="col-md-6"> </div>
        </div>
       </div>
      </form>
     </div>
    </div>
   </div>
  </div>
 </div>
 <!--./row--> 
</div>

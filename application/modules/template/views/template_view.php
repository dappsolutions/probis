<!DOCTYPE html>
<html lang="en">

 <?php echo Modules::run('header'); ?>

 <?php
 if (isset($header_data)) {
  foreach ($header_data as $v_head) {
   echo $v_head;
  }
 }
 ?>
 <body class="fix-header">
  <!-- ===== Main-Wrapper ===== -->
  <div id="wrapper">
   <div class='loader'>

   </div>
   <div class="preloader">
    <div class="cssload-speeding-wheel"></div>
   </div>

   <?php echo $this->load->view('header_content'); ?>
   <?php echo $this->load->view('left_content'); ?>
   <?php //echo $this->load->view('right_content'); ?>
   
   <!-- ===== Page-Content ===== -->
   <div class="page-wrapper">

    <!-- ===== Page-Container ===== -->
    <?php echo $this->load->view($module . '/' . $view_file); ?>
    <!-- ===== Page-Container-End ===== -->
    <?php echo $this->load->view('footer_content'); ?>
   </div>
   <!-- ===== Page-Content-End ===== -->
  </div>
  <!-- ===== Main-Wrapper-End ===== -->  
 </body>

</html>

<?php if ($this->session->userdata('hak_akses') == 'General') { ?>
 <li>
  <a class="waves-effect" href="<?php echo base_url() . 'dashboard' ?>" aria-expanded="false"><i class="icon-screen-desktop fa-fw"></i> <span class="hide-menu"> Dashboard </span></a>
 </li>
 <li>
  <a class="waves-effect" href="<?php echo base_url() . 'grafik' ?>" aria-expanded="false"><i class="icon-pie-chart fa-fw"></i> <span class="hide-menu"> Grafik </span></a>
 </li>
<?php } ?>   

<?php if ($this->session->userdata('hak_akses') == 'General') { ?>
 <li>
  <a class="waves-effect" href="javascript:void(0);" aria-expanded="false"><i class="icon-doc fa-fw"></i> <span class="hide-menu"> Proses Bisnis </span></a>
  <ul aria-expanded="false" class="collapse">     
   <li> <a href="<?php echo base_url() . 'probis' ?>"><?php echo 'Probis' ?></a> </li>
   <?php if (trim($this->session->userdata('nama_upt')) == 'Kantor Induk') { ?>
    <li> <a href="<?php echo base_url() . 'probisupt' ?>"><?php echo 'Probis UPT' ?></a> </li>
   <?php } else { ?>   
    <li> <a href="<?php echo base_url() . 'probiski' ?>"><?php echo 'Probis KI' ?></a> </li>
   <?php } ?>
  </ul>
 </li> 
<?php } ?>   
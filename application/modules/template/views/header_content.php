<!-- ===== Top-Navigation ===== -->
<nav class="navbar navbar-default navbar-static-top m-b-0">
 <div class="navbar-header">
  <a class="navbar-toggle font-20 hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse">
   <i class="fa fa-bars"></i>
  </a>
  <div class="top-left-part">
   <a class="logo" href="<?php echo base_url() . 'dashboard' ?>">
    <b>
     <img src="<?php echo base_url() ?>assets/images/Logo-PLN.png" alt="home" width="30" height="50" class=""/>
    </b>
    <span>
     <b><label class="bold" style="font-weight: bold;">PROBIS</label></b> 
    </span>
   </a>
  </div>
  <ul class="nav navbar-top-links navbar-left hidden-xs">
   <li>
    <a href="javascript:void(0)" class="sidebartoggler font-20 waves-effect waves-light"><i class="icon-arrow-left-circle"></i></a>
   </li>
   <li>
    <form role="search" class="app-search hidden-xs">     
     <label class="text-white">APLIKASI PROSES BISNIS MANAJEMEN ASET</label>
    </form>
   </li>
  </ul>
 </div>
</nav>
<!-- ===== Top-Navigation-End ===== -->
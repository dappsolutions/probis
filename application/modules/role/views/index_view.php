<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <!--.row-->
 <div class="row">
  <div class="col-md-12">
   <div class="panel panel-success">
    <div class="panel-heading"> <?php echo $title_content ?></div>
    <div class="panel-wrapper collapse in" aria-expanded="true">
     <div class="panel-body">
      <form action="#" class="form-horizontal">
       <div class="form-body">
        <h3 class="box-title"><?php echo 'Administrator' ?> <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">
        <?php if (!empty($content)) { ?>
         <div class="row" id="admin">
          <?php foreach ($content as $value) { ?>
           <div class="col-md-4 admin">
            <div class="form-group">
             <div class="col-md-9">
              <div class="checkbox checkbox-success">
               <?php $checked = '' ?>
               <?php $id_role = '' ?>
               <?php if (!empty($value['admin'])) { ?>
                <?php if ($value['admin']['status'] == 1) { ?>
                 <?php $checked = 'checked'; ?>
                <?php } ?>
                <?php $id_role = $value['admin']['id'] ?>
               <?php } ?>
               <input id="<?php echo $value['id'] ?>" type="checkbox" <?php echo $checked ?>>
               <label id_role="<?php echo $id_role ?>" for="<?php echo $value['id'] ?>"> &nbsp; <?php echo $value['nama_module'] ?></label>
              </div>
             </div>
            </div>
           </div>
          <?php } ?>
         </div>
        <?php } ?>
        <br/>

        <h3 class="box-title"><?php echo 'Wali Murid' ?> <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">
        <?php if (!empty($content)) { ?>
         <div class="row" id="wali">
          <?php foreach ($content as $value) { ?>
           <div class="col-md-4 wali">
            <div class="form-group">
             <div class="col-md-9">
              <div class="checkbox checkbox-success">
               <?php $checked = '' ?>
               <?php $id_role = '' ?>
               <?php if (!empty($value['wali'])) { ?>
                <?php if ($value['wali']['status'] == 1) { ?>
                 <?php $checked = 'checked'; ?>
                <?php } ?>
                <?php $id_role = $value['wali']['id'] ?>
               <?php } ?>
               <input id="<?php echo $value['id'] ?>" type="checkbox" <?php echo $checked ?>>
               <label id_role="<?php echo $id_role ?>" for="<?php echo $value['id'] ?>"> &nbsp; <?php echo $value['nama_module'] ?></label>
              </div>
             </div>
            </div>
           </div>
          <?php } ?>
         </div>
        <?php } ?>
        <br/>

        <h3 class="box-title"><?php echo 'Taruna' ?> <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">
        <?php if (!empty($content)) { ?>
         <div class="row" id="taruna">
          <?php foreach ($content as $value) { ?>
           <div class="col-md-4 taruna">
            <div class="form-group">
             <div class="col-md-9">
              <div class="checkbox checkbox-success">
               <?php $checked = '' ?>
               <?php $id_role = '' ?>
               <?php if (!empty($value['taruna'])) { ?>
                <?php if ($value['taruna']['status'] == 1) { ?>
                 <?php $checked = 'checked'; ?>
                <?php } ?>
                <?php $id_role = $value['taruna']['id'] ?>
               <?php } ?>
               <input id="<?php echo $value['id'] ?>" type="checkbox" <?php echo $checked ?>>
               <label id_role="<?php echo $id_role ?>" for="<?php echo $value['id'] ?>"> &nbsp; <?php echo $value['nama_module'] ?></label>
              </div>
             </div>
            </div>
           </div>
          <?php } ?>
         </div>
        <?php } ?>


        <div class="form-actions">
         <div class="row">
          <div class="col-md-12">
           <div class="row">
            <div class="col-md-offset-3 col-md-9 text-right">
             <button type="submit" class="btn btn-success" onclick="Role.simpan(this, event)">Ubah</button>
            </div>
           </div>
          </div>
          <div class="col-md-6"> </div>
         </div>
        </div>
      </form>
     </div>
    </div>
   </div>
  </div>
 </div>
 <!--./row--> 
</div>

<?php

class Role extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'role';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/role.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'role_priveledge';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Role";
  $data['title_content'] = 'Data Role';
  $content = $this->getDataRole();
  $data['content'] = $content;
//  $total_rows = $content['total_rows'];
//  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataRole($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.prodi', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*'),
              'where' => "t.deleted = 0"
  ));

  return $total;
 }

 public function getRoleAdmin($module, $hak_akses) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName(),
              'where' => "priveledge = '" . $hak_akses . "' and module = '" . $module . "'"
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
  }


  return $result;
 }

 public function getDataRole() {
  $data = Modules::run('database/get', array(
              'table' => 'module m',
              'field' => array('m.*'),
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['admin'] = $this->getRoleAdmin($value['id'], 1);
    $value['wali'] = $this->getRoleAdmin($value['id'], 2);
    $value['taruna'] = $this->getRoleAdmin($value['id'], 3);
    array_push($result, $value);
   }
  }

//  echo '<pre>';
//  print_r($result);die;
  return $result;
 }

 public function getDetailDataRole($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*'),
              'where' => "t.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Role";
  $data['title_content'] = 'Tambah Role';
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataRole($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Role";
  $data['title_content'] = 'Ubah Role';
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataRole($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Role";
  $data['title_content'] = "Detail Role";
  echo Modules::run('template', $data);
 }

 public function getPostDataRole($value, $hak_akses) {
  $data['priveledge'] = $hak_akses;
  $data['module'] = $value->module;
  $data['status'] = $value->checked;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);
//  die;
  $is_valid = false;

  $this->db->trans_begin();
  try {
   foreach ($data->admin as $value) {
    $post_admin = $this->getPostDataRole($value, 1);
    if($value->id_role == ''){
     Modules::run('database/_insert', $this->getTableName(), $post_admin);
    }else{
     Modules::run('database/_update', $this->getTableName(), $post_admin, array('id' => $value->id_role));
    }
   }
   
   foreach ($data->wali as $value) {
    $post_wali = $this->getPostDataRole($value, 2);
    if($value->id_role == ''){
     Modules::run('database/_insert', $this->getTableName(), $post_wali);
    }else{
     Modules::run('database/_update', $this->getTableName(), $post_wali, array('id' => $value->id_role));
    }
   }
   
   foreach ($data->taruna as $value) {
    $post_taruna = $this->getPostDataRole($value, 3);
    if($value->id_role == ''){
     Modules::run('database/_insert', $this->getTableName(), $post_taruna);
    }else{
     Modules::run('database/_update', $this->getTableName(), $post_taruna, array('id' => $value->id_role));
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Role";
  $data['title_content'] = 'Data Role';
  $content = $this->getDataRole($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}

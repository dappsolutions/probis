var RuleProbis = {
 module: function () {
  return 'rule_probis';
 },

 add: function () {
  window.location.href = url.base_url(RuleProbis.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(RuleProbis.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(RuleProbis.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(RuleProbis.module()) + "index";
   }
  }
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'nama': $('#nama').val(),
  };

  return data;
 },

 simpan: function (id, e) {
  e.preventDefault();
  var data = RuleProbis.getPostData();

  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);
  formData.append('file', $('input#file').prop('files')[0]);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(RuleProbis.module()) + "simpan",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(RuleProbis.module()) + "detail" + '/' + resp.id;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error(resp.message);
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(RuleProbis.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(RuleProbis.module()) + "detail/" + id;
 },

 feedback: function (id) {
  window.location.href = url.base_url(RuleProbis.module()) + "feedback/" + id;
 },

 delete: function (id) {

  var html = "<div class='row'>";
  html += "<div class='col-md-12 text-center'>";
  html += "<h4>Apa anda yakin akan menghapus data ini ?</h4>";
  html += "<div class='text-center'>";
  html += "<button class='btn btn-success font-10' onclick='RuleProbis.execDeleted(" + id + ")'>Proses</button>&nbsp;";
  html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'>Batal</button>&nbsp;";
  html += "</div>";
  html += "</div>";
  html += "</div>";

  bootbox.dialog({
   message: html,
  });
 },

 execDeleted: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(RuleProbis.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(RuleProbis.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 deleteFeedback: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(RuleProbis.module()) + "deleteFeedback/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.reload();
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 upload: function (elm) {
  var input = $(elm).closest('span').find('input');
  input.click();
 },

 getFilename: function (elm, type) {
  RuleProbis.checkFile(elm, type);
 },

 checkFile: function (elm, type) {
  if (window.FileReader) {
   var data_file = $(elm).get(0).files[0];
   var file_name = data_file.name;
   var data_from_file = data_file.name.split('.');

   var type_file = $.trim(data_from_file[data_from_file.length - 1]);
   if (type_file == 'xlsx') {
    type = 'xlsx';
   }
   if (type_file == type) {
    if (data_file.size <= 1324000) {
     $(elm).closest('div').find('span.fileinput-filename').text($(elm).val());
    } else {
     toastr.error('Gagal Upload, Ukuran File Maximal 1 MB');
     message.closeLoading();
    }
   } else {
    toastr.error('File Harus Berformat ' + type);
    $(elm).val('');
    message.closeLoading();
   }
  } else {
   toastr.error('FileReader is Not Supported');
   message.closeLoading();
  }
 },

 showLogo: function (elm, e) {
  e.preventDefault();
  $.ajax({
   type: 'POST',
   data: {
    file: $.trim($(elm).text())
   },
   dataType: 'html',
   async: false,
   url: url.base_url(RuleProbis.module()) + "showLogo",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },

 previewFile: function (name_of_file) {
  $.ajax({
   type: 'POST',
   data: {
    file: name_of_file
   },
   dataType: 'html',
   async: false,
   url: url.base_url(RuleProbis.module()) + "showLogo",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },

 changeManual: function (elm) {
  $('div.manual_detail').addClass('display-none');
  $('div.manual_add').removeClass('display-none');
  $('div.manual_add').append("<i class='mdi mdi-close mdi-24px hover' onclick='RuleProbis.cancelChangeManual(this)'></i>");
 },

 cancelChangeManual: function (elm) {
  $('div.manual_detail').removeClass('display-none');
  $('div.manual_add').addClass('display-none');
  $('i.mdi-close').remove();

  var inputFile = '<div class="form-control" data-trigger="fileinput"> ';
  inputFile += '<i class="glyphicon glyphicon-file fileinput-exists"></i>';
  inputFile += '<span class="fileinput-filename"></span>';
  inputFile += '</div> ';
  inputFile += '<span class="input-group-addon btn btn-default btn-file"> ';
  inputFile += '<span class="fileinput-new" onclick="RuleProbis.upload(this)">Select file</span> ';
  inputFile += '<input type="file" style="display: none;" id="file" onchange="RuleProbis.getFilename(this)"/>';
  inputFile += '</span>';
  $('div.manual_upload').html(inputFile);
 },

 showUpdateFoto: function (elm) {
  bootbox.dialog({
   message: 'Ganti Foto'
  });
 },

 showTooltip: function (elm) {

 },

 setDate: function () {
  $('input#tanggal_lahir').datepicker({
   format: 'yyyy-mm-dd',
   autoclose: true,
  });
 },

 sendFeedback: function () {
  var id = $('input#id').val();
  $.ajax({
   type: 'POST',
   data: {
    pesan: $('#keterangan_feedback').val()
   },
   dataType: 'json',
   async: false,
   url: url.base_url(RuleProbis.module()) + "sendFeedback/" + id,
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Simpan...");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Disimpan");
     var reload = function () {
      window.location.reload();
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Disimpan");
    }
    message.closeLoading();
   }
  });
 },

 aktif: function (elm, id) {
  var status = $.trim($(elm).text());

  $.ajax({
   type: 'POST',
   dataType: 'json',
   data:{
    status: $.trim(status.toLowerCase())
   },
   async: false,
   url: url.base_url(RuleProbis.module()) + "changeStatus/" + id,

   error: function () {
    toastr.error("Gagal");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Diproses");
     var reload = function () {
      window.location.href = url.base_url(RuleProbis.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Diproses");
    }
   }
  });
 }
};

$(function () {
 RuleProbis.setDate();
});
var Probis = {
	module: function () {
		return 'probis';
	},

	add: function () {
		window.location.href = url.base_url(Probis.module()) + "add";
	},

	back: function () {
		var referensi = $('input#ref').val();
		if (referensi != '') {
			window.location.href = url.base_url("referensi_probis") + "index";
		} else {
			window.location.href = url.base_url(Probis.module()) + "index";
		}
	},

	search: function (elm, e) {
		if (e.keyCode == 13) {
			var keyWord = $(elm).val();
			if (keyWord != '') {
				window.location.href = url.base_url(Probis.module()) + "search" + '/' + keyWord;
			} else {
				window.location.href = url.base_url(Probis.module()) + "index";
			}
		}
	},

	searchBtn: function () {
		var keyWord = $('input#keyword').val();
		if (keyWord != '') {
			window.location.href = url.base_url(Probis.module()) + "search" + '/' + keyWord;
		} else {
			window.location.href = url.base_url(Probis.module()) + "index";
		}
	},

	getPostKeyword: function () {
		var table = $('table#table_keyword').find('tbody').find('tr');
		var data = [];
		$.each(table, function () {
			var id = $(this).attr('id');
			if (id == 'baru') {
				data.push({
					'keyword': $(this).find('input#keyword').val()
				});
			}
		});

		return data;
	},

	getPostKeywordEdit: function () {
		var table = $('table#table_keyword').find('tbody').find('tr');
		var data = [];
		$.each(table, function () {
			var id = $(this).attr('id');
			if (id != 'baru') {
				data.push({
					'id': id,
					'is_edit': $(this).hasClass('display-none') ? 0 : 1,
					'keyword': $(this).find('input#keyword').val()
				});
			}
		});

		return data;
	},

	getPostData: function () {
		var data = {
			'id': $('#id').val(),
			'nama': $('#nama').val(),
			'tgl_update': $('#tgl_update').val(),
			'upt': $('select#upt').val(),
			'tahun': $('#tahun').val(),
			'edisi': $('#edisi').val(),
			'revisi': $('#revisi').val(),
			'keyword': Probis.getPostKeyword(),
			'keyword_edit': Probis.getPostKeywordEdit(),
		};

		return data;
	},

	simpan: function (id, e) {
		e.preventDefault();
		var data = Probis.getPostData();

		var formData = new FormData();
		formData.append('data', JSON.stringify(data));
		formData.append("id", id);
		formData.append('file_xls', $('input#probis_xls').prop('files')[0]);
		formData.append('file_pdf', $('input#probis_pdf').prop('files')[0]);

		if (validation.run()) {
			$.ajax({
				type: 'POST',
				data: formData,
				dataType: 'json',
				processData: false,
				contentType: false,
				async: false,
				url: url.base_url(Probis.module()) + "simpan",
				error: function () {
					toastr.error("Program Error");
					message.closeLoading();
				},

				beforeSend: function () {
					message.loadingProses("Proses Simpan...");
				},

				success: function (resp) {
					if (resp.is_valid) {
						toastr.success("Berhasil Disimpan");
						var reload = function () {
							window.location.href = url.base_url(Probis.module()) + "detail" + '/' + resp.id;
						};

						setTimeout(reload(), 1000);
					} else {
						toastr.error(resp.message);
					}
					message.closeLoading();
				}
			});
		}
	},

	ubah: function (id) {
		window.location.href = url.base_url(Probis.module()) + "ubah/" + id;
	},

	detail: function (id) {
		window.open(url.base_url(Probis.module()) + "detail/" + id);
	},

	feedback: function (id) {
		window.location.href = url.base_url(Probis.module()) + "feedback/" + id;
	},

	delete: function (id) {

		var html = "<div class='row'>";
		html += "<div class='col-md-12 text-center'>";
		html += "<h4>Apa anda yakin akan menghapus data ini ?</h4>";
		html += "<div class='text-center'>";
		html += "<button class='btn btn-success font-10' onclick='Probis.execDeleted(" + id + ")'>Proses</button>&nbsp;";
		html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'>Batal</button>&nbsp;";
		html += "</div>";
		html += "</div>";
		html += "</div>";

		bootbox.dialog({
			message: html,
		});
	},

	execDeleted: function (id) {
		$.ajax({
			type: 'POST',
			dataType: 'json',
			async: false,
			url: url.base_url(Probis.module()) + "delete/" + id,

			error: function () {
				toastr.error("Gagal Dihapus");
			},

			success: function (resp) {
				if (resp.is_valid) {
					toastr.success("Berhasil Dihapus");
					var reload = function () {
						window.location.href = url.base_url(Probis.module()) + "index";
					};

					setTimeout(reload(), 1000);
				} else {
					toastr.error("Gagal Dihapus");
				}
			}
		});
	},

	deleteFeedback: function (id) {
		$.ajax({
			type: 'POST',
			dataType: 'json',
			async: false,
			url: url.base_url(Probis.module()) + "deleteFeedback/" + id,

			error: function () {
				toastr.error("Gagal Dihapus");
			},

			success: function (resp) {
				if (resp.is_valid) {
					toastr.success("Berhasil Dihapus");
					var reload = function () {
						window.location.reload();
					};

					setTimeout(reload(), 1000);
				} else {
					toastr.error("Gagal Dihapus");
				}
			}
		});
	},

	upload: function (elm) {
		var input = $(elm).closest('span').find('input');
		input.click();
	},

	getFilename: function (elm, type) {
		Probis.checkFile(elm, type);
	},

	checkFile: function (elm, type) {
		if (window.FileReader) {
			var data_file = $(elm).get(0).files[0];
			var file_name = data_file.name;
			var data_from_file = data_file.name.split('.');

			var type_file = $.trim(data_from_file[data_from_file.length - 1]);
			if (type_file == 'xlsx') {
				type = 'xlsx';
			}

			// console.log('sze', data_file.size);
			if (type_file == type) {
				if (data_file.size <= 6384888) {
					$(elm).closest('div').find('span.fileinput-filename').text($(elm).val());
				} else {
					toastr.error('Gagal Upload, Ukuran File Maximal 1 MB');
					message.closeLoading();
				}
			} else {
				toastr.error('File Harus Berformat ' + type);
				$(elm).val('');
				message.closeLoading();
			}
		} else {
			toastr.error('FileReader is Not Supported');
			message.closeLoading();
		}
	},

	showLogo: function (elm, e) {
		e.preventDefault();
		var id = $('#id').val();
		var url_host = window.location.host;
		var url_protocol = window.location.protocol;
		var url = url_protocol + '//' + url_host + '/' + Probis.module() + '/probis/showLogo/' + id;
		var w = screen.width - 300,
			h = 500;
		var left = (screen.width / 2) - (w / 2);
		var top = (screen.height / 2) - (h / 2);

		window.open(
			url,
			"_blank",
			"toolbar=no, scrollbars=yes, resizable=yes, top=" + top + ", left=" + left + "," +
			" width=" + w + ", height=" + h
		);
		return false;
		// $.ajax({
		//  type: 'POST',
		//  data: {
		//   file: $.trim($(elm).text())
		//  },
		//  dataType: 'html',
		//  async: false,
		//  url: url.base_url(Probis.module()) + "showLogo",
		//  success: function (resp) {
		//   bootbox.dialog({
		//    message: resp,
		//    size: 'large'
		//   });
		//  }
		// });
	},

	previewFile: function (id) {
		var url_host = window.location.host;
		var url_protocol = window.location.protocol;
		var url = url_protocol + '//' + url_host + '/' + Probis.module() + '/probis/showLogo/' + id;
		var w = screen.width - 300,
			h = 500;
		var left = (screen.width / 2) - (w / 2);
		var top = (screen.height / 2) - (h / 2);

		window.open(
			url,
			"_blank",
			"toolbar=no, scrollbars=yes, resizable=yes, top=" + top + ", left=" + left + "," +
			" width=" + w + ", height=" + h
		);
		return false;
		// $.ajax({
		// 	type: 'POST',
		// 	data: {
		// 		file: name_of_file
		// 	},
		// 	dataType: 'html',
		// 	async: false,
		// 	url: url.base_url(Probis.module()) + "showLogo",
		// 	success: function (resp) {
		// 		bootbox.dialog({
		// 			message: resp,
		// 			size: 'large'
		// 		});
		// 	}
		// });
	},

	changeManual: function (elm) {
		$('div.manual_detail').addClass('display-none');
		$('div.manual_add').removeClass('display-none');
		$('div.manual_add').append("<i class='mdi mdi-close mdi-24px hover' onclick='Probis.cancelChangeManual(this)'></i>");
	},

	cancelChangeManual: function (elm) {
		$('div.manual_detail').removeClass('display-none');
		$('div.manual_add').addClass('display-none');
		$('i.mdi-close').remove();

		var inputFile = '<div class="form-control" data-trigger="fileinput"> ';
		inputFile += '<i class="glyphicon glyphicon-file fileinput-exists"></i>';
		inputFile += '<span class="fileinput-filename"></span>';
		inputFile += '</div> ';
		inputFile += '<span class="input-group-addon btn btn-default btn-file"> ';
		inputFile += '<span class="fileinput-new" onclick="Probis.upload(this)">Select file</span> ';
		inputFile += '<input type="file" style="display: none;" id="file" onchange="Probis.getFilename(this)"/>';
		inputFile += '</span>';
		$('div.manual_upload').html(inputFile);
	},

	showUpdateFoto: function (elm) {
		bootbox.dialog({
			message: 'Ganti Foto'
		});
	},

	showTooltip: function (elm) {

	},

	setDate: function () {
		$('input#tgl_update').datepicker({
			format: 'yyyy-mm-dd',
			autoclose: true,
		});
	},

	sendFeedback: function () {
		var id = $('input#id').val();
		$.ajax({
			type: 'POST',
			data: {
				pesan: $('#keterangan_feedback').val()
			},
			dataType: 'json',
			async: false,
			url: url.base_url(Probis.module()) + "sendFeedback/" + id,
			error: function () {
				toastr.error("Gagal");
				message.closeLoading();
			},

			beforeSend: function () {
				message.loadingProses("Proses Simpan...");
			},

			success: function (resp) {
				if (resp.is_valid) {
					toastr.success("Berhasil Disimpan");
					var reload = function () {
						window.location.reload();
					};

					setTimeout(reload(), 1000);
				} else {
					toastr.error("Gagal Disimpan");
				}
				message.closeLoading();
			}
		});
	},

	addData: function (elm) {
		var tr = $(elm).closest('tbody').find('tr:last');
		var newTr = tr.clone();
		newTr.find('input').val('');
		newTr.find('td:eq(1)').html('<i class="mdi mdi-delete mdi-24px hover text-danger" onclick="Probis.remove(this)"></i>');
		tr.after(newTr);
	},

	remove: function (elm) {
		$(elm).closest('tr').remove();
	},

	removeData: function (elm) {
		$(elm).closest('tr').addClass('display-none');
	},

	changeStatus: function (id) {
		$.ajax({
			type: 'POST',
			data: {
				id: id
			},
			dataType: 'html',
			async: false,
			url: url.base_url(Probis.module()) + "getStatus",
			error: function () {
				toastr.error("Gagal");
			},

			success: function (resp) {
				bootbox.dialog({
					message: resp
				});
			}
		});
	},

	updateStatus: function (elm) {
		var id = $('input#probis_status').val();
		var keterangan = $('textarea#keterangan').val();
		var status = $.trim($(elm).find('label').text());
		$.ajax({
			type: 'POST',
			data: {
				id: id,
				status: status,
				keterangan: keterangan
			},
			dataType: 'json',
			async: false,
			url: url.base_url(Probis.module()) + "updateStatus",
			error: function () {
				message.closeLoading();
				toastr.error("Gagal");
			},

			beforeSend: function () {
				message.loadingProses("Proses Update...");
			},

			success: function (resp) {
				message.closeLoading();
				if (resp.is_valid) {
					toastr.success("Update Sukses");
					var reload = function () {
						window.location.reload();
					};
					setTimeout(reload(), 1000);
				} else {
					toastr.error("Update Gagal");
				}
			}
		});
	},

	showKeteranganKlausul: function (elm, jenis) {
		var id = $(elm).attr('data_id');
		if ($(elm).is(':checked')) {
			var html = "<div class='row'>";
			html += "<div class='col-md-12 text-left'>";
			html += "<h4>Isi Keterangan Klausul ISO</h4>";
			html += "<div class='text-right'>";			
			html += "<textarea class='form-control' id='ket_klausul'></textarea><br/>";			
			html += "<button class='btn btn-success font-10' data_id='"+id+"' onclick='Probis.setStatus(this, 0)'>Proses</button>&nbsp;";
			html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'>Batal</button>&nbsp;";
			html += "</div>";
			html += "</div>";
			html += "</div>";

			bootbox.dialog({
				message: html,
			});
		}
	},
	
 showKeteranganRegulasi: function (elm, jenis) {
		var id = $(elm).attr('data_id');
		if ($(elm).is(':checked')) {
			var html = "<div class='row'>";
			html += "<div class='col-md-12 text-left'>";
			html += "<h4>Isi Keterangan Probis Terhadap Regulasi</h4>";
			html += "<div class='text-right'>";			
			html += "<textarea class='form-control' id='ket_klausul'></textarea><br/>";			
			html += "<button class='btn btn-success font-10' data_id='"+id+"' onclick='Probis.setStatus(this, 1)'>Proses</button>&nbsp;";
			html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'>Batal</button>&nbsp;";
			html += "</div>";
			html += "</div>";
			html += "</div>";

			bootbox.dialog({
				message: html,
			});
		}
	},
 
 showKeteranganResiko: function (elm, jenis) {
		var id = $(elm).attr('data_id');
		if ($(elm).is(':checked')) {
			var html = "<div class='row'>";
			html += "<div class='col-md-12 text-left'>";
			html += "<h4>Isi Keterangan Probis Terhadap Risiko</h4>";
			html += "<div class='text-right'>";			
			html += "<textarea class='form-control' id='ket_klausul'></textarea><br/>";			
			html += "<button class='btn btn-success font-10' data_id='"+id+"' onclick='Probis.setStatus(this, 2)'>Proses</button>&nbsp;";
			html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'>Batal</button>&nbsp;";
			html += "</div>";
			html += "</div>";
			html += "</div>";

			bootbox.dialog({
				message: html,
			});
		}
	},

	setStatus: function (elm, jenis) {
		var id = $(elm).attr('data_id');
		var keterangan = "";
		var checked = $(elm).is(':checked') ? 1 : 0;

		if(jenis == 0){
			keterangan = $('#ket_klausul').val();

			checked = 1;
			jenis = "klausul";
			if(keterangan == ''){
				toastr.error("Klausul Harus Diisi");			
				return;
			}
		}
		
  if(jenis == 1){
			keterangan = $('#ket_klausul').val();

			checked = 1;
			jenis = "regulasi";
			if(keterangan == ''){
				toastr.error("Regulasi Harus Diisi");			
				return;
			}
		}
  
  if(jenis == 2){
			keterangan = $('#ket_klausul').val();

			checked = 1;
			jenis = "resiko";
			if(keterangan == ''){
				toastr.error("Resiko Harus Diisi");			
				return;
			}
		}
		$.ajax({
			type: 'POST',
			data: {
				id: id,
				jenis: jenis,
				checked: checked,
				keterangan: keterangan
			},
			dataType: 'json',
			async: false,
			url: url.base_url(Probis.module()) + "setStatus",
			error: function () {
				message.closeLoading();
				toastr.error("Gagal");
			},

			beforeSend: function () {
				message.loadingProses("Proses Update...");
			},

			success: function (resp) {
				message.closeLoading();
				if (resp.is_valid) {
					toastr.success("Update Sukses");
					var reload = function () {
						window.location.reload();
					};
					setTimeout(reload(), 1000);
				} else {
					toastr.error("Update Gagal");
				}
			}
		});
	},

	getDetailChildProbis: function (elm, level) {
		var class_child = 'child-' + level;
		$('.' + class_child).remove();
		var tr = $(elm).closest('tr');

		var toggle = tr.find('i#toogle-action');
		if (toggle.hasClass('fa-chevron-down')) {
			var parent = $(elm).attr('parent');
			var child_first = $(elm).attr('child_first');
			$.ajax({
				type: 'POST',
				data: {
					parent: parent,
					child_first: child_first,
					level: level
				},
				dataType: 'html',
				async: false,
				url: url.base_url(Probis.module()) + "getDetailChildProbis",
				error: function () {
					toastr.error("Gagal");
					message.closeLoading();
				},

				beforeSend: function () {
					message.loadingProses("Proses Retrieving Data...");
				},

				success: function (resp) {
					message.closeLoading();

					toggle.addClass('fa-chevron-up');
					toggle.removeClass('fa-chevron-down');

					var newTr = tr.clone();
					newTr.addClass(class_child);
					newTr.html('<td colspan="11">' + resp + '</td>');
					tr.after(newTr);
				}
			});
		} else {
			toggle.addClass('fa-chevron-down');
			toggle.removeClass('fa-chevron-up');
		}
	},

	showStatusData: function(elm, id){
		if($(elm).hasClass('fa-chevron-down')){
			$(elm).removeClass('fa-chevron-down');
			$(elm).addClass('fa-chevron-up');
		}else{
			$(elm).removeClass('fa-chevron-up');
			$(elm).addClass('fa-chevron-down');
		}
		var tr = $('tr.status_data');

		$.each(tr, function(){
			var data_id = $(this).attr('data_id');
			if(id == data_id){
				if($(this).hasClass('hide')){
					$(this).removeClass('hide');
				}else{
					$(this).addClass('hide');
				}
			}			
		});
	}
};

$(function () {
	Probis.setDate();
});

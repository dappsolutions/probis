var Review = {
 module: function () {
  return 'review_usulan_probis';
 },

 add: function () {
  window.location.href = url.base_url(Review.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(Review.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(Review.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(Review.module()) + "index";
   }
  }
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'nama': $('#nama').val(),
  };

  return data;
 },

 simpan: function (id, e) {
  e.preventDefault();
  var data = Review.getPostData();

  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);
  formData.append('file_xls', $('input#probis_xls').prop('files')[0]);
  formData.append('file_pdf', $('input#probis_pdf').prop('files')[0]);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(Review.module()) + "simpan",
    error: function () {
     toastr.error("Program Error");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Review.module()) + "detail" + '/' + resp.id;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error(resp.message);
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(Review.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(Review.module()) + "detail/" + id;
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(Review.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(Review.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 upload: function (elm) {
  var input = $(elm).closest('span').find('input');
  input.click();
 },

 getFilename: function (elm, type) {
  Review.checkFile(elm, type);
 },

 checkFile: function (elm, type) {
  if (window.FileReader) {
   var data_file = $(elm).get(0).files[0];
   var file_name = data_file.name;
   var data_from_file = data_file.name.split('.');

   var type_file = $.trim(data_from_file[data_from_file.length - 1]);
   if (type_file == type || type_file == 'xlsx') {
    if (data_file.size <= 1324000) {
     $(elm).closest('div').find('span.fileinput-filename').text($(elm).val());
    } else {
     toastr.error('Gagal Upload, Ukuran File Maximal 1 MB');
     message.closeLoading();
    }
   } else {
    toastr.error('File Harus Berformat ' + type);
    $(elm).val('');
    message.closeLoading();
   }
  } else {
   toastr.error('FileReader is Not Supported');
   message.closeLoading();
  }
 },

 showLogo: function (elm, e) {
  e.preventDefault();
  $.ajax({
   type: 'POST',
   data: {
    file: $.trim($(elm).text())
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Review.module()) + "showLogo",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },

 rejected: function (id) {
  var html = "<div class='row'>";
  html += "<div class='col-md-12'>";
  html += "<h4>Isi Keterangan</h4>";
  html += '<textarea class="form-control" id="keterangan"></textarea><br/>';
  html += "<div class='text-right'>";
  html += "<button class='btn btn-success font-10' onclick='Review.exceRejected(" + id + ")'>Proses</button>&nbsp;";
  html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'>Batal</button>&nbsp;";
  html += "</div>";
  html += "</div>";
  html += "</div>";

  bootbox.dialog({
   message: html,
  });
 },

 exceRejected: function (id) {
  var keterangan = $('#keterangan').val();
  if (keterangan == '') {
   toastr.error("Keterangan Harus Diisi");
   return;
  }
  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   data: {
    keterangan: keterangan
   },
   url: url.base_url(Review.module()) + "rejected/" + id,
   error: function () {
    message.closeLoading();
    toastr.error("Gagal");
   },

   beforeSend: function () {
    message.loadingProses("Proses Reject Usulan")
   },

   success: function (resp) {
    message.closeLoading();
    toastr.success("Proses Rejected Sukses");
    var reload = function () {
     window.location.href = url.base_url(Review.module()) + "index";
    };

    setTimeout(reload(), 1000);
   }
  });
 },

 reviewed: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   url: url.base_url(Review.module()) + "reviewed/" + id,
   error: function () {
    message.closeLoading();
    toastr.error("Gagal");
   },

   beforeSend: function () {
    message.loadingProses("Proses reviewed Usulan")
   },

   success: function (resp) {
    message.closeLoading();
    toastr.success("Proses reviewed Sukses");
    var reload = function () {
     window.location.href = url.base_url(Review.module()) + "index";
    };

    setTimeout(reload(), 1000);
   }
  });
 },

 approved: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   url: url.base_url(Review.module()) + "approved" + '/' + id,
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving Data...");
   },

   success: function (resp) {
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
    message.closeLoading();
   }
  });
 },

 getPostDataTembusan: function () {
  var data = [];
  var tb_tembusan = $('table#tb_tembusan').find('tbody').find('tr.bg-warning');
  $.each(tb_tembusan, function () {
   var pegawai = $(this).attr('id');
   data.push({
    'pegawai': pegawai
   });
  });

  return data;
 },

 execApproved: function (id) {
  var data = Review.getPostDataTembusan();

  $.ajax({
   type: 'POST',
   dataType: 'json',
   data: {
    data: data
   },
   async: false,
   url: url.base_url(Review.module()) + "execApproved/" + id,
   error: function () {
    message.closeLoading();
    toastr.error("Gagal");
   },

   beforeSend: function () {
    message.loadingProses("Proses approved Usulan")
   },

   success: function (resp) {
    message.closeLoading();
    if (resp.is_valid) {
     toastr.success("Proses approved Sukses");
     var reload = function () {
      window.location.href = url.base_url(Review.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Approve");
    }
   }
  });
 },

 revision: function (id) {
  var html = "<div class='row'>";
  html += "<div class='col-md-12'>";
  html += "<h4>Isi Keterangan</h4>";
  html += '<textarea class="form-control" id="keterangan"></textarea><br/>';
  html += "<div class='text-right'>";
  html += "<button class='btn btn-success font-10' onclick='Review.execRevision(" + id + ")'>Proses</button>&nbsp;";
  html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'>Batal</button>&nbsp;";
  html += "</div>";
  html += "</div>";
  html += "</div>";

  bootbox.dialog({
   message: html,
  });
 },

 execRevision: function (id) {
  var keterangan = $('#keterangan').val();
  if (keterangan == '') {
   toastr.error("Keterangan Harus Diisi");
   return;
  }

  $.ajax({
   type: 'POST',
   dataType: 'html',
   data: {
    keterangan: keterangan
   },
   async: false,
   url: url.base_url(Review.module()) + "revision/" + id,
   error: function () {
    message.closeLoading();
    toastr.error("Gagal");
   },

   beforeSend: function () {
    message.loadingProses("Proses revision Usulan")
   },

   success: function (resp) {
    message.closeLoading();
    toastr.success("Proses revision Sukses");
    var reload = function () {
     window.location.href = url.base_url(Review.module()) + "index";
    };

    setTimeout(reload(), 1000);
   }
  });
 },

 previewFile: function (name_of_file) {
  $.ajax({
   type: 'POST',
   data: {
    file: name_of_file
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Review.module()) + "showLogo",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },

 changeManual: function (elm) {
  $('div.manual_detail').addClass('display-none');
  $('div.manual_add').removeClass('display-none');
  $('div.manual_add').append("<i class='mdi mdi-close mdi-24px hover' onclick='Review.cancelChangeManual(this)'></i>");
 },

 cancelChangeManual: function (elm) {
  $('div.manual_detail').removeClass('display-none');
  $('div.manual_add').addClass('display-none');
  $('i.mdi-close').remove();

  var inputFile = '<div class="form-control" data-trigger="fileinput"> ';
  inputFile += '<i class="glyphicon glyphicon-file fileinput-exists"></i>';
  inputFile += '<span class="fileinput-filename"></span>';
  inputFile += '</div> ';
  inputFile += '<span class="input-group-addon btn btn-default btn-file"> ';
  inputFile += '<span class="fileinput-new" onclick="Review.upload(this)">Select file</span> ';
  inputFile += '<input type="file" style="display: none;" id="file" onchange="Review.getFilename(this)"/>';
  inputFile += '</span>';
  $('div.manual_upload').html(inputFile);
 },

 showUpdateFoto: function (elm) {
  bootbox.dialog({
   message: 'Ganti Foto'
  });
 },

 showTooltip: function (elm) {

 },

 setDate: function () {
  $('input#tanggal_lahir').datepicker({
   format: 'yyyy-mm-dd',
   autoclose: true,
  });
 },

 searchInTable: function (elm, e) {
  var keyword = $(elm).val();
  helper.searchInTable(keyword, 'table');
 },

 choose: function (elm) {
  $(elm).closest('tr').toggleClass('bg-warning');
 },

 prosesApprove: function () {
  var tb_tembusan = $('table#tb_tembusan').find('tbody').find('tr.bg-warning');
  if (tb_tembusan.length > 0) {
   var id = $('input#id').val();
   Review.execApproved(id);
  } else {
   toastr.error("Belum ada yang dipilih");
  }
 }
};

$(function () {
 Review.setDate();
});
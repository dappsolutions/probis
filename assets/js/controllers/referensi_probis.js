var ReferensiProbis = {
 module: function () {
  return 'referensi_probis';
 },

 add: function (id) {
  window.location.href = url.base_url(ReferensiProbis.module()) + "add/" + id;
 },

 back: function () {
  window.location.href = url.base_url(ReferensiProbis.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(ReferensiProbis.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(ReferensiProbis.module()) + "index";
   }
  }
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'nama': $('#nama').val(),
   'probis': $('#probis').val(),
   'upt': $('select#probis').find('option[value=' + $('#probis').val() + ']').attr('upt')
  };

  return data;
 },

 simpan: function (id, e) {
  e.preventDefault();
  var data = ReferensiProbis.getPostData();

  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);
  formData.append('file', $('input#file').prop('files')[0]);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(ReferensiProbis.module()) + "simpan",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(ReferensiProbis.module()) + "detail" + '/' + resp.id;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error(resp.message);
     }
     message.closeLoading();
    }
   });
  }
 },

 simpanInput: function (id, e) {
  e.preventDefault();
  var data = ReferensiProbis.getPostData();

  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);
  formData.append('file', $('input#file').prop('files')[0]);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(ReferensiProbis.module()) + "simpanInput",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(ReferensiProbis.module()) + "detailInput" + '/' + resp.id;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error(resp.message);
     }
     message.closeLoading();
    }
   });
  }
 },

 simpanOutput: function (id, e) {
  e.preventDefault();
  var data = ReferensiProbis.getPostData();

  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);
  formData.append('file', $('input#file').prop('files')[0]);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(ReferensiProbis.module()) + "simpanOutput",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(ReferensiProbis.module()) + "detailOutput" + '/' + resp.id;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error(resp.message);
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(ReferensiProbis.module()) + "ubah/" + id;
 },

 ubahDokumen: function (id, jenis) {
  window.location.href = url.base_url(ReferensiProbis.module()) + "ubahDokumen/" + id + '/' + jenis;
 },

 detail: function (id) {
  window.location.href = url.base_url(ReferensiProbis.module()) + "detail/" + id;
 },

 detailDokumen: function (id, jenis) {
  if (jenis == 'input') {
   window.location.href = url.base_url(ReferensiProbis.module()) + "detailInput/" + id;
  } else {
   window.location.href = url.base_url(ReferensiProbis.module()) + "detailOutput/" + id;
  }
 },

 detailProbis: function (id) {
  window.location.href = url.base_url("probis") + "detail/" + id + "/referensi";
 },

 delete: function (id) {
  var html = "<div class='row'>";
  html += "<div class='col-md-12 text-center'>";
  html += "<h4>Apa anda yakin akan menghapus data ini ?</h4>";
  html += "<div class='text-center'>";
  html += "<button class='btn btn-success font-10' onclick='ReferensiProbis.execDeleted(" + id + ")'>Proses</button>&nbsp;";
  html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'>Batal</button>&nbsp;";
  html += "</div>";
  html += "</div>";
  html += "</div>";

  bootbox.dialog({
   message: html,
  });
 },

 deleteDokumen: function (id, table) {
  var html = "<div class='row'>";
  html += "<div class='col-md-12 text-center'>";
  html += "<h4>Apa anda yakin akan menghapus data ini ?</h4>";
  html += "<div class='text-center'>";
  html += "<button table='" + table + "' class='btn btn-success font-10' onclick='ReferensiProbis.execDeletedDokumen(this," + id + ")'>Proses</button>&nbsp;";
  html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'>Batal</button>&nbsp;";
  html += "</div>";
  html += "</div>";
  html += "</div>";

  bootbox.dialog({
   message: html,
  });
 },

 execDeleted: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(ReferensiProbis.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(ReferensiProbis.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 execDeletedDokumen: function (elm,id) {
  var table = $(elm).attr('table');
  $.ajax({
   type: 'POST',
   data:{
    table: table
   },
   dataType: 'json',
   async: false,
   url: url.base_url(ReferensiProbis.module()) + "deleteDokumen/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(ReferensiProbis.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 upload: function (elm) {
  var input = $(elm).closest('span').find('input');
  input.click();
 },

 getFilename: function (elm, type) {
  ReferensiProbis.checkFile(elm, type);
 },

 checkFile: function (elm, type) {
  if (window.FileReader) {
   var data_file = $(elm).get(0).files[0];
   var file_name = data_file.name;
   var data_from_file = data_file.name.split('.');

   var type_file = $.trim(data_from_file[data_from_file.length - 1]);
//   console.log(data_file.size);
   if (type_file == type) {
    if (data_file.size <= 13432846) {
     $(elm).closest('div').find('span.fileinput-filename').text($(elm).val());
    } else {
     toastr.error('Gagal Upload, Ukuran File Maximal 10 MB');
     message.closeLoading();
    }
   } else {
    toastr.error('File Harus Berformat ' + type);
    $(elm).val('');
    message.closeLoading();
   }
  } else {
   toastr.error('FileReader is Not Supported');
   message.closeLoading();
  }
 },

 showLogo: function (elm, e) {
  e.preventDefault();
  var id = $('#id').val();
  var url_host = window.location.host;
  var url_protocol = window.location.protocol;
  var url = url_protocol + '//' + url_host + '/probis/referensi_probis/showLogo/' + id;
  var w = screen.width - 300,
          h = 500;
  var left = (screen.width / 2) - (w / 2);
  var top = (screen.height / 2) - (h / 2);

  window.open(
          url,
          "_blank",
          "toolbar=no, scrollbars=yes, resizable=yes, top=" + top + ", left=" + left + "," +
          " width=" + w + ", height=" + h
          );
  return false;
 },
 
 showFileReferensi: function (id) {
//   var id = $('#id').val();
  var url_host = window.location.host;
  var url_protocol = window.location.protocol;
  var url = url_protocol + '//' + url_host + '/probis/referensi_probis/showLogo/' + id;
  var w = screen.width - 300,
          h = 500;
  var left = (screen.width / 2) - (w / 2);
  var top = (screen.height / 2) - (h / 2);

  window.open(
          url,
          "_blank",
          "toolbar=no, scrollbars=yes, resizable=yes, top=" + top + ", left=" + left + "," +
          " width=" + w + ", height=" + h
          );
  return false;
 },

 showDokumen: function (elm, e) {
  e.preventDefault();
  var jenis = $(elm).attr('jenis');
  var id = $('#id').val();
  var url_host = window.location.host;
  var url_protocol = window.location.protocol;
  var url = url_protocol + '//' + url_host + '/probis/referensi_probis/showDokumen/' + id + "/" + jenis;
  var w = screen.width - 300,
          h = 500;
  var left = (screen.width / 2) - (w / 2);
  var top = (screen.height / 2) - (h / 2);

  window.open(
          url,
          "_blank",
          "toolbar=no, scrollbars=yes, resizable=yes, top=" + top + ", left=" + left + "," +
          " width=" + w + ", height=" + h
          );
  return false;
 },
 
 showDokumenFile: function (elm,id) {
  var jenis = $(elm).attr('jenis');
//   var id = $('#id').val();
  var url_host = window.location.host;
  var url_protocol = window.location.protocol;
  var url = url_protocol + '//' + url_host + '/probis/referensi_probis/showDokumen/' + id + "/" + jenis;
  var w = screen.width - 300,
          h = 500;
  var left = (screen.width / 2) - (w / 2);
  var top = (screen.height / 2) - (h / 2);

  window.open(
          url,
          "_blank",
          "toolbar=no, scrollbars=yes, resizable=yes, top=" + top + ", left=" + left + "," +
          " width=" + w + ", height=" + h
          );
  return false;
 },

 changeManual: function (elm) {
  $('div.manual_detail').addClass('display-none');
  $('div.manual_add').removeClass('display-none');
  $('div.manual_add').append("<i class='mdi mdi-close mdi-24px hover' onclick='ReferensiProbis.cancelChangeManual(this)'></i>");
 },

 cancelChangeManual: function (elm) {
  $('div.manual_detail').removeClass('display-none');
  $('div.manual_add').addClass('display-none');
  $('i.mdi-close').remove();

  var inputFile = '<div class="form-control" data-trigger="fileinput"> ';
  inputFile += '<i class="glyphicon glyphicon-file fileinput-exists"></i>';
  inputFile += '<span class="fileinput-filename"></span>';
  inputFile += '</div> ';
  inputFile += '<span class="input-group-addon btn btn-default btn-file"> ';
  inputFile += '<span class="fileinput-new" onclick="ReferensiProbis.upload(this)">Select file</span> ';
  inputFile += '<input type="file" style="display: none;" id="file" onchange="ReferensiProbis.getFilename(this)"/>';
  inputFile += '</span>';
  $('div.manual_upload').html(inputFile);
 },

 showUpdateFoto: function (elm) {
  bootbox.dialog({
   message: 'Ganti Foto'
  });
 },

 showTooltip: function (elm) {

 },

 setDate: function () {
  $('input#tanggal_lahir').datepicker({
   format: 'yyyy-mm-dd',
   autoclose: true,
  });
 },

 daftarReferensi: function (id) {
  $.ajax({
   type: 'POST',
   data: {
    probis: id
   },
   dataType: 'html',
   async: false,
   url: url.base_url(ReferensiProbis.module()) + "daftarReferensi",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving Data...");
   },

   success: function (resp) {
    message.closeLoading();
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },

 daftarInput: function (id) {
  $.ajax({
   type: 'POST',
   data: {
    probis: id
   },
   dataType: 'html',
   async: false,
   url: url.base_url(ReferensiProbis.module()) + "daftarInput",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving Data...");
   },

   success: function (resp) {
    message.closeLoading();
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },
 
 daftarOutput: function (id) {
  $.ajax({
   type: 'POST',
   data: {
    probis: id
   },
   dataType: 'html',
   async: false,
   url: url.base_url(ReferensiProbis.module()) + "daftarOutput",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving Data...");
   },

   success: function (resp) {
    message.closeLoading();
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },

 getDetailChildProbis: function (elm, level) {
  var class_child = 'child-' + level;
  $('.' + class_child).remove();
  var tr = $(elm).closest('tr');

  var toggle = tr.find('i#toogle-action');
  if (toggle.hasClass('fa-chevron-down')) {
   var parent = $(elm).attr('parent');
   var child_first = $(elm).attr('child_first');
   $.ajax({
    type: 'POST',
    data: {
     parent: parent,
     child_first: child_first,
     level: level
    },
    dataType: 'html',
    async: false,
    url: url.base_url(ReferensiProbis.module()) + "getDetailChildProbis",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Retrieving Data...");
    },

    success: function (resp) {
     message.closeLoading();

     toggle.addClass('fa-chevron-up');
     toggle.removeClass('fa-chevron-down');

     var newTr = tr.clone();
     newTr.addClass(class_child);
     newTr.html('<td colspan="11">' + resp + '</td>');
     tr.after(newTr);
    }
   });
  } else {
   toggle.addClass('fa-chevron-down');
   toggle.removeClass('fa-chevron-up');
  }
 },

 addInput: function (id) {
  window.location.href = url.base_url(ReferensiProbis.module()) + "addInput/" + id;
 },

 addOutput: function (id) {
  window.location.href = url.base_url(ReferensiProbis.module()) + "addOutput/" + id;
 },

 previewFile: function (id) {
	var url_host = window.location.host;
	var url_protocol = window.location.protocol;
	var url = url_protocol + '//' + url_host + '/probis/referensi_probis/showLogoProbis/'+id;
	var w = screen.width - 300,
		h = 500;
	var left = (screen.width / 2) - (w / 2);
	var top = (screen.height / 2) - (h / 2);

	window.open(
		url,
		"_blank",
		"toolbar=no, scrollbars=yes, resizable=yes, top=" + top + ", left=" + left + "," +
		" width=" + w + ", height=" + h
	);
	return false;
	// $.ajax({
	// 	type: 'POST',
	// 	data: {
	// 		file: name_of_file
	// 	},
	// 	dataType: 'html',
	// 	async: false,
	// 	url: url.base_url(Probis.module()) + "showLogo",
	// 	success: function (resp) {
	// 		bootbox.dialog({
	// 			message: resp,
	// 			size: 'large'
	// 		});
	// 	}
	// });
},
};

$(function () {
 ReferensiProbis.setDate();
});

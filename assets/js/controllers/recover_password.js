var RecoverPassword = {
 module: function () {
  return 'recover_password';
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(RecoverPassword.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(RecoverPassword.module()) + "lupa_password";
   }
  }
 },

 recover: function (elm, e) {
  e.preventDefault();

  var email = $('#email').val();

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: {
     email: email,
    },
    dataType: 'json',
    async: false,
    url: url.base_url(RecoverPassword.module()) + 'recover',
    error: function () {
//     message.error('.message', 'RecoverPassword Gagal, Terjadi Error di Server');
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Recover...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Recover Password Diproses");
      var login = function () {
       window.location.href = url.base_url("login") + "index";
      };
      setTimeout(login, 1000);
     } else {
//      message.error('.message', 'Username atau Password Tidak Valid');
      toastr.error(resp.message);
     }
     message.closeLoading();
    }
   });
  }
 },

 goto_dashboard: function () {
  var url = "dashboard/dashboard";
  window.location = url;
 },

 check: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   url: url.base_url(RecoverPassword.module()) + "check/" + id,
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Checking...");
   },

   success: function (resp) {
    message.closeLoading();
    bootbox.dialog({
     message: resp
    });
   }
  });
 },

 delete: function (id) {

  var html = "<div class='row'>";
  html += "<div class='col-md-12 text-center'>";
  html += "<h4>Apa anda yakin akan menghapus data ini ?</h4>";
  html += "<div class='text-center'>";
  html += "<button class='btn btn-success font-10' onclick='RecoverPassword.execDeleted(" + id + ")'>Proses</button>&nbsp;";
  html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'>Batal</button>&nbsp;";
  html += "</div>";
  html += "</div>";
  html += "</div>";

  bootbox.dialog({
   message: html,
  });
 },

 execDeleted: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(RecoverPassword.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(RecoverPassword.module()) + "lupa_password";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },
};
